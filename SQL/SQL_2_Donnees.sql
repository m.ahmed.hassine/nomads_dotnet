
insert into ville_cp (libelle_ville, libelle_cp) values ('Paris',' 75000');
insert into ville_cp (libelle_ville, libelle_cp) values ('Paris 1er Arr.',' 75001');
insert into ville_cp (libelle_ville, libelle_cp) values ('Paris 2e Arr.',' 75002');
insert into ville_cp (libelle_ville, libelle_cp) values ('Paris 3e Arr.',' 75003');
insert into ville_cp (libelle_ville, libelle_cp) values ('Paris 4e Arr.',' 75004');
insert into ville_cp (libelle_ville, libelle_cp) values ('Paris 5e Arr.',' 75005');
insert into ville_cp (libelle_ville, libelle_cp) values ('Paris 6e Arr.',' 75006');
insert into ville_cp (libelle_ville, libelle_cp) values ('Paris 7e Arr.',' 75007');
insert into ville_cp (libelle_ville, libelle_cp) values ('Paris 8e Arr.',' 75008');
insert into ville_cp (libelle_ville, libelle_cp) values ('Paris 9e Arr.',' 75009');
insert into ville_cp (libelle_ville, libelle_cp) values ('Paris 10e Arr.',' 75010');
insert into ville_cp (libelle_ville, libelle_cp) values ('Paris 11e Arr.',' 75011');
insert into ville_cp (libelle_ville, libelle_cp) values ('Paris 12e Arr.',' 75012');
insert into ville_cp (libelle_ville, libelle_cp) values ('Paris 13e Arr.',' 75013');
insert into ville_cp (libelle_ville, libelle_cp) values ('Paris 14e Arr.',' 75014');
insert into ville_cp (libelle_ville, libelle_cp) values ('Paris 15e Arr.',' 75015');
insert into ville_cp (libelle_ville, libelle_cp) values ('Paris 16e Arr.',' 75016');
insert into ville_cp (libelle_ville, libelle_cp) values ('Paris 17e Arr.',' 75017');
insert into ville_cp (libelle_ville, libelle_cp) values ('Paris 18e Arr.',' 75018');
insert into ville_cp (libelle_ville, libelle_cp) values ('Paris 19e Arr.',' 75019');
insert into ville_cp (libelle_ville, libelle_cp) values ('Paris 20e Arr.',' 75020');
insert into ville_cp (libelle_ville, libelle_cp) values ('Rosny-sous-bois',' 93110');


insert into motif_fin_contrat_partenaire (libelle_motif_fin_contrat_partenaire) values ('Démenagement');
insert into motif_fin_contrat_partenaire (libelle_motif_fin_contrat_partenaire) values ('Intempérie');
insert into motif_fin_contrat_partenaire (libelle_motif_fin_contrat_partenaire) values ('Volonté');

insert into type_partenaire (libelle_type_partenaire) values ('Bar');
insert into type_partenaire (libelle_type_partenaire) values ('Restaurant');
insert into type_partenaire (libelle_type_partenaire) values ('Entreprise');
insert into type_partenaire (libelle_type_partenaire) values ('Evénement');

insert into type_pack_mads (libelle_pack, valeur) values ('3 Mads',10);
insert into type_pack_mads (libelle_pack, valeur) values ('5 Mads',16);
insert into type_pack_mads (libelle_pack, valeur) values ('10 Mads',32);
insert into type_pack_mads (libelle_pack, valeur) values ('15 Mads',44);
insert into type_pack_mads (libelle_pack, valeur) values ('20 Mads',56);
insert into type_pack_mads (libelle_pack, valeur) values ('25 Mads',69);

insert into partenaire( nom, raison_sociale,adresse_mail,telephone,numero_de_voie,complement_numero_voie,nom_de_voie,date_premier_enregistrement,id_ville,id_type_partenaire) values ('Panic Room','Panic Room','panic.room@gmail.com','0102020282','101',null, 'rue Amelot','2020-03-18',11,1);
insert into partenaire( nom, raison_sociale,adresse_mail,telephone,numero_de_voie,complement_numero_voie,nom_de_voie,date_premier_enregistrement,id_ville,id_type_partenaire) values ('Le Bistrot du Forum','Le Bistrot du Forum','bistrot.forum@gmail.com','0102020283','12',null,'Rue Montmartre','2020-03-18',1,1);
insert into partenaire( nom, raison_sociale,adresse_mail,telephone,numero_de_voie,complement_numero_voie,nom_de_voie,date_premier_enregistrement,id_ville,id_type_partenaire) values ('Aux Troix Maillets','Aux Troix Maillets','o3maillets@gmail.com','0102020283','78',null,'Rue Saint-Denis','2020-03-19',1,1);
insert into partenaire( nom, raison_sociale,adresse_mail,telephone,numero_de_voie,complement_numero_voie,nom_de_voie,date_premier_enregistrement,id_ville,id_type_partenaire) values ('Le Gob','Le Gob','le.gob@hotmail.com','0102020284','58',null,'Avenue des Gobelins','2020-03-20',13,1);
insert into partenaire( nom, raison_sociale,adresse_mail,telephone,numero_de_voie,complement_numero_voie,nom_de_voie,date_premier_enregistrement,id_ville,id_type_partenaire) values ('Ah! La Pompe à Bière','La Pompe à Bière','pompe@biere.ah','0102020285','7','ter','Rue Duvergier','2020-03-17',19,1);
insert into partenaire( nom, raison_sociale,adresse_mail,telephone,numero_de_voie,complement_numero_voie,nom_de_voie,date_premier_enregistrement,id_ville,id_type_partenaire) values ('Le 122','Le 122','le122@live.com','0102020286','122',null,'Rue de Grenelle','2020-03-17',7,2);
insert into partenaire( nom, raison_sociale,adresse_mail,telephone,numero_de_voie,complement_numero_voie,nom_de_voie,date_premier_enregistrement,id_ville,id_type_partenaire) values ('African Lounge','African Lounge','manger.africain@live.com','0102020287','20','bis','Rue Jean Giraudoux','2020-02-21',16,2);
insert into partenaire( nom, raison_sociale,adresse_mail,telephone,numero_de_voie,complement_numero_voie,nom_de_voie,date_premier_enregistrement,id_ville,id_type_partenaire) values ('LE bistrot de Marius','Bistrot Marius','Bistrot.marius@hotmail.fr','0102020288','6',null,'Avenue GeorgeV','2020-03-18',8,2);
insert into partenaire( nom, raison_sociale,adresse_mail,telephone,numero_de_voie,complement_numero_voie,nom_de_voie,date_premier_enregistrement,id_ville,id_type_partenaire) values ('EDF','Electricite de France','edf@edf.fr','8080','11',null,'Rue Victor Hugo','2020-03-12',8,3);
insert into partenaire( nom, raison_sociale,adresse_mail,telephone,numero_de_voie,complement_numero_voie,nom_de_voie,date_premier_enregistrement,id_ville,id_type_partenaire) values ('neovacs','neovacs','neovacs@neovacs.com','19','3',null,'Impasse Reille','2020-02-17',14,3);
insert into partenaire( nom, raison_sociale,adresse_mail,telephone,numero_de_voie,complement_numero_voie,nom_de_voie,date_premier_enregistrement,id_ville,id_type_partenaire) values ('Festival de Paris','festival','bchauvet@ecoleql.com','065478524','22',null,'Rue de Paris','2019-11-11',2,4);

insert into contrat_partenaire(date_debut_contrat_partenaire,date_fin_contrat_partenaire,id_motif_fin_contrat_partenaire,id_partenaire) values ('2020-02-01',null,null,1);
insert into contrat_partenaire(date_debut_contrat_partenaire,date_fin_contrat_partenaire,id_motif_fin_contrat_partenaire,id_partenaire) values ('2019-01-01','2020-05-10',2,3);
insert into contrat_partenaire(date_debut_contrat_partenaire,date_fin_contrat_partenaire,id_motif_fin_contrat_partenaire,id_partenaire) values ('2012-12-15','2018-11-12',1,2);
insert into contrat_partenaire(date_debut_contrat_partenaire,date_fin_contrat_partenaire,id_motif_fin_contrat_partenaire,id_partenaire) values ('2018-02-17','2022-07-19',null,5);

insert into motif_indisponibilite_partenaire(motif) values ('Travaux');
insert into motif_indisponibilite_partenaire(motif) values ('Vacances Annuelles');
insert into motif_indisponibilite_partenaire(motif) values ('Intempéries');
insert into motif_indisponibilite_partenaire(motif) values ('Dégâts');
insert into motif_indisponibilite_partenaire(motif) values ('Autres animations planifiées');
insert into motif_indisponibilite_partenaire(motif) values ('Autre');

insert into indisponibilite_partenaire(id_motif_indisponibilite_partenaire,date_debut_indisponibilite,date_fin_indisponibilite,id_partenaire) values (1,'2020-07-15','2020-08-03',4);
insert into indisponibilite_partenaire(id_motif_indisponibilite_partenaire,date_debut_indisponibilite,date_fin_indisponibilite,id_partenaire) values (2,'2020-03-16','2020-04-01',8);
insert into indisponibilite_partenaire(id_motif_indisponibilite_partenaire,date_debut_indisponibilite,date_fin_indisponibilite,id_partenaire) values (3,'2020-03-18',null,9);

insert into type_evaluation_partenaire(libelle_evaluation_partenaire,note_relationnel,note_ponctualite,note_qualite_massage) values ('Excellent',10,10,10);
insert into type_evaluation_partenaire(libelle_evaluation_partenaire,note_relationnel,note_ponctualite,note_qualite_massage) values ('Bien',9,1,9);
insert into type_evaluation_partenaire(libelle_evaluation_partenaire,note_relationnel,note_ponctualite,note_qualite_massage) values ('Moyen',7,5,7);
insert into type_evaluation_partenaire(libelle_evaluation_partenaire,note_relationnel,note_ponctualite,note_qualite_massage) values ('Médiocre',null,8,5);

insert into sexe(libelle_sexe) values ('Féminin');
insert into sexe(libelle_sexe) values ('Masculin ');
insert into sexe(libelle_sexe) values ('Indéterminé');

insert into niveau_competence(libelle_niveau_competence) values ('Neonomads');
insert into niveau_competence(libelle_niveau_competence) values ('Nomads expérimenté');
insert into niveau_competence(libelle_niveau_competence) values ('Nomads formateur');

insert into evolution_professionnelle_masseur(date_evolution,id_niveau_competence,id_masseur) values ('2020-03-25',1,1);
insert into evolution_professionnelle_masseur(date_evolution,id_niveau_competence,id_masseur) values ('2020-05-20',2,2);
insert into evolution_professionnelle_masseur(date_evolution,id_niveau_competence,id_masseur) values ('2020-08-18',3,3);
insert into evolution_professionnelle_masseur(date_evolution,id_niveau_competence,id_masseur) values ('2020-03-25',1,4);
insert into evolution_professionnelle_masseur(date_evolution,id_niveau_competence,id_masseur) values ('2020-05-20',2,5);
insert into evolution_professionnelle_masseur(date_evolution,id_niveau_competence,id_masseur) values ('2020-08-18',3,6);
insert into evolution_professionnelle_masseur(date_evolution,id_niveau_competence,id_masseur) values ('2020-03-25',1,7);
insert into evolution_professionnelle_masseur(date_evolution,id_niveau_competence,id_masseur) values ('2020-05-20',2,8);
insert into evolution_professionnelle_masseur(date_evolution,id_niveau_competence,id_masseur) values ('2020-08-18',3,9);
insert into evolution_professionnelle_masseur(date_evolution,id_niveau_competence,id_masseur) values ('2020-03-25',1,10);
insert into evolution_professionnelle_masseur(date_evolution,id_niveau_competence,id_masseur) values ('2020-05-20',2,11);
insert into evolution_professionnelle_masseur(date_evolution,id_niveau_competence,id_masseur) values ('2020-08-18',3,12);
insert into evolution_professionnelle_masseur(date_evolution,id_niveau_competence,id_masseur) values ('2020-03-25',1,13);
insert into evolution_professionnelle_masseur(date_evolution,id_niveau_competence,id_masseur) values ('2020-05-20',2,14);
insert into evolution_professionnelle_masseur(date_evolution,id_niveau_competence,id_masseur) values ('2020-08-18',3,15);
insert into evolution_professionnelle_masseur(date_evolution,id_niveau_competence,id_masseur) values ('2020-03-25',1,16);

insert into categorie(libelle_categorie) values ('Bars/restaurants');
insert into categorie(libelle_categorie) values ('Evementiels');
insert into categorie(libelle_categorie) values ('Entreprise');

insert into aptitude(date_debut_aptitude,date_fin_aptitude,id_categorie,id_masseur) values ('2019-01-01','2020-03-04',2,1);
insert into aptitude(date_debut_aptitude,date_fin_aptitude,id_categorie,id_masseur) values ('2020-03-16',null,1,2);
insert into aptitude(date_debut_aptitude,date_fin_aptitude,id_categorie,id_masseur) values ('2019-03-15','2020-03-16',3,3);
insert into aptitude(date_debut_aptitude,date_fin_aptitude,id_categorie,id_masseur) values ('2020-01-01',null,2,4);
insert into aptitude(date_debut_aptitude,date_fin_aptitude,id_categorie,id_masseur) values ('2020-03-16',null,1,5);
insert into aptitude(date_debut_aptitude,date_fin_aptitude,id_categorie,id_masseur) values ('2020-03-15',null,3,6);
insert into aptitude(date_debut_aptitude,date_fin_aptitude,id_categorie,id_masseur) values ('2018-01-01','2019-04-12',2,7);
insert into aptitude(date_debut_aptitude,date_fin_aptitude,id_categorie,id_masseur) values ('2019-03-16',null,1,8);
insert into aptitude(date_debut_aptitude,date_fin_aptitude,id_categorie,id_masseur) values ('2020-03-15',null,3,9);
insert into aptitude(date_debut_aptitude,date_fin_aptitude,id_categorie,id_masseur) values ('2020-01-01',null,2,10);
insert into aptitude(date_debut_aptitude,date_fin_aptitude,id_categorie,id_masseur) values ('2020-03-16',null,1,11);
insert into aptitude(date_debut_aptitude,date_fin_aptitude,id_categorie,id_masseur) values ('2020-03-15',null,3,12);
insert into aptitude(date_debut_aptitude,date_fin_aptitude,id_categorie,id_masseur) values ('2020-01-01',null,2,13);
insert into aptitude(date_debut_aptitude,date_fin_aptitude,id_categorie,id_masseur) values ('2018-03-16',null,1,14);
insert into aptitude(date_debut_aptitude,date_fin_aptitude,id_categorie,id_masseur) values ('2020-03-15',null,3,15);
insert into aptitude(date_debut_aptitude,date_fin_aptitude,id_categorie,id_masseur) values ('2020-01-01',null,2,16);

insert into motif_fin_contrat_masseur (libelle_motif_fin_contrat_masseur) values ('Démenagement');
insert into motif_fin_contrat_masseur (libelle_motif_fin_contrat_masseur) values ('CDI trouvé');
insert into motif_fin_contrat_masseur (libelle_motif_fin_contrat_masseur) values ('Accouchement');

insert into masseur(nom,prenom,date_de_naissance,adresse_mail,telephone,numero_de_voie,complement_numero_de_voie,nom_de_voie,date_premier_enregistrement,date_depart_definitif,id_sexe,id_evolution_professionnelle,id_aptitude,id_ville) values ('Malone','Arthur','1970-08-06','amet@ategestasa.edu','01 83 93 48 22','44','bis','Avenue De Gaulle','2016-10-18','2020-02-16',2,1,1,1);
insert into masseur(nom,prenom,date_de_naissance,adresse_mail,telephone,numero_de_voie,complement_numero_de_voie,nom_de_voie,date_premier_enregistrement,date_depart_definitif,id_sexe,id_evolution_professionnelle,id_aptitude,id_ville) values ('Obrien','Macaulay','1949-01-03','est.tempor@eu.net','01 42 02 49 75','75','','Rue Perrier','2017-05-14',null,3,2,2,2);
insert into masseur(nom,prenom,date_de_naissance,adresse_mail,telephone,numero_de_voie,complement_numero_de_voie,nom_de_voie,date_premier_enregistrement,date_depart_definitif,id_sexe,id_evolution_professionnelle,id_aptitude,id_ville) values ('Brady','Slade','1980-12-14','pede.malesuada@vulputate.org','06 86 49 50 71','50','','Rue Nido','2017-08-17',null,2,3,3,3);
insert into masseur(nom,prenom,date_de_naissance,adresse_mail,telephone,numero_de_voie,complement_numero_de_voie,nom_de_voie,date_premier_enregistrement,date_depart_definitif,id_sexe,id_evolution_professionnelle,id_aptitude,id_ville) values ('Frazier','Rebecca','1985-09-04','eget@arcueu.com','06 33 25 81 39','5','','Rue Mopi','2018-06-30',null,1,4,4,4);
insert into masseur(nom,prenom,date_de_naissance,adresse_mail,telephone,numero_de_voie,complement_numero_de_voie,nom_de_voie,date_premier_enregistrement,date_depart_definitif,id_sexe,id_evolution_professionnelle,id_aptitude,id_ville) values ('Salazar','Jesse','1975-09-05','pretium.et@mauris.ca','06 55 61 51 15','60','','Avenue Magenta','2018-01-01',null,2,5,5,5);
insert into masseur(nom,prenom,date_de_naissance,adresse_mail,telephone,numero_de_voie,complement_numero_de_voie,nom_de_voie,date_premier_enregistrement,date_depart_definitif,id_sexe,id_evolution_professionnelle,id_aptitude,id_ville) values ('Richmond','Giselle','1990-04-02','Integer.in@ligula.ca','06 07 38 37 93','69','','Avenue Moulin','2018-01-02',null,1,6,6,6);
insert into masseur(nom,prenom,date_de_naissance,adresse_mail,telephone,numero_de_voie,complement_numero_de_voie,nom_de_voie,date_premier_enregistrement,date_depart_definitif,id_sexe,id_evolution_professionnelle,id_aptitude,id_ville) values ('Cantu','Basil','1997-05-05','sed@Nulla.net','06 78 08 35 07','36','bis','Rue Tipe','2018-01-03',null,2,7,7,7);
insert into masseur(nom,prenom,date_de_naissance,adresse_mail,telephone,numero_de_voie,complement_numero_de_voie,nom_de_voie,date_premier_enregistrement,date_depart_definitif,id_sexe,id_evolution_professionnelle,id_aptitude,id_ville) values ('Case','Beau','1994-08-03','iaculis.aliquet@Cumsociisnatoque.net','06 55 46 97 00','52','','Rue Blanc','2018-01-04',null,2,8,8,8);
insert into masseur(nom,prenom,date_de_naissance,adresse_mail,telephone,numero_de_voie,complement_numero_de_voie,nom_de_voie,date_premier_enregistrement,date_depart_definitif,id_sexe,id_evolution_professionnelle,id_aptitude,id_ville) values ('Miller','Rebecca','1984-07-24','rutrum.Fusce.dolor@nibhenim.org','06 40 17 48 32','85','','Rue Nices','2019-05-25','2018-09-15',1,9,9,9);
insert into masseur(nom,prenom,date_de_naissance,adresse_mail,telephone,numero_de_voie,complement_numero_de_voie,nom_de_voie,date_premier_enregistrement,date_depart_definitif,id_sexe,id_evolution_professionnelle,id_aptitude,id_ville) values ('Stark','Coby','1985-06-30','ac@nascetur.ca','06 45 72 21 66','54','','Boulevard Noir','2019-05-26',null,2,10,10,10);
insert into masseur(nom,prenom,date_de_naissance,adresse_mail,telephone,numero_de_voie,complement_numero_de_voie,nom_de_voie,date_premier_enregistrement,date_depart_definitif,id_sexe,id_evolution_professionnelle,id_aptitude,id_ville) values ('Crane','Lionel','1999-09-09','vitae@Pellentesqueultriciesdignissim.com','06 89 14 09 22','75','','Rue Feri','2019-05-27',null,2,11,11,11);
insert into masseur(nom,prenom,date_de_naissance,adresse_mail,telephone,numero_de_voie,complement_numero_de_voie,nom_de_voie,date_premier_enregistrement,date_depart_definitif,id_sexe,id_evolution_professionnelle,id_aptitude,id_ville) values ('Rivers','Ingrid','1987-06-01','molestie.orci.tincidunt@nisl.co.uk','06 88 31 13 89','9','','Rue Jali','2019-05-28',null,1,12,12,6);
insert into masseur(nom,prenom,date_de_naissance,adresse_mail,telephone,numero_de_voie,complement_numero_de_voie,nom_de_voie,date_premier_enregistrement,date_depart_definitif,id_sexe,id_evolution_professionnelle,id_aptitude,id_ville) values ('Richard','Jolene','1979-02-09','Aenean@nibhdolor.org','06 85 37 27 21','68','','Rue Toluda','2019-05-29',null,3,13,13,13);
insert into masseur(nom,prenom,date_de_naissance,adresse_mail,telephone,numero_de_voie,complement_numero_de_voie,nom_de_voie,date_premier_enregistrement,date_depart_definitif,id_sexe,id_evolution_professionnelle,id_aptitude,id_ville) values ('Solis','Sylvia','2000-02-05','dolor.tempus.non@massarutrummagna.ca','06 72 59 96 93','61','bis','Boulevard Rouge','2019-05-30',null,1,14,14,5);
insert into masseur(nom,prenom,date_de_naissance,adresse_mail,telephone,numero_de_voie,complement_numero_de_voie,nom_de_voie,date_premier_enregistrement,date_depart_definitif,id_sexe,id_evolution_professionnelle,id_aptitude,id_ville) values ('Watson','Leo','1986-06-14','sociis.natoque@mi.co.uk','06 83 29 40 02','25','','Rue Guilo','2020-01-20',null,2,15,15,21);
insert into masseur(nom,prenom,date_de_naissance,adresse_mail,telephone,numero_de_voie,complement_numero_de_voie,nom_de_voie,date_premier_enregistrement,date_depart_definitif,id_sexe,id_evolution_professionnelle,id_aptitude,id_ville) values ('Riley','Amber','1996-01-01','sagittis.lobortis.mauris@Sedet.edu','06 36 73 96 37','35','','Rue Yvero','2020-01-21',null,1,16,16,20);

insert into libelle_recurrence(libelle) values ('Journalière');
insert into libelle_recurrence(libelle) values ('Hebdomadaire');
insert into libelle_recurrence(libelle) values ('Mensuelle');
insert into libelle_recurrence(libelle) values ('Annuelle');

insert into libelle_duree_recurrence(libelle) values ('Jours');
insert into libelle_duree_recurrence(libelle) values ('Semaines');
insert into libelle_duree_recurrence(libelle) values ('Mois');
insert into libelle_duree_recurrence(libelle) values ('Année(s)');

insert into recurrence(id_libelle_recurrence,id_libelle_duree_recurrence,pas,duree_recurrence, delta) values (2,1,12,2,2);
insert into recurrence(id_libelle_recurrence,id_libelle_duree_recurrence,pas,duree_recurrence, delta) values (1,null,2,5,1);
insert into recurrence(id_libelle_recurrence,id_libelle_duree_recurrence,pas,duree_recurrence, delta) values (2,1,1,1,1);
insert into recurrence(id_libelle_recurrence,id_libelle_duree_recurrence,pas,duree_recurrence, delta) values (3,2,1,0,2);
insert into recurrence(id_libelle_recurrence,id_libelle_duree_recurrence,pas,duree_recurrence, delta) values (4,2,1,1,1);

insert into motif_annulation_prestation(libelle_motif_annulation) values ('manque de masseurs disponible');
insert into motif_annulation_prestation(libelle_motif_annulation) values ('bar réservé par un client');
insert into motif_annulation_prestation(libelle_motif_annulation) values ('bar prévoit un autre type d animation');
insert into motif_annulation_prestation(libelle_motif_annulation) values ('covid 19');

insert into libelle_type_paiement(libelle) values ('Mads');
insert into libelle_type_paiement(libelle) values ('Devis');

insert into type_paiement(id_libelle_type_paiement,date_emission,montant_facturation,date_validation) values (2,'2020-02-02','1260.5','2020-03-01');
insert into type_paiement(id_libelle_type_paiement,date_emission,montant_facturation,date_validation) values (2,'2020-03-25','760',null);
insert into type_paiement(id_libelle_type_paiement,date_emission,montant_facturation,date_validation) values (1,null,null,null);

insert into prestation(libelle,date_prestation,heure_debut, heure_fin,nombre_masseurs,nombre_chaises_ergonomiques,cout_mads,montant_remuneration,date_annulation,date_paiement,numero_de_voie,complement_numero_voie,nom_de_voie,id_partenaire,id_recurrence,id_motif_annulation,id_type_paiement,id_ville,specificites) values ('Panic Room night Fever','2020-04-02 00:00:00','18:00','22:00',4,0,2,NULL,NULL,NULL,'101',NULL,'Rue Amelot',1,2,NULL,3,11,'Rien');
insert into prestation(libelle,date_prestation,heure_debut, heure_fin,nombre_masseurs,nombre_chaises_ergonomiques,cout_mads,montant_remuneration,date_annulation,date_paiement,numero_de_voie,complement_numero_voie,nom_de_voie,id_partenaire,id_recurrence,id_motif_annulation,id_type_paiement,id_ville,specificites) values ('Le Gob Special Night','2020-06-03 00:00:00','19:30','23:00',6,2,3,NULL,'2020-03-19 00:00:00',NULL,'58',NULL,'Avenue des Gobelins',4,3,4,3,13,'Rien');
insert into prestation(libelle,date_prestation,heure_debut, heure_fin,nombre_masseurs,nombre_chaises_ergonomiques,cout_mads,montant_remuneration,date_annulation,date_paiement,numero_de_voie,complement_numero_voie,nom_de_voie,id_partenaire,id_recurrence,id_motif_annulation,id_type_paiement,id_ville,specificites) values ('African Lounge Event ','2020-04-04 00:00:00','18:00','22:30',4,0,2,NULL,NULL,NULL,'20','bis','Rue Jean Giraudoux',7,4,NULL,3,16,'Rien');
insert into prestation(libelle,date_prestation,heure_debut, heure_fin,nombre_masseurs,nombre_chaises_ergonomiques,cout_mads,montant_remuneration,date_annulation,date_paiement,numero_de_voie,complement_numero_voie,nom_de_voie,id_partenaire,id_recurrence,id_motif_annulation,id_type_paiement,id_ville,specificites) values ('EDF Best Night Ever','2020-05-18 00:00:00','17:30','20:30',4,4,0,70,NULL,'2020-05-19 00:00:00','11',NULL,'Rue Victor Hugo',9,5,NULL,2,8,'Rien');
insert into prestation(libelle,date_prestation,heure_debut, heure_fin,nombre_masseurs,nombre_chaises_ergonomiques,cout_mads,montant_remuneration,date_annulation,date_paiement,numero_de_voie,complement_numero_voie,nom_de_voie,id_partenaire,id_recurrence,id_motif_annulation,id_type_paiement,id_ville,specificites) values ('Festival de Paris in Paris','2020-06-06 00:00:00','14:00','16:00',12,0,0,80,NULL,'2020-06-07 00:00:00','22',NULL,'Rue de Paris',11,4,NULL,1,2,'Rien');
insert into prestation(libelle,date_prestation,heure_debut, heure_fin,nombre_masseurs,nombre_chaises_ergonomiques,cout_mads,montant_remuneration,date_annulation,date_paiement,numero_de_voie,complement_numero_voie,nom_de_voie,id_partenaire,id_recurrence,id_motif_annulation,id_type_paiement,id_ville,specificites) values ('La nuit de Marius','2020-06-06 00:00:00','20:00','23:30',12,0,0,80,NULL,'2020-06-07 00:00:00','22',NULL,'Rue de Paris',9,2,NULL,1,2,'Rien');
insert into prestation(libelle,date_prestation,heure_debut, heure_fin,nombre_masseurs,nombre_chaises_ergonomiques,cout_mads,montant_remuneration,date_annulation,date_paiement,numero_de_voie,complement_numero_voie,nom_de_voie,id_partenaire,id_recurrence,id_motif_annulation,id_type_paiement,id_ville,specificites) values ('Test Antérieur','2020-02-02 00:00:00','16:00','19:00',2,0,2,0,NULL,NULL,'22',NULL,'Rue du test',8,2,NULL,1,7,'Rien');
insert into prestation(libelle,date_prestation,heure_debut, heure_fin,nombre_masseurs,nombre_chaises_ergonomiques,cout_mads,montant_remuneration,date_annulation,date_paiement,numero_de_voie,complement_numero_voie,nom_de_voie,id_partenaire,id_recurrence,id_motif_annulation,id_type_paiement,id_ville,specificites) values ('Le début de la Faim','2020-01-06 00:00:00','20:00','23:45',2,0,2,0,NULL,NULL,'27','ter','Rue du Début',4,2,NULL,1,2,'Rien');
insert into prestation(libelle,date_prestation,heure_debut, heure_fin,nombre_masseurs,nombre_chaises_ergonomiques,cout_mads,montant_remuneration,date_annulation,date_paiement,numero_de_voie,complement_numero_voie,nom_de_voie,id_partenaire,id_recurrence,id_motif_annulation,id_type_paiement,id_ville,specificites) values ('Apocalhips now','2020-03-06 00:00:00','12:00','14:00',2,0,2,0,NULL,NULL,'27','ter','Rue du Début',4,2,NULL,1,2,'Rien');
insert into prestation(libelle,date_prestation,heure_debut, heure_fin,nombre_masseurs,nombre_chaises_ergonomiques,cout_mads,montant_remuneration,date_annulation,date_paiement,numero_de_voie,complement_numero_voie,nom_de_voie,id_partenaire,id_recurrence,id_motif_annulation,id_type_paiement,id_ville,specificites) values ('Le Neovac SpringTime','2020-06-06 00:00:00','20:00','22:00',4,1,3,NULL,NULL,NULL,'3',NULL,'Impasse reille',10,1,NULL,1,14,'Choisir des vêtements verts et un chapeau à pois');
insert into prestation(libelle,date_prestation,heure_debut, heure_fin,nombre_masseurs,nombre_chaises_ergonomiques,cout_mads,montant_remuneration,date_annulation,date_paiement,numero_de_voie,complement_numero_voie,nom_de_voie,id_partenaire,id_recurrence,id_motif_annulation,id_type_paiement,id_ville,specificites) values ('Au printemps du 122','2020-04-04 00:00:00','17:00','20:00',2,0,2,NULL,NULL,NULL,'122',NULL,'Rue de Grenelle',6,1,NULL,1,7,'ne pas avoir de moustache supérieure à 122 mm, sauf si retroussée. Chaussures bleues exigées.');
insert into prestation(libelle,date_prestation,heure_debut, heure_fin,nombre_masseurs,nombre_chaises_ergonomiques,cout_mads,montant_remuneration,date_annulation,date_paiement,numero_de_voie,complement_numero_voie,nom_de_voie,id_partenaire,id_recurrence,id_motif_annulation,id_type_paiement,id_ville,specificites) values ('Le Neovac SpringMidTime','2020-06-06 00:00:00','11:00','13:30',4,1,3,NULL,NULL,NULL,'3',NULL,'Impasse reille',10,1,NULL,1,14,'Arriver 15 min à l avance pour se fondre dans le décor; anniversaire du gérant');
insert into prestation(libelle,date_prestation,heure_debut, heure_fin,nombre_masseurs,nombre_chaises_ergonomiques,cout_mads,montant_remuneration,date_annulation,date_paiement,numero_de_voie,complement_numero_voie,nom_de_voie,id_partenaire,id_recurrence,id_motif_annulation,id_type_paiement,id_ville,specificites) values ('Les 3 coups de 4 heures','2020-06-06 00:00:00','17:00','19:00',4,1,3,NULL,NULL,NULL,'78',NULL,'Rue Saint Denis',3,1,NULL,1,1,'Penser à apporter ses masques et gants; une consommation offerte après le service!');
insert into prestation(libelle,date_prestation,heure_debut, heure_fin,nombre_masseurs,nombre_chaises_ergonomiques,cout_mads,montant_remuneration,date_annulation,date_paiement,numero_de_voie,complement_numero_voie,nom_de_voie,id_partenaire,id_recurrence,id_motif_annulation,id_type_paiement,id_ville,specificites) values ('La nocturne','2020-05-05 00:00:00','18:00','23:00',4,1,3,NULL,NULL,NULL,'78',NULL,'Rue Saint Denis',3,1,NULL,1,1,'Grignotage prévu tout au long de la prestation, une boisson au choix');


insert into motif_desinscription (libelle_motif_desinscription) values ('maladie');
insert into motif_desinscription (libelle_motif_desinscription) values ('décés d un proche');
insert into motif_desinscription (libelle_motif_desinscription) values ('force majeure');

insert into type_evaluation_masseur(libelle_evaluation_masseur,note_accueil,note_ambiance,note_clientele) values ('Excellent',10,10,null);
insert into type_evaluation_masseur(libelle_evaluation_masseur,note_accueil,note_ambiance,note_clientele) values ('Bien',9,1,null);
insert into type_evaluation_masseur(libelle_evaluation_masseur,note_accueil,note_ambiance,note_clientele) values ('Moyen',8,5,null);
insert into type_evaluation_masseur(libelle_evaluation_masseur,note_accueil,note_ambiance,note_clientele) values ('Médiocre',7,8,null);

insert into inscription_masseurs(date_inscription, date_desinscription, fichier_joint, id_masseur, id_prestation, id_motif_desinscription) values ('2020-03-14',null,'justificatif_id_masseur1.pdf',1,1,null);
insert into inscription_masseurs(date_inscription, date_desinscription, fichier_joint, id_masseur, id_prestation, id_motif_desinscription) values ('2020-03-15',null,null,10,5,null);
insert into inscription_masseurs(date_inscription, date_desinscription, fichier_joint, id_masseur, id_prestation, id_motif_desinscription) values ('2020-03-15',null,'justificatif_id_masseur3.pdf',3,2,null);
insert into inscription_masseurs(date_inscription, date_desinscription, fichier_joint, id_masseur, id_prestation, id_motif_desinscription) values ('2020-03-16',null,null,10,1,null);
insert into inscription_masseurs(date_inscription, date_desinscription, fichier_joint, id_masseur, id_prestation, id_motif_desinscription) values ('2020-03-16',null,null,5,1,null);
insert into inscription_masseurs(date_inscription, date_desinscription, fichier_joint, id_masseur, id_prestation, id_motif_desinscription) values ('2020-03-18',null,'justificatif_id_masseur8.pdf',8,1,null);
insert into inscription_masseurs(date_inscription, date_desinscription, fichier_joint, id_masseur, id_prestation, id_motif_desinscription) values ('2020-01-22',null,null,5,7,null);
insert into inscription_masseurs(date_inscription, date_desinscription, fichier_joint, id_masseur, id_prestation, id_motif_desinscription) values ('2020-01-01',null,null,5,8,null);
insert into inscription_masseurs(date_inscription, date_desinscription, fichier_joint, id_masseur, id_prestation, id_motif_desinscription) values ('2020-01-18',null,null,5,9,null);
insert into inscription_masseurs(date_inscription, date_desinscription, fichier_joint, id_masseur, id_prestation, id_motif_desinscription) values ('2020-01-18',null,null,5,10,null);
insert into inscription_masseurs(date_inscription, date_desinscription, fichier_joint, id_masseur, id_prestation, id_motif_desinscription) values ('2020-01-18',null,null,5,11,null);


insert into contrat_masseur(date_debut_contrat_masseur, date_fin_contrat_masseur, id_masseur, id_motif_fin_contrat_masseur) values ('2016-10-18','2020-02-16',1,1);
insert into contrat_masseur(date_debut_contrat_masseur, date_fin_contrat_masseur, id_masseur, id_motif_fin_contrat_masseur) values ('2017-05-14',null,2,null);
insert into contrat_masseur(date_debut_contrat_masseur, date_fin_contrat_masseur, id_masseur, id_motif_fin_contrat_masseur) values ('2017-08-17','2018-01-25',3,3);
insert into contrat_masseur(date_debut_contrat_masseur, date_fin_contrat_masseur, id_masseur, id_motif_fin_contrat_masseur) values ('2018-06-30',null,4,null);
insert into contrat_masseur(date_debut_contrat_masseur, date_fin_contrat_masseur, id_masseur, id_motif_fin_contrat_masseur) values ('2018-01-01',null,5,null);
insert into contrat_masseur(date_debut_contrat_masseur, date_fin_contrat_masseur, id_masseur, id_motif_fin_contrat_masseur) values ('2018-01-02',null,6,null);
insert into contrat_masseur(date_debut_contrat_masseur, date_fin_contrat_masseur, id_masseur, id_motif_fin_contrat_masseur) values ('2018-01-03',null,7,null);
insert into contrat_masseur(date_debut_contrat_masseur, date_fin_contrat_masseur, id_masseur, id_motif_fin_contrat_masseur) values ('2018-01-04',null,8,null);
insert into contrat_masseur(date_debut_contrat_masseur, date_fin_contrat_masseur, id_masseur, id_motif_fin_contrat_masseur) values ('2019-03-29',null,3,null);
insert into contrat_masseur(date_debut_contrat_masseur, date_fin_contrat_masseur, id_masseur, id_motif_fin_contrat_masseur) values ('2019-05-25','2018-09-15',9,2);
insert into contrat_masseur(date_debut_contrat_masseur, date_fin_contrat_masseur, id_masseur, id_motif_fin_contrat_masseur) values ('2019-05-26',null,10,null);
insert into contrat_masseur(date_debut_contrat_masseur, date_fin_contrat_masseur, id_masseur, id_motif_fin_contrat_masseur) values ('2019-05-27',null,11,null);
insert into contrat_masseur(date_debut_contrat_masseur, date_fin_contrat_masseur, id_masseur, id_motif_fin_contrat_masseur) values ('2019-05-28',null,12,null);
insert into contrat_masseur(date_debut_contrat_masseur, date_fin_contrat_masseur, id_masseur, id_motif_fin_contrat_masseur) values ('2019-05-29',null,13,null);
insert into contrat_masseur(date_debut_contrat_masseur, date_fin_contrat_masseur, id_masseur, id_motif_fin_contrat_masseur) values ('2019-05-30',null,14,null);
insert into contrat_masseur(date_debut_contrat_masseur, date_fin_contrat_masseur, id_masseur, id_motif_fin_contrat_masseur) values ('2020-01-20',null,15,null);
insert into contrat_masseur(date_debut_contrat_masseur, date_fin_contrat_masseur, id_masseur, id_motif_fin_contrat_masseur) values ('2020-01-21',null,16,null);

insert into retour_experience_partenaire (libelle_retour_experience_partenaire, date_retour_experience_partenaire, id_partenaire, id_prestation, id_type_evaluation_partenaire) values ('Parfait','2020-07-15',1,1,1);
insert into retour_experience_partenaire (libelle_retour_experience_partenaire, date_retour_experience_partenaire, id_partenaire, id_prestation, id_type_evaluation_partenaire) values ('Bien','2020-03-11',1,3,2);
insert into retour_experience_partenaire (libelle_retour_experience_partenaire, date_retour_experience_partenaire, id_partenaire, id_prestation, id_type_evaluation_partenaire) values ('Correct','2020-01-09',1,4,3);
insert into retour_experience_partenaire (libelle_retour_experience_partenaire, date_retour_experience_partenaire, id_partenaire, id_prestation, id_type_evaluation_partenaire) values ('A améliorer','2019-07-02',1,6,3);
insert into retour_experience_partenaire (libelle_retour_experience_partenaire, date_retour_experience_partenaire, id_partenaire, id_prestation, id_type_evaluation_partenaire) values ('Décévant','2020-11-12',1,5,4);

insert into location_materiels (article, quantite_articles, date_emprunt, date_retour, montant_caution_en_mads, id_prestation, id_masseur) values ('chaise',3,'2020-03-02','2020-03-06',120,1,1);
insert into location_materiels (article, quantite_articles, date_emprunt, date_retour, montant_caution_en_mads, id_prestation, id_masseur) values ('Gants',12,'2020-04-09','2020-04-17',200,5,10);
insert into location_materiels (article, quantite_articles, date_emprunt, date_retour, montant_caution_en_mads, id_prestation, id_masseur) values ('Serviette',20,'2020-02-13','2020-02-28',100,1,3);
insert into location_materiels (article, quantite_articles, date_emprunt, date_retour, montant_caution_en_mads, id_prestation, id_masseur) values ('Tshirt',10,'2020-01-16','2020-02-06',50,1,10);
insert into location_materiels (article, quantite_articles, date_emprunt, date_retour, montant_caution_en_mads, id_prestation, id_masseur) values ('Bonnet',35,'2020-04-09','2020-04-17',10,5,10);

insert into transaction(nombre_de_pack,date_transaction,id_masseur,id_type_pack) values (1,'2020-02-01',2,3);
insert into transaction(nombre_de_pack,date_transaction,id_masseur,id_type_pack) values (2,'2020-02-02',5,5);

insert into motif_indisponibilite_masseur(motif) values ('Maladie');
insert into motif_indisponibilite_masseur(motif) values ('Vacances Annuelles');
insert into motif_indisponibilite_masseur(motif) values ('Famille');
insert into motif_indisponibilite_masseur(motif) values ('Autre');

insert into indisponibilite_masseur(id_motif_indisponibilite_masseur,date_debut_indisponibilite,date_fin_indisponibilite,id_masseur) values (1,'2020-07-15','2020-08-03',4);
insert into indisponibilite_masseur(id_motif_indisponibilite_masseur,date_debut_indisponibilite,date_fin_indisponibilite,id_masseur) values (2,'2020-03-16','2020-04-01',8);
insert into indisponibilite_masseur(id_motif_indisponibilite_masseur,date_debut_indisponibilite,date_fin_indisponibilite,id_masseur) values (3,'2020-03-18',null,9);
