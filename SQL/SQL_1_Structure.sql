drop database if exists Nomads;

create database Nomads;

use Nomads;

CREATE TABLE ville_cp (
	id_ville int auto_increment not null,
	libelle_ville varchar(50) not null,
	libelle_cp varchar(10) not null,
	PRIMARY KEY (id_ville)
	);

CREATE TABLE motif_fin_contrat_partenaire (
	id_motif_fin_contrat_partenaire int auto_increment not null,
	libelle_motif_fin_contrat_partenaire varchar(255) not null,
	PRIMARY KEY (id_motif_fin_contrat_partenaire)
);

CREATE TABLE type_partenaire (
	id_type_partenaire int auto_increment not null,
	libelle_type_partenaire varchar(50) not null,
	PRIMARY KEY (id_type_partenaire)
);

CREATE TABLE partenaire(
	id_partenaire int auto_increment not null,
	nom varchar(50) not null,
	raison_sociale varchar(25) not null,
	adresse_mail varchar(50) not null,
	telephone varchar(13) not null,
	numero_de_voie varchar(	5) not null,
	complement_numero_voie varchar(5),
	nom_de_voie varchar(25) not null ,
	date_premier_enregistrement dateTime not null,
	id_ville int not null,
	id_type_partenaire int not null,
	PRIMARY KEY (id_partenaire),
	CONSTRAINT FK_PARTENAIRE_VILLE_CP FOREIGN KEY (id_ville) REFERENCES Ville_CP(id_ville),
	CONSTRAINT FK_PARTENAIRE_TYPE_PARTENAIRE FOREIGN KEY (id_type_partenaire) REFERENCES type_partenaire(id_type_partenaire)
	
);

CREATE TABLE contrat_partenaire (
	id_contrat_partenaire int auto_increment not null,
	date_debut_contrat_partenaire datetime not null,
	date_fin_contrat_partenaire datetime,
	id_motif_fin_contrat_partenaire int,
	id_partenaire int not null,
	PRIMARY KEY (id_contrat_partenaire),
	CONSTRAINT FK_CONTRAT_MOTIF_FIN_CONTRAT_PARTENAIRE FOREIGN KEY (id_motif_fin_contrat_partenaire) REFERENCES motif_fin_contrat_partenaire (id_motif_fin_contrat_partenaire),
	CONSTRAINT FK_CONTRAT_PARTENAIRE FOREIGN KEY (id_partenaire) REFERENCES partenaire(id_partenaire)
);

CREATE TABLE motif_indisponibilite_partenaire(
id_motif_indisponibilite_partenaire int auto_increment not null,
motif varchar(255) not null,
PRIMARY KEY (id_motif_indisponibilite_partenaire)
);

CREATE TABLE indisponibilite_partenaire (
	id_indisponibilite_partenaire int auto_increment not null,
	date_debut_indisponibilite dateTime not null,
	date_fin_indisponibilite dateTime,
	id_Partenaire int not null,
    id_motif_indisponibilite_partenaire int,
	PRIMARY KEY (id_indisponibilite_partenaire),
	CONSTRAINT FK_INDISPONIBILITE_PARTENAIRE FOREIGN KEY (id_partenaire) REFERENCES partenaire(id_partenaire),
    CONSTRAINT FK_INDISPONIBILITE_PARTENAIRE_MOTIF FOREIGN KEY (id_motif_indisponibilite_partenaire) REFERENCES motif_indisponibilite_partenaire(id_motif_indisponibilite_partenaire)
);

CREATE TABLE type_evaluation_partenaire (
	id_type_evaluation_partenaire  int auto_increment not null,
	libelle_evaluation_partenaire varchar(255) not null,
    note_relationnel int,
    note_ponctualite int, 
    note_qualite_massage int,
	PRIMARY KEY (id_type_evaluation_partenaire)
);

create TABLE sexe(
id_sexe int not null auto_increment,
libelle_sexe varchar(15),
PRIMARY KEY (id_sexe)
); 

CREATE TABLE niveau_competence(
id_statut int not null auto_increment,
libelle_niveau_competence varchar(25),
PRIMARY KEY (id_statut)
);

CREATE TABLE evolution_professionnelle_masseur(
id_suivi_competences int not null auto_increment,
date_evolution datetime not null,
id_niveau_competence int not null,
id_masseur int not null,
PRIMARY KEY (id_suivi_competences),
CONSTRAINT FK_EVOLUTION_PROFESSIONNELLE_MASSEUR_NIVEAU_COMPETENCE FOREIGN KEY (id_niveau_competence) REFERENCES niveau_competence(id_statut)
); 

CREATE TABLE categorie(
id_categorie int not null auto_increment,
libelle_categorie varchar(25),
PRIMARY KEY (id_categorie)
);

CREATE TABLE aptitude(
id_aptitude int not null auto_increment,
date_debut_aptitude datetime not null,
date_fin_aptitude datetime,
id_categorie int not null,
id_masseur int not null,
PRIMARY KEY (id_aptitude),
CONSTRAINT FK_APTITUDE_CATEGORIE FOREIGN KEY (id_categorie) REFERENCES categorie(id_categorie)
);

CREATE TABLE masseur (
id_masseur int not null auto_increment,
nom varchar(50) not null,
prenom varchar(50) not null,
date_de_naissance datetime not null,
adresse_mail varchar(50) not null,
telephone varchar(15) not null,
numero_de_voie varchar(5) not null,
complement_numero_de_voie varchar(5),
nom_de_voie varchar(50) not null,
date_premier_enregistrement datetime not null,
date_depart_definitif datetime,
id_sexe int not null,
id_evolution_professionnelle int not null,
id_aptitude int not null,
id_ville int,
PRIMARY KEY (id_masseur),
CONSTRAINT FK_MASSEUR_SEXE FOREIGN KEY (id_sexe) REFERENCES sexe(id_sexe),
CONSTRAINT FK_MASSEUR_EVOLUTION_PROFESSIONNELLE FOREIGN KEY (id_evolution_professionnelle) REFERENCES evolution_professionnelle_masseur(id_suivi_competences),
CONSTRAINT FK_MASSEUR_APTITUDE FOREIGN KEY (id_aptitude) REFERENCES aptitude(id_aptitude),
CONSTRAINT FK_MASSEUR_VILLE FOREIGN KEY (id_ville) REFERENCES ville_cp(id_ville)
);
CREATE TABLE libelle_duree_recurrence(
	id_libelle_duree_recurrence int not null auto_increment unique,
	libelle varchar(50) not null,
	PRIMARY KEY (id_libelle_duree_recurrence)	
);
CREATE TABLE libelle_recurrence(
	id_libelle_recurrence int not null auto_increment unique,
	libelle varchar(50) not null,
	PRIMARY KEY (id_libelle_recurrence)	
);
CREATE TABLE recurrence(
	id_recurrence int not null auto_increment unique,
	id_libelle_recurrence int not null,
	id_libelle_duree_recurrence int,
	pas int not null,
	duree_recurrence int, 
	delta int, 
	PRIMARY KEY (id_recurrence),
	CONSTRAINT FK_RECURRENCE_LIBELLE_RECURENCE FOREIGN KEY (id_libelle_recurrence) REFERENCES libelle_recurrence(id_libelle_recurrence),
	CONSTRAINT FK_RECURRENCE_LIBELLE_DUREE_RECURENCE FOREIGN KEY (id_libelle_duree_recurrence) REFERENCES libelle_duree_recurrence(id_libelle_duree_recurrence)
);

CREATE TABLE motif_annulation_prestation(
	id_motif_annulation int not null auto_increment unique,
	libelle_motif_annulation varchar(50) not null,
	PRIMARY KEY (id_motif_annulation)
);

CREATE TABLE libelle_type_paiement(
	id_libelle_type_paiement int not null auto_increment unique,
	libelle varchar(50) not null,
	PRIMARY KEY (id_libelle_type_paiement)
);

CREATE TABLE type_paiement(
	id_type_paiement int not null auto_increment unique,
	id_libelle_type_paiement int,
	date_emission dateTime, 
	montant_facturation double, 
	date_validation dateTime, 
	PRIMARY KEY (id_type_paiement),
	CONSTRAINT FK_TYPE_PAIEMENT_LIBELLE_TYPE_PAIEMENT FOREIGN KEY (id_libelle_type_paiement) REFERENCES libelle_type_paiement(id_libelle_type_paiement)
);

CREATE TABLE prestation (
	id_prestation int not null auto_increment unique, 
	libelle varchar(255) not null, 
	date_prestation dateTime not null, 
	heure_debut varchar(25),
	heure_fin varchar(25),	
	nombre_masseurs int not null default 2, 
	nombre_chaises_ergonomiques int,
	cout_mads double not null default 0,
	montant_remuneration double, 
	date_annulation dateTime default null, 
	date_paiement dateTime, 
	numero_de_voie varchar(5) not null,
	complement_numero_voie varchar(5),
	nom_de_voie varchar(255) not null,
	specificites text,
	id_partenaire int not null,
	id_recurrence int, 
	id_motif_annulation int, 
	id_type_paiement int,
    id_ville int not null, 
	PRIMARY KEY (id_prestation),
	CONSTRAINT FK_PRESTATION_PARTENAIRE FOREIGN KEY (id_partenaire) REFERENCES partenaire (id_partenaire),
	CONSTRAINT FK_PRESTATION_RECURRENCE FOREIGN KEY (id_recurrence) REFERENCES recurrence (id_recurrence),
	CONSTRAINT FK_PRESTATION_MOTIF_ANNULATION FOREIGN KEY (id_motif_annulation) REFERENCES motif_annulation_prestation (id_motif_annulation),
	CONSTRAINT FK_PRESTATION_TYPE_PAIEMENT FOREIGN KEY (id_type_paiement) REFERENCES type_paiement (id_type_paiement),
	CONSTRAINT FK_PRESTATION_VILLE_CP FOREIGN KEY (id_ville) REFERENCES ville_cp (id_ville)
    
    
);


CREATE TABLE motif_desinscription(
id_motif_desinscription int not null auto_increment,
libelle_motif_desinscription varchar(25),
PRIMARY KEY(id_motif_desinscription)
);

CREATE TABLE inscription_masseurs(
id_inscription_masseur int not null auto_increment,
date_inscription datetime not null,
date_desinscription datetime,
fichier_joint varchar(50), 
id_masseur int not null,
id_prestation int not null,
id_motif_desinscription int,
PRIMARY KEY(id_inscription_masseur),
CONSTRAINT FK_INSCRIPTION_MASSEUR_MASSEUR FOREIGN KEY (id_masseur) REFERENCES masseur(id_masseur),
CONSTRAINT FK_INSCRIPTION_MASSEUR_PRESTATION FOREIGN KEY (id_prestation) REFERENCES prestation (id_prestation) ,
CONSTRAINT FK_INSCRIPTION_MASSEUR_MOTIF_DESINSCRIPTION FOREIGN KEY (id_motif_desinscription) REFERENCES motif_desinscription(id_motif_desinscription)

);


CREATE TABLE retour_experience_partenaire (
	id_retour_experience_partenaire  int auto_increment not null,
	libelle_retour_experience_partenaire varchar(255) not null,
	date_retour_experience_partenaire datetime not null,
	id_partenaire int not null,
	id_prestation int not null,
	id_type_evaluation_partenaire int not null,
	PRIMARY KEY (id_retour_experience_partenaire),
    CONSTRAINT FK_RETOUR_EXPERIENCE_PARTENAIRE_PARTENAIRE FOREIGN KEY (id_partenaire) REFERENCES partenaire(id_partenaire),
	CONSTRAINT FK_RETOUR_EXPERIENCE_PARTENAIRE_PRESTATION FOREIGN KEY (id_prestation) REFERENCES prestation(id_prestation),
	CONSTRAINT FK_RETOUR_EXPERIENCE_PARTENAIRE_TYPE_EVALUATION_PARTENAIRE
	FOREIGN KEY (id_type_evaluation_partenaire) REFERENCES type_evaluation_partenaire(id_type_evaluation_partenaire)
	
);

CREATE TABLE location_materiels(
	id_location_materiels int not null auto_increment unique,
	article varchar(255) not null,
	quantite_articles int not null, 
	date_emprunt dateTime not null, 
	date_retour dateTime, 
	montant_caution_en_mads int,
	id_prestation int not null,
	id_masseur int not null,
	PRIMARY KEY (id_location_materiels),
	CONSTRAINT FK_LOCATION_MATERIELS_PRESTATION FOREIGN KEY (id_prestation) REFERENCES prestation (id_prestation),
	CONSTRAINT FK_LOCATION_MATERIELS_MASSEUR FOREIGN KEY (id_masseur) REFERENCES masseur (id_masseur)
	
);

CREATE TABLE motif_fin_contrat_masseur(
id_motif_fin_contrat_masseur int not null auto_increment ,
libelle_motif_fin_contrat_masseur varchar(50),
PRIMARY KEY (id_motif_fin_contrat_masseur)
);

CREATE TABLE contrat_masseur(
id_contrat_masseur int not null auto_increment,
date_debut_contrat_masseur datetime not null,
date_fin_contrat_masseur datetime,
id_masseur int not null,
id_motif_fin_contrat_masseur int,
primary key (id_contrat_masseur),
CONSTRAINT FK_CONTRAT_MASSEUR FOREIGN KEY (id_masseur) REFERENCES masseur(id_masseur),
CONSTRAINT FK_CONTRAT_MASSEUR_MOTIF_FIN_CONTRAT_MASSEUR FOREIGN KEY (id_motif_fin_contrat_masseur) REFERENCES motif_fin_contrat_masseur(id_motif_fin_contrat_masseur)
);




CREATE TABLE type_evaluation_masseur(
id_type_evaluation_masseur int not null auto_increment,
libelle_evaluation_masseur varchar(15),
note_accueil int, 
note_ambiance int, 
note_clientele int,
PRIMARY KEY(id_type_evaluation_masseur)
);



CREATE TABLE retour_experience_masseur(
id_retour_experience_masseur int not null auto_increment,
commentaire text,
date_commentaire datetime not null,
chiffre_affaire double not null,
nombre_masses int not null,
id_masseur int not null,
id_prestation int not null,
id_type_evaluation_masseur int not null,
PRIMARY KEY (id_retour_experience_masseur),
CONSTRAINT FK_RETOUR_EXPERIENCE_MASSEUR_MASSEUR FOREIGN KEY (id_masseur) REFERENCES masseur(id_masseur),
CONSTRAINT FK_RETOUR_EXPERIENCE_MASSEUR_PRESTATION FOREIGN KEY (id_prestation) REFERENCES prestation (id_prestation),
CONSTRAINT FK_RETOUR_EXPERIENCE_MASSEUR_TYPE_EVALUATION_MASSEUR FOREIGN KEY (id_type_evaluation_masseur) REFERENCES type_evaluation_masseur(id_type_evaluation_masseur)

);

CREATE TABLE type_pack_mads(
id_type_pack int not null auto_increment,
libelle_pack varchar(25),
valeur double not null,
PRIMARY KEY(id_type_pack)
);


CREATE TABLE transaction(
id_transaction int not null auto_increment,
nombre_de_pack int not null,
date_transaction datetime not null,
id_masseur int not null,
id_type_pack int not null,
PRIMARY KEY(id_transaction),
CONSTRAINT FK_TRANSACTION_MASSEUR FOREIGN KEY (id_masseur) REFERENCES masseur(id_masseur),
CONSTRAINT FK_TRANSACTION_TYPE_PACK FOREIGN KEY (id_type_pack) REFERENCES type_pack_mads(id_type_pack)
);
CREATE TABLE motif_indisponibilite_masseur(
id_motif_indisponibilite_masseur int auto_increment not null,
motif varchar(255) not null,
PRIMARY KEY (id_motif_indisponibilite_masseur)
);
CREATE TABLE indisponibilite_masseur(
	id_indisponibilite_masseur int auto_increment not null,
	date_debut_indisponibilite dateTime not null,
	date_fin_indisponibilite dateTime,
	id_masseur int not null,
    id_motif_indisponibilite_masseur int,
	PRIMARY KEY (id_indisponibilite_masseur),
	CONSTRAINT FK_INDISPONIBILITE_MASSEUR FOREIGN KEY (id_masseur) REFERENCES masseur(id_masseur),
    CONSTRAINT FK_INDISPONIBILITE_MASSEUR_MOTIF FOREIGN KEY (id_motif_indisponibilite_masseur) REFERENCES motif_indisponibilite_masseur(id_motif_indisponibilite_masseur)
);