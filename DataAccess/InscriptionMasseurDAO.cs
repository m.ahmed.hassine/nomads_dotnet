﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fr.afcepf.ai107.nomads.DataAccess
{
    public class InscriptionMasseurDAO : DAO
    {

        public void UnsubscribeMasseur(int idPrestation, int idMasseur, int idMotif)
        {
            IDbCommand cmd = GetCommand(@"UPDATE inscription_masseurs
                                        SET id_motif_desinscription = @idMotif , date_desinscription = @Date
                                        WHERE (id_prestation = @idPrestation
                                        AND id_masseur = @idMasseur)
                                        ;");
            cmd.Parameters.Add(new MySqlParameter("@idMotif", idMotif));
            cmd.Parameters.Add(new MySqlParameter("@Date", DateTime.Now));
            cmd.Parameters.Add(new MySqlParameter("@idPrestation", idPrestation));
            cmd.Parameters.Add(new MySqlParameter("@idMasseur", idMasseur));

            try
            {
                cmd.Connection.Open();

                cmd.ExecuteNonQuery();

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                cmd.Connection.Close();
            }
        }
    }
}