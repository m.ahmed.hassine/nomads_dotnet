﻿using fr.afcepf.ai107.nomads.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fr.afcepf.ai107.nomads.DataAccess
{
    public class TypePartenaireDAO : DAO
    {
        
        public string GetTypePartenaireById(int idTypePartenaire)
        {
            TypePartenaireEntity typePartenaire = new TypePartenaireEntity();

            IDbCommand cmd = GetCommand("select * from type_partenaire where id_type_partenaire = @id_type_partenaire ");
            cmd.Parameters.Add(new MySqlParameter("@id_type_partenaire", idTypePartenaire));
            try
            {
                cmd.Connection.Open();

                IDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    typePartenaire.IdTypePartenaire = dr.GetInt32(dr.GetOrdinal("id_type_partenaire"));
                    typePartenaire.LibelleTypePartenaire = dr.GetString(dr.GetOrdinal("libelle_type_partenaire"));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return typePartenaire.LibelleTypePartenaire;
        }

        public List<TypePartenaireEntity> GetAllTypePartenaire()
        {
            List<TypePartenaireEntity> listeTypePartenaire = new List<TypePartenaireEntity>();

            IDbCommand cmd = GetCommand("select * from type_partenaire");
            try
            {
                cmd.Connection.Open();

                IDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    listeTypePartenaire.Add(DataReaderToEntity(dr));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return listeTypePartenaire;
        }


        private TypePartenaireEntity DataReaderToEntity(IDataReader dr)
        {
            TypePartenaireEntity typePartenaire = new TypePartenaireEntity
            {
                IdTypePartenaire = dr.GetInt32(dr.GetOrdinal("id_type_partenaire")),
                LibelleTypePartenaire = dr.GetString(dr.GetOrdinal("libelle_type_partenaire")),
            };
            return typePartenaire;
        }

    }
}
