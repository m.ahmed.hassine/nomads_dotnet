﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;

namespace fr.afcepf.ai107.nomads.DataAccess
{
    public abstract class DAO
    {
        private const string connectionString = "Server=127.0.0.1;Database=nomads;Uid=root;Pwd=root;";


        protected IDbCommand GetCommand(string sql)
        {
            IDbConnection cnx = new MySqlConnection
            {
                ConnectionString = connectionString
            };

            IDbCommand cmd = new MySqlCommand();
            cmd.Connection = cnx;
            cmd.CommandText = sql;

            return cmd;
        }




    }
}
