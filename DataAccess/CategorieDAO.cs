﻿using fr.afcepf.ai107.nomads.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fr.afcepf.ai107.nomads.DataAccess
{
    public class CategorieDAO : DAO
    {
        public CategorieEntity GetCategorieByIdTypePartenaire(int idTypePartenaire)
        {
            int idCategorie = 1;
            CategorieEntity categorie = new CategorieEntity();
            if (idTypePartenaire>2)
            {
                idCategorie = idTypePartenaire-1;
            }
            IDbCommand cmd = GetCommand("select * from categorie where id_categorie = @id_categorie ");
            cmd.Parameters.Add(new MySqlParameter("@id_categorie", idCategorie));
            try
            {
                cmd.Connection.Open();

                IDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    categorie.IdCategorie = dr.GetInt32(dr.GetOrdinal("id_categorie"));
                    categorie.LibelleCategorie = dr.GetString(dr.GetOrdinal("libelle_categorie"));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return categorie;
        }

        public int GetIdCategorieByIdMasseur(int idMasseur)
        {
            int idCategorie = 0;
            IDbCommand cmd = GetCommand(@"select apt.id_aptitude, apt.id_categorie 
                                          from aptitude apt 
                                          where id_masseur=@id and 
                                          apt.id_aptitude IN (SELECT Max(apt2.id_aptitude)
                                          from aptitude apt2
                                          where apt.id_masseur = apt2.id_masseur);");
            cmd.Parameters.Add(new MySqlParameter("@id", idMasseur));
            try
            {
                cmd.Connection.Open();

                IDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    idCategorie = dr.GetInt32(dr.GetOrdinal("id_categorie"));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return idCategorie;
        }

        public List<CategorieEntity> GetAllCategorie()
        {
            List<CategorieEntity> listeCategorie = new List<CategorieEntity>();

            IDbCommand cmd = GetCommand("select * from categorie");
            try
            {
                cmd.Connection.Open();

                IDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    listeCategorie.Add(DataReaderToEntity(dr));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return listeCategorie;
        }
        private CategorieEntity DataReaderToEntity(IDataReader dr)
        {
            CategorieEntity categorie = new CategorieEntity
            {
                IdCategorie = dr.GetInt32(dr.GetOrdinal("id_categorie")),
                LibelleCategorie = dr.GetString(dr.GetOrdinal("libelle_categorie")),
            };
            return categorie;
        }
    }
}
