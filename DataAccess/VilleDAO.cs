﻿using MySql.Data.MySqlClient;
using fr.afcepf.ai107.nomads.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fr.afcepf.ai107.nomads.DataAccess
{
    public class VilleDAO : DAO
    {
        public VilleEntity GetVilleByLibelle(string libelle)
        {
            VilleEntity ville = new VilleEntity();

            IDbCommand cmd = GetCommand("select * from ville_cp where libelle_ville = @libelle ");
            cmd.Parameters.Add(new MySqlParameter("@libelle", libelle));
            try
            {
                cmd.Connection.Open();

                IDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    ville.LibelleCP = dr.GetString(dr.GetOrdinal("libelle_cp"));
                    ville.LibelleVille = dr.GetString(dr.GetOrdinal("libelle_ville"));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return ville;
        }
        public VilleEntity GetVilleById(int idVille)
        {
            VilleEntity ville = new VilleEntity();
            
            IDbCommand cmd = GetCommand("select * from ville_cp where id_ville = @id_ville ");
            cmd.Parameters.Add(new MySqlParameter("@id_ville", idVille));
            try
            {
                cmd.Connection.Open();

                IDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    ville.LibelleCP = dr.GetString(dr.GetOrdinal("libelle_cp"));
                    ville.LibelleVille = dr.GetString(dr.GetOrdinal("libelle_ville"));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return ville;
        }
        public List<VilleEntity> GetListeVille()
        {
            List<VilleEntity> listeVille = new List<VilleEntity>();

            IDbCommand cmd = GetCommand("select * from ville_cp ");

            try
            {
                cmd.Connection.Open();

                IDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    listeVille.Add(DataReaderToEntity(dr));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return listeVille;
        }
        private VilleEntity DataReaderToEntity(IDataReader dr)
        {
            VilleEntity ville = new VilleEntity
            {
                IdVille = dr.GetInt32(dr.GetOrdinal("id_ville")),
                LibelleCP = dr.GetString(dr.GetOrdinal("libelle_cp")),
                LibelleVille = dr.GetString(dr.GetOrdinal("libelle_ville")),
        };
            return ville;
        }
    }

}
