﻿using fr.afcepf.ai107.nomads.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fr.afcepf.ai107.nomads.DataAccess
{
    public class MotifDesinscriptionDAO : DAO
    {
        public List<MotifDesinscriptionEntity> GetListeMotifDesinscriptionMasseur()
        {
            IDbCommand cmd = GetCommand(@"Select * 
                                         From motif_desinscription;");

            List<MotifDesinscriptionEntity> liste = new List<MotifDesinscriptionEntity>();
            try
            {
                cmd.Connection.Open();

                IDataReader dr = cmd.ExecuteReader();


                while (dr.Read())
                {
                MotifDesinscriptionEntity motif = new MotifDesinscriptionEntity();
                    motif.IdMotifDesinscription = dr.GetInt32(dr.GetOrdinal("id_motif_desinscription"));
                    motif.LibelleMotif = dr.GetString(dr.GetOrdinal("libelle_motif_desinscription"));
                    liste.Add(motif);
                }
            }catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return liste;
        }
    }
}
