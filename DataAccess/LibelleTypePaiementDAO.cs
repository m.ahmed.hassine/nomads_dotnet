﻿using fr.afcepf.ai107.nomads.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fr.afcepf.ai107.nomads.DataAccess
{
    public class LibelleTypePaiementDAO : DAO
    {
        public LibelleTypePaiementEntity GetLibelleTypePaiementById(int idLibelleTypePaiement)
        {
            LibelleTypePaiementEntity libelleTypePaiement = new LibelleTypePaiementEntity();

            IDbCommand cmd = GetCommand("select * from libelle_type_paiement where id_libelle_type_paiement = @id_libelle_type_paiement ");
            cmd.Parameters.Add(new MySqlParameter("@id_libelle_type_paiement", idLibelleTypePaiement));
            try
            {
                cmd.Connection.Open();

                IDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    libelleTypePaiement.IdLibelleTypePaiement = dr.GetInt32(dr.GetOrdinal("id_libelle_type_paiement"));
                    libelleTypePaiement.Libelle = dr.GetString(dr.GetOrdinal("libelle"));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return libelleTypePaiement;
        }
        
        public List<LibelleTypePaiementEntity> GetListeLibelleTypePaiement()
        {
             List<LibelleTypePaiementEntity> ListeLibelletypePaiement = new List<LibelleTypePaiementEntity>();

                IDbCommand cmd = GetCommand("select * from libelle_type_paiement");

                try
                {
                    cmd.Connection.Open();

                    IDataReader dr = cmd.ExecuteReader();

                    while (dr.Read())
                    {
                    ListeLibelletypePaiement.Add(DataReaderToEntity(dr));
                }
                }
                catch (Exception exc)
                {
                    throw exc;
                }
                finally
                {
                    cmd.Connection.Close();
                }
                return ListeLibelletypePaiement;
            }
        private LibelleTypePaiementEntity DataReaderToEntity(IDataReader dr)
        {
            LibelleTypePaiementEntity libelleTypePaiement = new LibelleTypePaiementEntity
            {
                IdLibelleTypePaiement = dr.GetInt32(dr.GetOrdinal("id_libelle_type_paiement")),
                Libelle = dr.GetString(dr.GetOrdinal("libelle")),
        };
            
            return libelleTypePaiement;
        }
    }
}
