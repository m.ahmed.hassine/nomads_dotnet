﻿using fr.afcepf.ai107.nomads.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fr.afcepf.ai107.nomads.DataAccess
{
    public class MotifFinContratMasseurDAO : DAO
    {
        public List<MotifFinContratMasseurEntity> GetListeLibelleMotifFinContratMasseur()
        {
            List<MotifFinContratMasseurEntity> listeMotifFinContratMasseurEntity = new List<MotifFinContratMasseurEntity>();

            IDbCommand cmd = GetCommand("select * from motif_fin_contrat_masseur ");

            try
            {
                cmd.Connection.Open();

                IDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    listeMotifFinContratMasseurEntity.Add(DataReaderToEntity(dr));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return listeMotifFinContratMasseurEntity;
        }
        private MotifFinContratMasseurEntity DataReaderToEntity(IDataReader dr)
        {
            MotifFinContratMasseurEntity motif = new MotifFinContratMasseurEntity
            {
                IdMotifFinContratMasseur = dr.GetInt32(dr.GetOrdinal("id_motif_fin_contrat_masseur")),
                LibelleMotifFinContratMasseur = dr.GetString(dr.GetOrdinal("libelle_motif_fin_contrat_masseur")),
            };
            return motif;
        }
    }
}
