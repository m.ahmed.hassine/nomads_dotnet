﻿using fr.afcepf.ai107.nomads.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;

namespace fr.afcepf.ai107.nomads.DataAccess
{
    public class PartenaireDAO : DAO
    {
       public DataTable GetAll()
        {

            DataTable partenaire = new DataTable();

            IDbCommand cmd = this.GetCommand(@"SELECT p.id_partenaire, p.nom, p.raison_sociale, p.adresse_mail, p.telephone, p.numero_de_voie, 
                                              p.complement_numero_voie, p.nom_de_voie, p.date_premier_enregistrement, 
                                              v.libelle_cp, v.libelle_ville, t.id_type_partenaire, t.libelle_type_partenaire
                                              FROM nomads.partenaire p
                                              join nomads.ville_cp v on p.id_ville = v.id_ville
                                              join nomads.type_partenaire t on p.id_type_partenaire= t.id_type_partenaire;");

                                              
            MySqlDataAdapter da = new MySqlDataAdapter();
            da.SelectCommand = (MySqlCommand)cmd;
            da.Fill(partenaire);

            return partenaire;

        }

        
        public DataTable GetPartListByPresta(int idPrestation)
        {
            DataTable partPrest = new DataTable();

            IDbCommand cmd = this.GetCommand(@"SELECT pa.nom, pa.raison_sociale
                                             FROM partenaire pa
                                             join prestation pr on pa.id_prestation = pr.id_prestation
                                             where pr.id_prestation =@idPrestation
                                             ;");
            cmd.Parameters.Add(new MySqlParameter("@idPrestation", idPrestation));

            MySqlDataAdapter da = new MySqlDataAdapter();
            da.SelectCommand = (MySqlCommand)cmd;
            da.Fill(partPrest);

            return partPrest;
        }

        public void UpdatePartenaire(PartenaireEntity partenaire)
        {
            IDbCommand cmd = GetCommand(@"UPDATE partenaire 
                                          SET nom=@nom, raison_sociale=@raison_sociale, adresse_mail=@adresse_mail, 
                                        telephone=@telephone, numero_de_voie=@numero_de_voie, complement_numero_voie=@complement_numero_de_voie,
                                        nom_de_voie=@nom_de_voie, id_type_partenaire=@id_type_partenaire,id_ville=@id_ville
                                        WHERE id_partenaire=@id_partenaire");
            cmd.Parameters.Add(new MySqlParameter("@nom", partenaire.Nom));
            cmd.Parameters.Add(new MySqlParameter("@id_type_partenaire", partenaire.IdTypePartenaire));
            cmd.Parameters.Add(new MySqlParameter("@raison_sociale", partenaire.RaisonSociale));
            cmd.Parameters.Add(new MySqlParameter("@adresse_mail", partenaire.AdresseMail));
            cmd.Parameters.Add(new MySqlParameter("@telephone", partenaire.NumeroTelephone));
            cmd.Parameters.Add(new MySqlParameter("@numero_de_voie", partenaire.NumeroVoie));
            cmd.Parameters.Add(new MySqlParameter("@complement_numero_de_voie", partenaire.ComplementNumeroVoie));
            cmd.Parameters.Add(new MySqlParameter("@nom_de_voie", partenaire.NomVoie));
            cmd.Parameters.Add(new MySqlParameter("@id_ville", partenaire.IdVille));
            cmd.Parameters.Add(new MySqlParameter("@id_partenaire", partenaire.IdPartenaire));
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();

            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
        }
        
        public List<PartenaireEntity> GetListePartenaires()
        {
            return GetListePartenairesByIdCategorie(-1);
        }
        public List<PartenaireEntity> GetListePartenairesByIdCategorie(int idCategorie)
        {
            List<PartenaireEntity> listePartenaires = new List<PartenaireEntity>();

            IDbCommand cmd;

            if (idCategorie <=0)
            {
                cmd=GetCommand("select * from partenaire");
            }
            else
            {
                cmd = GetCommand("select * from partenaire where id_type_partenaire=@id_categorie");
                cmd.Parameters.Add(new MySqlParameter("@id_categorie", idCategorie));
            }

            try
            {
                cmd.Connection.Open();

                IDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    listePartenaires.Add(DataReaderToEntity(dr));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return listePartenaires;
        }


        public DataTable GetById(int idPartenaire)
        {
            DataTable partenaire = new DataTable();

            IDbCommand cmd = this.GetCommand(@"SELECT p.id_partenaire, p.nom, p.raison_sociale, p.adresse_mail, p.telephone
                                              FROM nomads.partenaire p
                                              WHERE id_partenaire = @idPartenaire;");

            cmd.Parameters.Add(new MySqlParameter("@idPartenaire", idPartenaire));

            MySqlDataAdapter da = new MySqlDataAdapter();
            da.SelectCommand = (MySqlCommand)cmd;
            da.Fill(partenaire);

            return partenaire;

        }


        private PartenaireEntity DataReaderToEntity(IDataReader dr)
        {
            PartenaireEntity partenaire = new PartenaireEntity();

            partenaire.IdPartenaire = dr.GetInt32(dr.GetOrdinal("id_partenaire"));
           partenaire.IdVille = dr.GetInt32(dr.GetOrdinal("id_ville"));
            partenaire.Nom = dr.GetString(dr.GetOrdinal("nom"));
            partenaire.AdresseMail = dr.GetString(dr.GetOrdinal("adresse_mail"));
            partenaire.NumeroTelephone = dr.GetString(dr.GetOrdinal("telephone"));
            partenaire.NumeroVoie = dr.GetString(dr.GetOrdinal("numero_de_voie"));
            partenaire.RaisonSociale= dr.GetString(dr.GetOrdinal("raison_sociale"));
            if (!dr.IsDBNull(dr.GetOrdinal("complement_numero_voie")))
            {
                partenaire.ComplementNumeroVoie = dr.GetString(dr.GetOrdinal("complement_numero_voie"));

            }
            partenaire.NomVoie = dr.GetString(dr.GetOrdinal("nom_de_voie"));
            partenaire.DatePremierEnregistrement = dr.GetDateTime(dr.GetOrdinal("date_premier_enregistrement"));
            return partenaire;
        }

        public PartenaireEntity GetPartenairesById(int idPartenaire)
        {
            PartenaireEntity partenaire = new PartenaireEntity();

            IDbCommand cmd;

                cmd = GetCommand("select * from partenaire where id_partenaire=@id_partenaire");
                cmd.Parameters.Add(new MySqlParameter("@id_partenaire", idPartenaire));
            
            try
            {
                cmd.Connection.Open();

                IDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    partenaire.IdPartenaire = dr.GetInt32(dr.GetOrdinal("id_partenaire"));
                    partenaire.IdVille = dr.GetInt32(dr.GetOrdinal("id_ville"));
                    partenaire.IdTypePartenaire = dr.GetInt32(dr.GetOrdinal("id_type_partenaire"));
                    partenaire.Nom = dr.GetString(dr.GetOrdinal("nom"));
                    partenaire.AdresseMail = dr.GetString(dr.GetOrdinal("adresse_mail"));
                    partenaire.NumeroTelephone = dr.GetString(dr.GetOrdinal("telephone"));
                    partenaire.NumeroVoie = dr.GetString(dr.GetOrdinal("numero_de_voie"));
                    partenaire.RaisonSociale = dr.GetString(dr.GetOrdinal("raison_sociale"));

                    if (!dr.IsDBNull(dr.GetOrdinal("complement_numero_voie")))
                    {
                        partenaire.ComplementNumeroVoie = dr.GetString(dr.GetOrdinal("complement_numero_voie"));

                    }
                    partenaire.NomVoie = dr.GetString(dr.GetOrdinal("nom_de_voie"));
                    partenaire.DatePremierEnregistrement = dr.GetDateTime(dr.GetOrdinal("date_premier_enregistrement"));
                    return partenaire;
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return partenaire;
        }

        public void AddPartenaireToDB(PartenaireEntity partenaire)
        {
            IDbCommand cmd = GetCommand(@"INSERT INTO partenaire (nom, raison_sociale, adresse_mail, 
                                        telephone, numero_de_voie, complement_numero_voie, nom_de_voie, 
                                        date_premier_enregistrement,id_type_partenaire,id_ville)VALUES(@nom, @raison_sociale,
                                        @adresse_mail, @telephone, @numero_de_voie, @complement_numero_de_voie, 
                                        @nom_de_voie, @date_premier_enregistrement,@id_type_partenaire, @id_ville)");
            cmd.Parameters.Add(new MySqlParameter("@nom", partenaire.Nom));
            cmd.Parameters.Add(new MySqlParameter("@id_type_partenaire", partenaire.IdTypePartenaire));
            cmd.Parameters.Add(new MySqlParameter("@raison_sociale", partenaire.RaisonSociale));
            cmd.Parameters.Add(new MySqlParameter("@adresse_mail", partenaire.AdresseMail));
            cmd.Parameters.Add(new MySqlParameter("@telephone", partenaire.NumeroTelephone));
            cmd.Parameters.Add(new MySqlParameter("@numero_de_voie", partenaire.NumeroVoie));
            cmd.Parameters.Add(new MySqlParameter("@complement_numero_de_voie", partenaire.ComplementNumeroVoie));
            cmd.Parameters.Add(new MySqlParameter("@nom_de_voie", partenaire.NomVoie));
            cmd.Parameters.Add(new MySqlParameter("@date_premier_enregistrement", partenaire.DatePremierEnregistrement));
            cmd.Parameters.Add(new MySqlParameter("@id_ville", partenaire.IdVille));

            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();

            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
        }

        //récup liste partenaire ddl voir prestation
        public DataTable GetPartenaireWithPrestation()
        {
            DataTable data = new DataTable();

            IDbCommand cmd = GetCommand(@"
SELECT DISTINCT p.id_partenaire, p.nom from partenaire p
inner join prestation a on a.id_partenaire = p.id_partenaire
where a.date_annulation is null 
and a.date_prestation >= now()");
        

            MySqlDataAdapter da = new MySqlDataAdapter();
            da.SelectCommand = (MySqlCommand)cmd;
            da.Fill(data);

            return data;
        }
        //récup liste partenaire histo ddl voir prestation
        public DataTable GetPartenaireWithPrestationForHisto()
        {
            DataTable data = new DataTable();

            IDbCommand cmd = GetCommand(@"SELECT DISTINCT p.id_partenaire, p.nom from partenaire p
inner join prestation a on a.id_partenaire = p.id_partenaire
where a.date_annulation is null 
and a.date_prestation < now()");


            MySqlDataAdapter da = new MySqlDataAdapter();
            da.SelectCommand = (MySqlCommand)cmd;
            da.Fill(data);

            return data;
        }
        //essai à voir samedi
        public List<PartenaireEntity> GetPartenaireWithPrestationOk()
        {
            List<PartenaireEntity> result = new List<PartenaireEntity>();

            // 2 - Préparer une commande SQL
            IDbCommand cmd = GetCommand(@"select distinct p.nom, p.id_partenaire, p.raison_sociale, p.adresse_mail,
p.telephone, p.numero_de_voie, p.complement_numero_voie, p.nom_de_voie,
p.date_premier_enregistrement, p.id_ville, p.id_type_partenaire from partenaire p
join prestation a on a.id_partenaire = p.id_partenaire
where a.date_annulation is null 
and a.date_prestation >= now()");

            try
            {
                // 3 - Ouvrir la connection
                cmd.Connection.Open();

                // 4 - Exécuter la commande
                IDataReader dr = cmd.ExecuteReader();

                // 5 - Traiter le résultat
                while (dr.Read())
                {
                    result.Add(DataReaderToEntity(dr));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                // 6 - Fermer la connection
                cmd.Connection.Close();
            }
            return result;
        }
        public List<PartenaireEntity> GetPartenaireWithPrestationHistoOk()
        {
            List<PartenaireEntity> result = new List<PartenaireEntity>();

            // 2 - Préparer une commande SQL
            IDbCommand cmd = GetCommand(@"select distinct p.nom, p.id_partenaire, p.raison_sociale, p.adresse_mail,
p.telephone, p.numero_de_voie, p.complement_numero_voie, p.nom_de_voie,
p.date_premier_enregistrement, p.id_ville, p.id_type_partenaire from partenaire p
join prestation a on a.id_partenaire = p.id_partenaire
where a.date_annulation is null 
and a.date_prestation < now()");

            try
            {
                // 3 - Ouvrir la connection
                cmd.Connection.Open();

                // 4 - Exécuter la commande
                IDataReader dr = cmd.ExecuteReader();

                // 5 - Traiter le résultat
                while (dr.Read())
                {
                    result.Add(DataReaderToEntity(dr));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                // 6 - Fermer la connection
                cmd.Connection.Close();
            }
            return result;
        }
    }
    }

