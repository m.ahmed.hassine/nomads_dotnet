﻿using fr.afcepf.ai107.nomads.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fr.afcepf.ai107.nomads.DataAccess
{
    public class TypeEvaluationPartenaireDAO : DAO
    {
        public List<TypeEvaluationPartenaireEntity> GetListeEvaluationPartenaire()
        {
            IDbCommand cmd = GetCommand(@"Select * 
                                         From type_evaluation_partenaire;");

            List<TypeEvaluationPartenaireEntity> liste = new List<TypeEvaluationPartenaireEntity>();
            try
            {
                cmd.Connection.Open();

                IDataReader dr = cmd.ExecuteReader();


                while (dr.Read())
                {
                    TypeEvaluationPartenaireEntity type = new TypeEvaluationPartenaireEntity();
                    type.IdTypeEvaluationPartenaire = dr.GetInt32(dr.GetOrdinal("id_type_evaluation_partenaire"));
                    type.LibelleTypeEvaluationPartenaire = dr.GetString(dr.GetOrdinal("libelle_evaluation_partenaire"));
                    liste.Add(type);
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return liste;
        }


    }
}
