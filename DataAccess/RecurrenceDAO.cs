﻿using fr.afcepf.ai107.nomads.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;

namespace fr.afcepf.ai107.nomads.DataAccess
{
    public class RecurrenceDAO : DAO
    {
        public int GetLastIdRecurrence()
        {
            RecurrenceEntity recurrence = new RecurrenceEntity();
            int id = 0;
            IDbCommand cmd = GetCommand("select Max(id_recurrence) as max_id from recurrence ");

            try
            {
                cmd.Connection.Open();

                IDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    id = dr.GetInt32(dr.GetOrdinal("max_id"));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return id;
        }
        public void AddReccurence(RecurrenceEntity recurrence)
        {
            IDbCommand cmd = GetCommand(@"INSERT INTO recurrence (id_libelle_recurrence, id_libelle_duree_recurrence,pas, duree_recurrence, 
                                        delta) VALUES(@idLibelleRecurrence, @idDureeRecurrence,@pas, @dureeRecurrence,
                                        @delta)");

            cmd.Parameters.Add(new MySqlParameter("@idLibelleRecurrence", recurrence.IdLibelleRecurrence));
            cmd.Parameters.Add(new MySqlParameter("@idDureeRecurrence", recurrence.IdLibelleDureeRecurrence));
            cmd.Parameters.Add(new MySqlParameter("@pas", recurrence.Pas));
            cmd.Parameters.Add(new MySqlParameter("@dureeRecurrence", recurrence.DureeRecurrence));
            cmd.Parameters.Add(new MySqlParameter("@delta", recurrence.Delta));

            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }

        }
        public List<RecurrenceEntity> GetListeRecurrence()
        {
            List<RecurrenceEntity> listeRecurrence = new List<RecurrenceEntity>();
            
            IDbCommand cmd = GetCommand("select * from recurrence ");

            try
            {
                cmd.Connection.Open();

                IDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    listeRecurrence.Add(DataReaderToEntity(dr));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return listeRecurrence;
        }
        private RecurrenceEntity DataReaderToEntity(IDataReader dr)
        {
            RecurrenceEntity recurrence = new RecurrenceEntity
            {
                IdRecurrence = dr.GetInt32(dr.GetOrdinal("id_recurrence")),
                IdLibelleRecurrence = dr.GetInt32(dr.GetOrdinal("id_libelle_recurrence")),
                Pas = dr.GetInt32(dr.GetOrdinal("pas")),
                IdLibelleDureeRecurrence = dr.GetInt32(dr.GetOrdinal("id_libelle_duree_recurrence")),
                DureeRecurrence = dr.GetInt32(dr.GetOrdinal("duree_recurrence")),
                Delta = dr.GetInt32(dr.GetOrdinal("delta")),
            };
            return recurrence;
        }

    }
}
