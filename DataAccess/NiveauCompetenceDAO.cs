﻿using fr.afcepf.ai107.nomads.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fr.afcepf.ai107.nomads.DataAccess
{
    public class NiveauCompetenceDAO : DAO
    {

        public NiveauCompetenceEntity GetNiveauCompetenceByLibelle(string libelleNiveauCompetence)
        {
            NiveauCompetenceEntity niveauCompetence = new NiveauCompetenceEntity();

            IDbCommand cmd = GetCommand("select * from niveau_competence where libelle_niveau_competence = @libelle ");
            cmd.Parameters.Add(new MySqlParameter("@libelle", libelleNiveauCompetence));
            try
            {
                cmd.Connection.Open();

                IDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    niveauCompetence.LibelleNiveauCompetence = dr.GetString(dr.GetOrdinal("libelle_niveau_competence"));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return niveauCompetence;
        }
        public NiveauCompetenceEntity GetNiveauCompetenceById(int idStatut)
        {
            NiveauCompetenceEntity niveauCompetence = new NiveauCompetenceEntity();

            IDbCommand cmd = GetCommand("select * from niveau_competence where id_statut = @id_statut ");
            cmd.Parameters.Add(new MySqlParameter("@id_statut", idStatut));
            try
            {
                cmd.Connection.Open();

                IDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    niveauCompetence.LibelleNiveauCompetence = dr.GetString(dr.GetOrdinal("libelle_niveau_competence"));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return niveauCompetence;
        }
        public List<NiveauCompetenceEntity> GetListeNiveauCompetence()
        {
            List<NiveauCompetenceEntity> listeNiveauCompetence = new List<NiveauCompetenceEntity>();

            IDbCommand cmd = GetCommand("select * from niveau_competence");

            try
            {
                cmd.Connection.Open();

                IDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    listeNiveauCompetence.Add(DataReaderToEntity(dr));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return listeNiveauCompetence;
        }
        private NiveauCompetenceEntity DataReaderToEntity(IDataReader dr)
        {
            NiveauCompetenceEntity niveauCompetence = new NiveauCompetenceEntity
            {
                IdStatut = dr.GetInt32(dr.GetOrdinal("id_statut")),
                LibelleNiveauCompetence = dr.GetString(dr.GetOrdinal("libelle_niveau_competence")),
            };
            return niveauCompetence;
        }
    }
}
