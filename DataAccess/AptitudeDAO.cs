﻿using fr.afcepf.ai107.nomads.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fr.afcepf.ai107.nomads.DataAccess
{
    public class AptitudeDAO : DAO
    {
        public AptitudeEntity GetAptitudeById(int idAptitude)
        {
            AptitudeEntity aptitude = new AptitudeEntity();

            IDbCommand cmd = GetCommand("select * from aptitude where id_aptitude = @id ");
            cmd.Parameters.Add(new MySqlParameter("@id", idAptitude));
            try
            {
                cmd.Connection.Open();

                IDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    aptitude.IdCategorie = dr.GetInt32(dr.GetOrdinal("id_categorie"));
                    aptitude.DateDebut = dr.GetDateTime(dr.GetOrdinal("date_debut_aptitude"));
                    aptitude.DateFin = dr.GetDateTime(dr.GetOrdinal("date_fin_aptitude"));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return aptitude;
        }
        public void AddAptitude(AptitudeEntity aptitude)
        {
            IDbCommand cmd = GetCommand(@"INSERT INTO aptitude (id_categorie,date_debut_aptitude,id_masseur) 
                                        VALUES(@id_categorie,@date_debut_aptitude,@id_masseur)");

            cmd.Parameters.Add(new MySqlParameter("@id_categorie", aptitude.IdCategorie));
            cmd.Parameters.Add(new MySqlParameter("@date_debut_aptitude", aptitude.DateDebut));
            cmd.Parameters.Add(new MySqlParameter("@id_masseur", aptitude.IdMasseur));
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
        }
        public int UpDateAptitude(AptitudeEntity aptitude)
        {
            IDbCommand cmd = GetCommand(@"UPDATE aptitude 
                                          SET date_fin_aptitude=@date_fin_aptitude
                                        WHERE id_aptitude=@id_aptitude");
            cmd.Parameters.Add(new MySqlParameter("@date_fin_aptitude", DateTime.Now));
            cmd.Parameters.Add(new MySqlParameter("@id_aptitude", aptitude.IdAptitude));
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();

            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
            AddAptitude(aptitude);
            int nvIdAptitude = GetLastIdAptitude();
            return nvIdAptitude;
        }
        public int GetLastIdAptitude()
        {
            AptitudeEntity aptitude = new AptitudeEntity();
            int id = 0;
            IDbCommand cmd = GetCommand("select Max(id_aptitude) as max_id from aptitude ");

            try
            {
                cmd.Connection.Open();

                IDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    id = dr.GetInt32(dr.GetOrdinal("max_id"));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return id;
        }
        public void SetEndAptitude(AptitudeEntity aptitude)
        {
            IDbCommand cmd = GetCommand(@"UPDATE aptitude 
                                          SET date_fin_aptitude=@date_fin_aptitude
                                        WHERE id_aptitude=@id_aptitude");
            cmd.Parameters.Add(new MySqlParameter("@date_fin_aptitude", DateTime.Now));
            cmd.Parameters.Add(new MySqlParameter("@id_aptitude", aptitude.IdAptitude));
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();

            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
        }
    }
}
