﻿using fr.afcepf.ai107.nomads.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fr.afcepf.ai107.nomads.DataAccess
{
    public class RetourPartenaireDAO : DAO
    {

        public void Insert(RetourExperiencePartenaireEntity retourPartenaire, int idTypeEvaluationPartenaire)
        {
            IDbCommand cmd = GetCommand(@"INSERT INTO retour_experience_partenaire
                                        (libelle_retour_experience_partenaire, date_retour_experience_partenaire, id_partenaire, id_prestation, id_type_evaluation_partenaire) VALUES 
                                        (@libelle_retour_experience_partenaire, @date_retour_experience_partenaire, @id_partenaire, @id_prestation, @id_type_evaluation_partenaire)");

            cmd.Parameters.Add(new MySqlParameter("@libelle_retour_experience_partenaire", retourPartenaire.Commentaire));
            cmd.Parameters.Add(new MySqlParameter("@date_retour_experience_partenaire", DateTime.Now));
            cmd.Parameters.Add(new MySqlParameter("@id_partenaire", retourPartenaire.IdPartenaire));
            cmd.Parameters.Add(new MySqlParameter("@id_prestation", retourPartenaire.IdPrestation));
            cmd.Parameters.Add(new MySqlParameter("@id_type_evaluation_partenaire", idTypeEvaluationPartenaire));


            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
        }
    }
}
