﻿using fr.afcepf.ai107.nomads.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fr.afcepf.ai107.nomads.DataAccess
{
    public class LibelleRecurrenceDAO : DAO
    {
        public int GetIdLibelleRecurrenceByLibelle(string libelle)
        {
            int id=0;

            IDbCommand cmd = GetCommand("select * from libelle_recurrence where libelle=@Libelle");
            cmd.Parameters.Add(new MySqlParameter("@libelle", libelle));
            try
            {
                cmd.Connection.Open();

                IDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    id = dr.GetInt32(dr.GetOrdinal("id_libelle_recurrence"));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return id;
        }
        public List<LibelleRecurrenceEntity> GetListeLibelleRecurrence()
        {
            List<LibelleRecurrenceEntity> listeLibelleRecurrence = new List<LibelleRecurrenceEntity>();

            IDbCommand cmd = GetCommand("select * from libelle_recurrence ");

            try
            {
                cmd.Connection.Open();

                IDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    listeLibelleRecurrence.Add(DataReaderToEntity(dr));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return listeLibelleRecurrence;
        }
        private LibelleRecurrenceEntity DataReaderToEntity(IDataReader dr)
        {
            LibelleRecurrenceEntity recurrence = new LibelleRecurrenceEntity
            {
                Id = dr.GetInt32(dr.GetOrdinal("id_libelle_recurrence")),
                Libelle = dr.GetString(dr.GetOrdinal("libelle")),
            };
            return recurrence;
        }
    }
}
