﻿using fr.afcepf.ai107.nomads.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fr.afcepf.ai107.nomads.DataAccess
{
    public class ContratPartenaireDAO : DAO
    {
        public void SetEndContratPartenaire(ContratPartenaireEntity contratPartenaire)
        {
            IDbCommand cmd = GetCommand(@"UPDATE contrat_partenaire 
                                          SET date_fin_contrat_partenaire=@date_fin_contrat_partenaire,
                                              id_motif_fin_contrat_partenaire=@id_motif_fin_contrat_partenaire
                                        WHERE id_contrat_partenaire=@id_contrat_partenaire");
            cmd.Parameters.Add(new MySqlParameter("@date_fin_contrat_partenaire", DateTime.Now));
            cmd.Parameters.Add(new MySqlParameter("@id_contrat_partenaire", contratPartenaire.IdContratPartenaire));
            cmd.Parameters.Add(new MySqlParameter("@id_motif_fin_contrat_partenaire", contratPartenaire.IdMotifFinContrat));
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();

            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
        }
        public int GetLastIdContratPartenaire()
        {
            ContratPartenaireEntity contratPartenaire = new ContratPartenaireEntity();
            int id = 0;
            IDbCommand cmd = GetCommand("select Max(id_contrat_partenaire) as max_id from contrat_partenaire ");

            try
            {
                cmd.Connection.Open();

                IDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    id = dr.GetInt32(dr.GetOrdinal("max_id"));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return id;
        }
        public void AddContratPartenaire(ContratPartenaireEntity contratPartenaire)
        {
            IDbCommand cmd = GetCommand(@"INSERT INTO contrat_partenaire (date_debut_contrat_partenaire,id_partenaire) 
                                        VALUES(@date_debut_contrat_partenaire,@id_partenaire)");

            cmd.Parameters.Add(new MySqlParameter("@date_debut_contrat_partenaire", DateTime.Now));
            cmd.Parameters.Add(new MySqlParameter("@id_partenaire", contratPartenaire.IdPartenaire));
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
        }
        public ContratPartenaireEntity GetContratPartenaireById(int idContratPartenaire)
        {
            ContratPartenaireEntity contrat = new ContratPartenaireEntity();

            IDbCommand cmd = GetCommand("select * from contrat_partenaire where id_contrat_partenaire = @id ");
            cmd.Parameters.Add(new MySqlParameter("@id", idContratPartenaire));
            try
            {
                cmd.Connection.Open();

                IDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    contrat.DateDebutContratPartenaire = dr.GetDateTime(dr.GetOrdinal("date_debut_contrat_partenaire"));
                    contrat.IdMotifFinContrat = dr.GetInt32(dr.GetOrdinal("id_motif_fin_contrat_partenaire"));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return contrat;
        }
    }
}
