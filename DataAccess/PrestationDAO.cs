﻿using fr.afcepf.ai107.nomads.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using fr.afcepf.ai107.nomads.Entities;
using MySql.Data.MySqlClient;

namespace fr.afcepf.ai107.nomads.DataAccess
{
    public class PrestationDAO : DAO
    {
        //modifiable
        public List<PrestationEntity> GetAll()
        {
            List<PrestationEntity> completeList = new List<PrestationEntity>();

            IDbCommand cmd = GetCommand(@"select p.nom 'nom_partenaire', a.id_prestation, a.libelle, a.date_prestation,
                                          a.heure_debut, a.heure_fin, a.nombre_masseurs, a.nombre_chaises_ergonomiques, 
                                        a.cout_mads, a.montant_remuneration, a.date_annulation, a.date_paiement, 
                                        a.numero_de_voie, a.complement_numero_voie, a.nom_de_voie, a.specificites,
                                        a.id_partenaire, a.id_recurrence, a.id_motif_annulation, a.id_type_paiement,
                                        a.id_ville from prestation a 
                                        join partenaire p on a.id_partenaire = p.id_partenaire
                                        where a.date_annulation is null
                                        and a.date_prestation >= now()");

            try
            {
                cmd.Connection.Open();
                IDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                   
                   completeList.Add(DataReaderToEntity(dr));
                }

            }

            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }

            return completeList;
        }
        // pour gridview de voir prestation
        public DataTable GetPrestationWithNomPartenaire()
        {

            DataTable result = new DataTable();

            IDbCommand cmd = GetCommand(@"select p.nom 'nom_partenaire', a.id_prestation, a.libelle, DATE_FORMAT(a.date_prestation,GET_FORMAT(date,'EUR')),
                                          a.heure_debut, a.heure_fin, a.nombre_masseurs, a.nombre_chaises_ergonomiques, 
                                        a.cout_mads, a.montant_remuneration, a.date_annulation, a.date_paiement, 
                                        a.numero_de_voie, a.complement_numero_voie, a.nom_de_voie, a.specificites,
                                        a.id_partenaire, a.id_recurrence, a.id_motif_annulation, a.id_type_paiement,
                                        a.id_ville from prestation a 
                                        join partenaire p on a.id_partenaire = p.id_partenaire
                                        where a.date_annulation is null
                                        and a.date_prestation >= now() order by nom");


            MySqlDataAdapter da = new MySqlDataAdapter();
            da.SelectCommand = (MySqlCommand)cmd;

            // remplir la datatable :
            da.Fill(result);

            return result;
        }
        //pour grid historique de voir presta 
        public DataTable GetHistoriquePrestationWithNomPartenaire()
        {

            DataTable result = new DataTable();

            IDbCommand cmd = GetCommand(@"select p.nom 'nom_partenaire', a.id_prestation, a.libelle, DATE_FORMAT(a.date_prestation,GET_FORMAT(date,'EUR')),
                                          a.heure_debut, a.heure_fin, a.nombre_masseurs, a.nombre_chaises_ergonomiques, 
                                        a.cout_mads, a.montant_remuneration, a.date_annulation, a.date_paiement, 
                                        a.numero_de_voie, a.complement_numero_voie, a.nom_de_voie, a.specificites,
                                        a.id_partenaire, a.id_recurrence, a.id_motif_annulation, a.id_type_paiement,
                                        a.id_ville from prestation a 
                                        join partenaire p on a.id_partenaire = p.id_partenaire
                                        where a.date_annulation is null
                                        and a.date_prestation < now() order by nom");


            MySqlDataAdapter da = new MySqlDataAdapter();
            da.SelectCommand = (MySqlCommand)cmd;

            // remplir la datatable :
            da.Fill(result);

            return result;
        }
        public void CreateEvent(PrestationEntity prestation)
        {
            IDbCommand cmd = GetCommand(@"INSERT INTO prestation (libelle, date_prestation, heure_debut,heure_fin,
                                        nombre_masseurs, nombre_chaises_ergonomiques, cout_mads, montant_remuneration,
                                        date_paiement, numero_de_voie, complement_numero_voie,
                                        nom_de_voie, id_partenaire, id_recurrence, id_type_paiement, id_ville, specificites) 
                                        VALUES(@libelle, @datePrestation, @heureDebut, @heureFin, @nombreMasseurs, @nombreChaisesErgonomiques, @coutMads,
                                         @montantRemuneration, @datePaiement, @numeroVoie, @complementNumVoie, @nomVoie, @idPartenaire,@idRecurrence, @idTypePaiement, @idVille, 
                                        @specificites)");

            cmd.Parameters.Add(new MySqlParameter("@libelle", prestation.Libelle));
            cmd.Parameters.Add(new MySqlParameter("@datePrestation", prestation.DatePrestation));
            cmd.Parameters.Add(new MySqlParameter("@heureDebut", prestation.HeureDebut));
            cmd.Parameters.Add(new MySqlParameter("@heureFin", prestation.HeureFin));
            cmd.Parameters.Add(new MySqlParameter("@nombreMasseurs", prestation.NombreMasseurs));
            cmd.Parameters.Add(new MySqlParameter("@nombreChaisesErgonomiques", prestation.NombreChaisesErgonomiques));
            cmd.Parameters.Add(new MySqlParameter("@coutMads", prestation.CoutMads));
            cmd.Parameters.Add(new MySqlParameter("@montantRemuneration", prestation.MontantRemuneration));
            cmd.Parameters.Add(new MySqlParameter("@datePaiement", prestation.DatePaiement));
            cmd.Parameters.Add(new MySqlParameter("@numeroVoie", prestation.NumeroVoie));
            cmd.Parameters.Add(new MySqlParameter("@complementNumVoie", prestation.ComplementNumVoie));
            cmd.Parameters.Add(new MySqlParameter("@nomVoie", prestation.NomVoie));
            cmd.Parameters.Add(new MySqlParameter("@idPartenaire", prestation.IdPartenaire));
            cmd.Parameters.Add(new MySqlParameter("@idRecurrence", prestation.IdRecurrence));
            cmd.Parameters.Add(new MySqlParameter("@idTypePaiement", prestation.IdTypePaiement));
            cmd.Parameters.Add(new MySqlParameter("@idVille", prestation.IdVille));
            cmd.Parameters.Add(new MySqlParameter("@specificites", prestation.Specificites));

            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
        }
        public DataTable GetDetailsPrestationDispoByIdAndDate(int idMasseur, DateTime date)
        {
            DataTable result = new DataTable();

            IDbCommand cmd = GetCommand(@"SELECT pr.id_prestation, pr.libelle, DATE_FORMAT(pr.date_prestation, GET_FORMAT(DATE, 'EUR')),pr.heure_debut, pr.heure_fin,  pr.cout_mads, pr.montant_remuneration
									        FROM nomads.prestation pr                                                     
                                            LEFT JOIN
                                                   (select pr2.id_prestation, pr2.libelle, pr2.date_prestation,pr2.heure_debut, pr2.heure_fin,  pr2.cout_mads, pr2.montant_remuneration
													from prestation pr2
                                                    join inscription_masseurs i on i.id_prestation = pr2.id_prestation
                                                    where i.id_masseur = @id_masseur) pr3 on pr.id_prestation = pr3.id_prestation
                                                                                
											WHERE pr.date_annulation is null /* gère les événements annulés */
                                            AND pr3.id_prestation is null 
                                            AND pr.date_prestation > @date
                                            ORDER BY pr.id_prestation ;");

            cmd.Parameters.Add(new MySqlParameter("@id_masseur", idMasseur));
            cmd.Parameters.Add(new MySqlParameter("@date", date));
            //ATTENTION: Gérer par la suite les problèmes d'horaires

            MySqlDataAdapter da = new MySqlDataAdapter();
            da.SelectCommand = (MySqlCommand) cmd;
            da.Fill(result);

            return result;

        }
        public DataTable GetByLibellePartenaireDate(int idPartenaire, DateTime now)
        {
            DataTable table = new DataTable();
            IDbCommand cmd = GetCommand(@"SELECT p.id_prestation, p.libelle, p.date_prestation, p.heure_debut, p.heure_fin, p.nombre_masseurs
                                        FROM prestation p
                                        where id_partenaire = @idPartenaire
                                        and date_prestation < @date; ");

            cmd.Parameters.Add(new MySqlParameter("@idPartenaire", idPartenaire));
            cmd.Parameters.Add(new MySqlParameter("@date", now));


            MySqlDataAdapter da = new MySqlDataAdapter();
            da.SelectCommand = (MySqlCommand)cmd;
            da.Fill(table);
            return table;
        }
        public DataTable GetDetailsPrestation()
        {

            DataTable result = new DataTable();

            IDbCommand cmd = GetCommand(@"SELECT pr.id_prestation, pr.libelle, pr.date_prestation,pr.heure_debut, pr.heure_fin, pr.nombre_masseurs, pr.nombre_chaises_ergonomiques, pr.cout_mads, 
                                                    pr.montant_remuneration,pr.date_annulation, 
											        map.libelle_motif_annulation, 
                                                    pr.date_paiement, pr.numero_de_voie, pr.complement_numero_voie, pr.nom_de_voie,
                                                    v.libelle_cp, v.libelle_ville, 
                                                    pa.nom, 
                                                    typ.libelle_type_partenaire, 
                                                    librec.libelle,
                                                    libtyppay.libelle, 
                                                    pa.telephone                                                    
                                                    FROM nomads.prestation pr
											        left join motif_annulation_prestation map on pr.id_motif_annulation = map.id_motif_annulation
                                                    join ville_cp v on pr.id_ville = v.id_ville
                                                    join nomads.partenaire pa on pr.id_partenaire = pa.id_partenaire
                                                    join type_partenaire typ on pa.id_type_partenaire = typ.id_type_partenaire
                                                    join recurrence recur on pr.id_recurrence = recur.id_recurrence
                                                    join libelle_recurrence librec on recur.id_libelle_recurrence = librec.id_libelle_recurrence
                                                    join type_paiement typpay on pr.id_type_paiement = typpay.id_type_paiement
                                                    join libelle_type_paiement libtyppay on typpay.id_libelle_type_paiement = libtyppay.id_libelle_type_paiement
                                                    ORDER BY pr.id_prestation ;");


            MySqlDataAdapter da = new MySqlDataAdapter();
            da.SelectCommand = (MySqlCommand)cmd;

            // remplir la datatable :
            da.Fill(result);

            return result;
        }
        public DataTable GetByMasseur(int idMasseur)
        {
            DataTable prestaMasseur = new DataTable();

            IDbCommand cmd = GetCommand(@"select i.id_prestation, p.libelle, p.date_prestation, p.heure_debut, p.heure_fin, p.nombre_masseurs, p.cout_mads, p.montant_remuneration,
		                                p.numero_de_voie, p.complement_numero_voie, p.nom_de_voie, 
                                        v.libelle_cp, v.libelle_ville, 
                                        par.nom, par.telephone,
                                        libtyppay.libelle
                                        from inscription_masseurs i
                                        inner join prestation p on i.id_prestation = p.id_prestation
                                        inner join ville_cp v on p.id_ville = v.id_ville
                                        inner join partenaire par on p.id_partenaire = par.id_partenaire
                                        inner join type_paiement tp on p.id_type_paiement = tp.id_type_paiement
                                        inner join libelle_type_paiement libtyppay on tp.id_libelle_type_paiement = libtyppay.id_libelle_type_paiement
                                        where id_masseur = @idMasseur ;");

            cmd.Parameters.Add(new MySqlParameter("@idMasseur", idMasseur));


            MySqlDataAdapter da = new MySqlDataAdapter();
            da.SelectCommand = (MySqlCommand)cmd;
            da.Fill(prestaMasseur);

            return prestaMasseur;
        }
        public DataTable GetByMasseurDateAnt(int idMasseur, DateTime now) // voir pour surcharge avec précédente fonction?
        {
            DataTable prestaMasseur = new DataTable();

            IDbCommand cmd = GetCommand(@"select i.id_prestation, p.libelle, p.date_prestation, p.heure_debut, p.heure_fin, p.nombre_masseurs, p.cout_mads, p.montant_remuneration,
		                                p.numero_de_voie, p.complement_numero_voie, p.nom_de_voie, 
                                        v.libelle_cp, v.libelle_ville, 
                                        par.nom, par.telephone,
                                        libtyppay.libelle
                                        from inscription_masseurs i
                                        inner join prestation p on i.id_prestation = p.id_prestation
                                        inner join ville_cp v on p.id_ville = v.id_ville
                                        inner join partenaire par on p.id_partenaire = par.id_partenaire
                                        inner join type_paiement tp on p.id_type_paiement = tp.id_type_paiement
                                        inner join libelle_type_paiement libtyppay on tp.id_libelle_type_paiement = libtyppay.id_libelle_type_paiement
                                        where id_masseur = @idMasseur
                                        and date_prestation < @date;");

            cmd.Parameters.Add(new MySqlParameter("@idMasseur", idMasseur));
            cmd.Parameters.Add(new MySqlParameter("@date", now));


            MySqlDataAdapter da = new MySqlDataAdapter();
            da.SelectCommand = (MySqlCommand)cmd;
            da.Fill(prestaMasseur);

            return prestaMasseur;
        }
        public DataTable GetByMasseurDateFut(int idMasseur, DateTime now)
        {
            DataTable prestaMasseur = new DataTable();

            IDbCommand cmd = GetCommand(@"select i.id_prestation, p.libelle, p.date_prestation, p.heure_debut, p.heure_fin, p.nombre_masseurs, p.cout_mads, p.montant_remuneration,
		                                p.numero_de_voie, p.complement_numero_voie, p.nom_de_voie, 
                                        v.libelle_cp, v.libelle_ville, 
                                        par.nom, par.telephone,
                                        libtyppay.libelle
                                        from inscription_masseurs i
                                        inner join prestation p on i.id_prestation = p.id_prestation
                                        inner join ville_cp v on p.id_ville = v.id_ville
                                        inner join partenaire par on p.id_partenaire = par.id_partenaire
                                        inner join type_paiement tp on p.id_type_paiement = tp.id_type_paiement
                                        inner join libelle_type_paiement libtyppay on tp.id_libelle_type_paiement = libtyppay.id_libelle_type_paiement
                                        where id_masseur = @idMasseur
                                        and i.date_desinscription is null
                                        and date_prestation > @date;");

            cmd.Parameters.Add(new MySqlParameter("@idMasseur", idMasseur));
            cmd.Parameters.Add(new MySqlParameter("@date", now));


            MySqlDataAdapter da = new MySqlDataAdapter();
            da.SelectCommand = (MySqlCommand)cmd;
            da.Fill(prestaMasseur);

            return prestaMasseur;
        }
        public DataTable GetByLibellePartenaire(int idPartenaire)
        {
            DataTable table = new DataTable();
            IDbCommand cmd = GetCommand(@"select distinct t.libelle_type_partenaire 
                                            from type_partenaire t inner join partenaire p
                                            on t.id_type_partenaire = p.id_type_partenaire
                                            inner join prestation a 
                                            on a.id_partenaire = p.id_partenaire
                                            where id_Partenaire=@idPartenaire");

            cmd.Parameters.Add(new MySqlParameter("@idPartenaire", idPartenaire));

            MySqlDataAdapter da = new MySqlDataAdapter();
            da.SelectCommand = (MySqlCommand)cmd;
            da.Fill(table);
            return table;
        }
        public DataTable GetByLibellePartenaire() // Comment faire surcharge méthode précédente?
        {
            DataTable table = new DataTable();
            IDbCommand cmd = GetCommand(@"select distinct t.libelle_type_partenaire 
                                            from type_partenaire t inner join partenaire p
                                            on t.id_type_partenaire = p.id_type_partenaire
                                            inner join prestation a 
                                            on a.id_partenaire = p.id_partenaire");

            MySqlDataAdapter da = new MySqlDataAdapter();
            da.SelectCommand = (MySqlCommand)cmd;
            da.Fill(table);
            return table;
        }
        public List<PrestationEntity> GetPrestationByIdTypePartenaire(int IdTypePartenaire)
        {
            List<PrestationEntity> prestations = new List<PrestationEntity>();

            IDbCommand cmd = GetCommand(@"select * from prestation a
                                           inner join partenaire p
                                           on a.id_partenaire = p.id_type_partenaire");

            cmd.Parameters.Add(new MySqlParameter("IdTypePartenaire", IdTypePartenaire));

            try
            {
                cmd.Connection.Open();

                IDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    prestations.Add(DataReaderToEntity(dr));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return prestations;
        }
        // pour lier drop down list partenaire et libelle 
        public List<PrestationEntity> GetPrestationByIdPartenaire(int idPartenaire) //différence avec précédent?
        {
            List<PrestationEntity> prestations = new List<PrestationEntity>();

            IDbCommand cmd = GetCommand(@"select * from prestation 
                                           where id_partenaire = @idPartenaire");

            cmd.Parameters.Add(new MySqlParameter("idPartenaire", idPartenaire));

            try
            {
                cmd.Connection.Open();

                IDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    prestations.Add(DataReaderToEntity(dr));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return prestations;
        }
        public List<PrestationEntity> GetDatePrestationByLibelle(int idPrestation)
        {
            List<PrestationEntity> prestations = new List<PrestationEntity>();

            IDbCommand cmd = GetCommand(@"select * from prestation 
                                        WHERE id_prestation = @Id");

            cmd.Parameters.Add(new MySqlParameter("@Id", idPrestation));

            try
            {
                cmd.Connection.Open();

                IDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    prestations.Add(DataReaderToEntity(dr));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return prestations;
        }


        public DataTable GetlistPrestaByIdPart(int idPartenaire)
        {
            DataTable table = new DataTable();

            IDbCommand cmd = GetCommand(@"SELECT p.id_prestation, p.libelle, p.date_prestation, p.heure_debut, p.heure_fin, p.nombre_masseurs
                                        FROM prestation p
                                        where id_partenaire =@idPartenaire
                                        and date_prestation >@date;");
            cmd.Parameters.Add(new MySqlParameter("@idPartenaire", idPartenaire));
            cmd.Parameters.Add(new MySqlParameter("@date", DateTime.Now));

            MySqlDataAdapter da = new MySqlDataAdapter();
            da.SelectCommand = (MySqlCommand)cmd;
            da.Fill(table);
            return table;


        }
        public int GetOccuNbr(int idPrestation)
        {
            int NbOccurence = 0;

        IDbCommand cmd = GetCommand(@"SELECT COUNT(id_prestation) as nombre_presta
                                        FROM nomads.inscription_masseurs
                                        Where id_prestation = @idPrestation
                                        and date_desinscription is null
                                        ;");
            cmd.Parameters.Add(new MySqlParameter("@idPrestation", idPrestation));

            try
            {
                cmd.Connection.Open();
                NbOccurence = Convert.ToInt32(cmd.ExecuteScalar()); //Forcément un chiffre? besoin de passer en int? suivi d'aun if null??...
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                cmd.Connection.Close();
            }

            return NbOccurence;
        }

        // GridView dans la page MajAnnulationPrestation
        public DataTable GetAllForGridView()
        {

            DataTable result = new DataTable();

            IDbCommand cmd = GetCommand(@"select p.nom 'nom_partenaire', a.id_prestation, a.libelle 'libelle_prestation', DATE_FORMAT(a.date_prestation,GET_FORMAT(date,'EUR')), a.nombre_masseurs
                                         from prestation a 
                                         join partenaire p on a.id_partenaire = p.id_partenaire
                                         where a.date_annulation is null
                                         and a.date_prestation >= now() order by nom");


            MySqlDataAdapter da = new MySqlDataAdapter();
            da.SelectCommand = (MySqlCommand)cmd;

            // remplir la datatable :
            da.Fill(result);

            return result;
        }
        //recherche GridView dans la page MajAnnulationPrestation
        public DataTable SearchInGridview(string nomPartenaire, int idPrestation)
        {
            DataTable result = new DataTable();

            IDbCommand cmd = GetCommand(@"SELECT p.nom 'nom_partenaire', a.id_prestation, 
                a.libelle 'libelle_prestation', DATE_FORMAT(a.date_prestation,GET_FORMAT(date,'EUR')), a.nombre_masseurs
                from prestation a 
                join partenaire p on a.id_partenaire = p.id_partenaire
                where a.date_annulation is null
                AND a.date_prestation >= now()
                AND p.nom LIKE @nomPartenaire OR (a.id_prestation = @idPrestation)
                OR (p.nom LIKE @nomPartenaire AND (@idPrestation = -1 OR a.id_prestation = @idPrestation))
                order by nom");

            cmd.Parameters.Add(new MySqlParameter("@nomPartenaire", "%" + nomPartenaire + "%"));
            cmd.Parameters.Add(new MySqlParameter("@idPrestation", idPrestation));
 
            

            MySqlDataAdapter da = new MySqlDataAdapter();
            da.SelectCommand = (MySqlCommand)cmd;

            // remplir la datatable :
            da.Fill(result);

            return result;
        }
        public void UpdateDateAnnulation(int idPrestation)
        {
            IDbCommand cmd = GetCommand(@"UPDATE prestation 
                                        set date_annulation = NOW()
                                        where id_prestation = @id");

            cmd.Parameters.Add(new MySqlParameter("@id", idPrestation));

            try
            {
                cmd.Connection.Open();

                cmd.ExecuteNonQuery();
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
        }
        public void UpdatePrestation (PrestationEntity prestation)
        {
            IDbCommand cmd = GetCommand(@"UPDATE prestation 
                set libelle = @Libelle, date_prestation = @DatePrestation, 
                heure_debut = @HeureDebut, heure_fin = @HeureFin, 
                nombre_masseurs = @NombreMasseurs, nombre_chaises_ergonomiques = @NombreChaisesErgonomiques,
                cout_mads = @CoutMads, montant_remuneration = @MontantRemuneration, 
                numero_de_voie = @NumeroVoie, complement_numero_voie = @ComplementNumVoie,
                nom_de_voie = @NomVoie, specificites = @Specificites, id_ville  = @idVille 
                where id_prestation = @Id");

        
            cmd.Parameters.Add(new MySqlParameter("@Id", prestation.Id));
            cmd.Parameters.Add(new MySqlParameter("@Libelle", prestation.Libelle));
            cmd.Parameters.Add(new MySqlParameter("@DatePrestation", prestation.DatePrestation));
            cmd.Parameters.Add(new MySqlParameter("@HeureDebut", prestation.HeureDebut));
            cmd.Parameters.Add(new MySqlParameter("@HeureFin", prestation.HeureFin));
            cmd.Parameters.Add(new MySqlParameter("@NombreMasseurs", prestation.NombreMasseurs));
            cmd.Parameters.Add(new MySqlParameter("@NombreChaisesErgonomiques", prestation.NombreChaisesErgonomiques));
            cmd.Parameters.Add(new MySqlParameter("@CoutMads", prestation.CoutMads));
            cmd.Parameters.Add(new MySqlParameter("@MontantRemuneration", prestation.MontantRemuneration));
            cmd.Parameters.Add(new MySqlParameter("@NumeroVoie", prestation.NumeroVoie));
            cmd.Parameters.Add(new MySqlParameter("@ComplementNumVoie", prestation.ComplementNumVoie));
            cmd.Parameters.Add(new MySqlParameter("@NomVoie", prestation.NomVoie));
            cmd.Parameters.Add(new MySqlParameter("@Specificites", prestation.Specificites));
            cmd.Parameters.Add(new MySqlParameter("@idVille", prestation.IdVille));


            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
        
    }// récupérer une prestation avec id pour edition
        public PrestationEntity GetById(int IdPrestation)
        {
            PrestationEntity result = null;
           
            IDbCommand cmd = GetCommand(@"select id_prestation, ifnull(libelle,0), ifnull(date_prestation,0), 
ifnull(heure_debut,0), ifnull(heure_fin,0), ifnull(nombre_masseurs,0),
ifnull(nombre_chaises_ergonomiques,0), ifnull(cout_mads,0), ifnull(montant_remuneration,0),
ifnull(date_paiement,0), ifnull(numero_de_voie,0), ifnull(complement_numero_voie,0), 
ifnull(nom_de_voie,0), ifnull(id_ville,0)
from Prestation 
where id_prestation = @Id");

            cmd.Parameters.Add(new MySqlParameter("@Id", IdPrestation));

            try
            { 
                cmd.Connection.Open();

                IDataReader dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    result = DataReaderToEntity(dr);
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
               
                cmd.Connection.Close();
            }
            return result;
        }
        // recherche gridview prestation de voir prestation 
        public DataTable SearchInGridviewGen(int IdPartenaire)
        {
            DataTable result = new DataTable();

            IDbCommand cmd = GetCommand(@"SELECT p.id_partenaire, p.nom 'nom_partenaire', DATE_FORMAT(a.date_prestation,GET_FORMAT(date,'EUR')), a.libelle, a.date_prestation
                                         from partenaire p 
                                        left outer join prestation a on a.id_partenaire = p.id_partenaire
                                        where a.date_annulation is null 
                                        AND a.date_prestation >= now() 
                                        AND (@IdPartenaire = -1 OR p.id_partenaire = @IdPartenaire)
                                        order by nom");

            cmd.Parameters.Add(new MySqlParameter("@IdPartenaire", IdPartenaire));





            MySqlDataAdapter da = new MySqlDataAdapter();
            da.SelectCommand = (MySqlCommand)cmd;

            // remplir la datatable :
            da.Fill(result);

            return result;
        }
        public DataTable SearchInGridviewGenHisto(int IdPartenaire)
        {
            DataTable result = new DataTable();

            IDbCommand cmd = GetCommand(@"SELECT p.id_partenaire, p.nom 'nom_partenaire', a.id_prestation, a.libelle, DATE_FORMAT(a.date_prestation,GET_FORMAT(date,'EUR'))
                                         from partenaire p 
                                        left outer join prestation a on a.id_partenaire = p.id_partenaire
                                        where a.date_annulation is null and a.date_prestation < now()
                                        and (@IdPartenaire = -1 OR p.id_partenaire = @IdPartenaire)
                                        order by nom");

            cmd.Parameters.Add(new MySqlParameter("@IdPartenaire", IdPartenaire));





            MySqlDataAdapter da = new MySqlDataAdapter();
            da.SelectCommand = (MySqlCommand)cmd;

            // remplir la datatable :
            da.Fill(result);
            return result;
        }
        public PrestationEntity GetPrestationById(int IdPrestation)
        {
            PrestationEntity prestation = new PrestationEntity();

            IDbCommand cmd = GetCommand(@"select *
                                        from prestation p
                                        where p.id_prestation = @idPrestation;");

            cmd.Parameters.Add(new MySqlParameter("@idPrestation", IdPrestation));

            try
            {
                cmd.Connection.Open();

                IDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    prestation = DataReaderToEntity(dr);
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return prestation;
        }

        private PrestationEntity DataReaderToEntity(IDataReader dr)
        {
            PrestationEntity prestation = new PrestationEntity();
            prestation.Id = dr.GetInt32(dr.GetOrdinal("id_prestation"));
            prestation.Libelle = dr.GetString(dr.GetOrdinal("libelle"));
            prestation.DatePrestation = dr.GetDateTime(dr.GetOrdinal("date_prestation"));
            prestation.NombreMasseurs = dr.GetInt32(dr.GetOrdinal("nombre_masseurs"));
            if (!dr.IsDBNull(dr.GetOrdinal("nombre_chaises_ergonomiques")))
            {
                prestation.NombreChaisesErgonomiques = dr.GetInt32(dr.GetOrdinal("nombre_chaises_ergonomiques"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("montant_remuneration")))
            {
                prestation.MontantRemuneration = dr.GetDouble(dr.GetOrdinal("montant_remuneration"));
            }
            prestation.CoutMads = dr.GetInt32(dr.GetOrdinal("cout_mads"));
            if (!dr.IsDBNull(dr.GetOrdinal("date_annulation")))
            {
                prestation.DateAnnulation = dr.GetDateTime(dr.GetOrdinal("date_annulation"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("date_paiement")))
            {
                prestation.DatePaiement = dr.GetDateTime(dr.GetOrdinal("date_paiement"));
            }
            prestation.NumeroVoie = dr.GetString(dr.GetOrdinal("numero_de_voie"));
            if (!dr.IsDBNull(dr.GetOrdinal("complement_numero_voie")))
            {
                prestation.ComplementNumVoie = dr.GetString(dr.GetOrdinal("complement_numero_voie"));
            }
            prestation.NomVoie = dr.GetString(dr.GetOrdinal("nom_de_voie"));
            prestation.IdPartenaire = dr.GetInt32(dr.GetOrdinal("id_partenaire"));
            if (!dr.IsDBNull(dr.GetOrdinal("id_recurrence")))
            {
                prestation.IdRecurrence = dr.GetInt32(dr.GetOrdinal("id_recurrence"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("id_motif_annulation")))
            {
                prestation.IdMotifAnnulation = dr.GetInt32(dr.GetOrdinal("id_motif_annulation"));
            }
            prestation.IdTypePaiement = dr.GetInt32(dr.GetOrdinal("id_type_paiement"));
            prestation.IdVille = dr.GetInt32(dr.GetOrdinal("id_ville"));
            if (!dr.IsDBNull(dr.GetOrdinal("heure_debut")))
            {
                prestation.HeureDebut = dr.GetString(dr.GetOrdinal("heure_debut"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("heure_fin")))
            {
                prestation.HeureFin = dr.GetString(dr.GetOrdinal("heure_fin"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("specificites")))
            {
                prestation.Specificites = dr.GetString(dr.GetOrdinal("specificites"));
            }
            return prestation;

        }
    }





}




