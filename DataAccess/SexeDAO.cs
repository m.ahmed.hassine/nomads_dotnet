﻿using fr.afcepf.ai107.nomads.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fr.afcepf.ai107.nomads.DataAccess
{
    public class SexeDAO : DAO
    {
        public SexeEntity GetSexeByLibelle(string libelleSexe)
        {
            SexeEntity sexe = new SexeEntity();

            IDbCommand cmd = GetCommand("select * from sexe where libelle_sexe = @libelle ");
            cmd.Parameters.Add(new MySqlParameter("@libelle", libelleSexe));
            try
            {
                cmd.Connection.Open();

                IDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    sexe.LibelleSexe = dr.GetString(dr.GetOrdinal("libelle_sexe"));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return sexe;
        }
        public SexeEntity GetSexeById(int idSexe)
        {
            SexeEntity sexe = new SexeEntity();

            IDbCommand cmd = GetCommand("select * from sexe where id_sexe = @id_sexe ");
            cmd.Parameters.Add(new MySqlParameter("@id_sexe", idSexe));
            try
            {
                cmd.Connection.Open();

                IDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    sexe.LibelleSexe = dr.GetString(dr.GetOrdinal("libelle_sexe"));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return sexe;
        }
        public List<SexeEntity> GetListeSexe()
        {
            List<SexeEntity> listeSexe = new List<SexeEntity>();

            IDbCommand cmd = GetCommand("select * from sexe ");

            try
            {
                cmd.Connection.Open();

                IDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    listeSexe.Add(DataReaderToEntity(dr));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return listeSexe;
        }
        private SexeEntity DataReaderToEntity(IDataReader dr)
        {
            SexeEntity sexe = new SexeEntity
            {
                IdSexe = dr.GetInt32(dr.GetOrdinal("id_sexe")),
                LibelleSexe = dr.GetString(dr.GetOrdinal("libelle_sexe")),
            };
            return sexe;
        }
    }
}
