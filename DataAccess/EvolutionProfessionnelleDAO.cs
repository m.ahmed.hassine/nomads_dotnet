﻿using fr.afcepf.ai107.nomads.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fr.afcepf.ai107.nomads.DataAccess
{
    public class EvolutionProfessionnelleDAO : DAO
    {
        public int GetIdNiveauCompetencesByIdMasseur(int idMasseur)
        {
            int idNiveauCompetences = 0;
            IDbCommand cmd = GetCommand(@"select epm.id_suivi_competences, epm.id_niveau_competence 
                                          from evolution_professionnelle_masseur epm 
                                          where id_masseur=@id and 
                                          epm.date_evolution IN (SELECT Max(epm2.date_evolution)
                                          from evolution_professionnelle_masseur epm2 
                                          where epm.id_masseur = epm2.id_masseur);");
            cmd.Parameters.Add(new MySqlParameter("@id", idMasseur));
            try
            {
                cmd.Connection.Open();

                IDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    idNiveauCompetences = dr.GetInt32(dr.GetOrdinal("id_niveau_competence"));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return idNiveauCompetences;
        }
        public EvolutionProfessionnelleEntity GetEvolutionProfessionnelleById(int idSuiviCompetences)
        {
            EvolutionProfessionnelleEntity evolutionProfessionnelle = new EvolutionProfessionnelleEntity();

            IDbCommand cmd = GetCommand("select * from evolution_professionnelle_masseur where id_suivi_competences = @id ");
            cmd.Parameters.Add(new MySqlParameter("@id", idSuiviCompetences));
            try
            {
                cmd.Connection.Open();

                IDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    evolutionProfessionnelle.IdNiveauCompetences = dr.GetInt32(dr.GetOrdinal("id_niveau_competence"));
                    evolutionProfessionnelle.DateEvolution = dr.GetDateTime(dr.GetOrdinal("date_evolution"));
                    evolutionProfessionnelle.IdSuiviCompetences = dr.GetInt32(dr.GetOrdinal("id_suivi_competences"));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return evolutionProfessionnelle;
        }
        public void AddEvolutionProfessionnelle(EvolutionProfessionnelleEntity evolutionProfessionnelle)
        {
            IDbCommand cmd = GetCommand(@"INSERT INTO evolution_professionnelle_masseur (date_evolution,id_niveau_competence,id_masseur) 
                                        VALUES(@date_evolution,@id_niveau_competence,@id_masseur)");

            cmd.Parameters.Add(new MySqlParameter("@date_evolution", evolutionProfessionnelle.DateEvolution));
            cmd.Parameters.Add(new MySqlParameter("@id_niveau_competence", evolutionProfessionnelle.IdNiveauCompetences));
            cmd.Parameters.Add(new MySqlParameter("@id_masseur", evolutionProfessionnelle.IdMasseur));
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
        }        
        public int GetLastIdEvolutionProfessionnelle()
        {
            EvolutionProfessionnelleEntity evolutionProfessionnelle = new EvolutionProfessionnelleEntity();
            int id = 0;
            IDbCommand cmd = GetCommand("select Max(id_suivi_competences) as max_id from evolution_professionnelle_masseur ");

            try
            {
                cmd.Connection.Open();

                IDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    id = dr.GetInt32(dr.GetOrdinal("max_id"));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return id;
        }
    }
}
