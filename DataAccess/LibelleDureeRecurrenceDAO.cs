﻿using fr.afcepf.ai107.nomads.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fr.afcepf.ai107.nomads.DataAccess
{
    public class LibelleDureeRecurrenceDAO : DAO
    {
        public int GetIdDureeLibelleRecurrenceByLibelle(string libelle)
        {
            int id = 0;

            IDbCommand cmd = GetCommand("select * from libelle_duree_recurrence where libelle=@Libelle");
            cmd.Parameters.Add(new MySqlParameter("@libelle", libelle));
            try
            {
                cmd.Connection.Open();

                IDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    id = dr.GetInt32(dr.GetOrdinal("id_libelle_duree_recurrence"));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return id;
        }
        public List<LibelleDureeRecurrenceEntity> GetListeLibelleDureeRecurrence()
        {
            List<LibelleDureeRecurrenceEntity> listeLibelleDureeRecurrence = new List<LibelleDureeRecurrenceEntity>();

            IDbCommand cmd = GetCommand("select * from libelle_duree_recurrence ");

            try
            {
                cmd.Connection.Open();

                IDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    listeLibelleDureeRecurrence.Add(DataReaderToEntity(dr));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return listeLibelleDureeRecurrence;
        }
        private LibelleDureeRecurrenceEntity DataReaderToEntity(IDataReader dr)
        {
            LibelleDureeRecurrenceEntity libelleDureeRecurrence = new LibelleDureeRecurrenceEntity
            {
                Id = dr.GetInt32(dr.GetOrdinal("id_libelle_duree_recurrence")),
                Libelle = dr.GetString(dr.GetOrdinal("libelle")),
            };
            return libelleDureeRecurrence;
        }
    }
}
