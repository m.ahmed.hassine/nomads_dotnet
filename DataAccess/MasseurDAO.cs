﻿using fr.afcepf.ai107.nomads.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;

namespace fr.afcepf.ai107.nomads.DataAccess
{
    public class MasseurDAO : DAO
    {
        public void UpdateMasseur(MasseurEntity masseur)
        {
            IDbCommand cmd = GetCommand(@"UPDATE masseur 
                                          SET nom=@nom, prenom=@prenom, date_de_naissance=@date_de_naissance, 
                                        adresse_mail=@adresse_mail, telephone=@telephone, numero_de_voie=@numero_de_voie, 
                                        complement_numero_de_voie=@complement_numero_de_voie, nom_de_voie=@nom_de_voie, 
                                        id_sexe=@id_sexe,id_evolution_professionnelle=@id_evolution_professionnelle,
                                        id_aptitude=@id_aptitude,id_ville=@id_ville
                                        WHERE id_masseur=@id_masseur");

            cmd.Parameters.Add(new MySqlParameter("@nom", masseur.Nom));
            cmd.Parameters.Add(new MySqlParameter("@prenom", masseur.Prenom));
            cmd.Parameters.Add(new MySqlParameter("@date_de_naissance", masseur.DateNaissance));
            cmd.Parameters.Add(new MySqlParameter("@adresse_mail", masseur.AdresseMail));
            cmd.Parameters.Add(new MySqlParameter("@telephone", masseur.NumeroTelephone));
            cmd.Parameters.Add(new MySqlParameter("@numero_de_voie", masseur.NumeroVoie));
            cmd.Parameters.Add(new MySqlParameter("@complement_numero_de_voie", masseur.ComplementNumeroVoie));
            cmd.Parameters.Add(new MySqlParameter("@nom_de_voie", masseur.NomVoie));
            cmd.Parameters.Add(new MySqlParameter("@id_sexe", masseur.IdSexe));
            cmd.Parameters.Add(new MySqlParameter("@id_evolution_professionnelle", masseur.IdEvolutionPro));
            cmd.Parameters.Add(new MySqlParameter("@id_aptitude", masseur.IdAptitude));
            cmd.Parameters.Add(new MySqlParameter("@id_ville", masseur.IdVille));
            cmd.Parameters.Add(new MySqlParameter("@id_masseur", masseur.IdMasseur));
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();

            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
        }
        public MasseurEntity GetMassById(int idMasseur)
        {
            MasseurEntity masseur = new MasseurEntity();
            IDbCommand cmd = GetCommand(@"select * from masseur m
                                            where id_masseur=@id_masseur");

            cmd.Parameters.Add(new MySqlParameter("@id_masseur", idMasseur));
        
            try
            {
                cmd.Connection.Open();

                IDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    masseur.IdMasseur = dr.GetInt32(dr.GetOrdinal("id_masseur"));
                    masseur.Nom = dr.GetString(dr.GetOrdinal("nom"));
                    masseur.Prenom = dr.GetString(dr.GetOrdinal("prenom"));
                    masseur.DateNaissance = dr.GetDateTime(dr.GetOrdinal("date_de_naissance"));
                    masseur.AdresseMail = dr.GetString(dr.GetOrdinal("adresse_mail"));
                    masseur.NumeroTelephone = dr.GetString(dr.GetOrdinal("telephone"));
                    masseur.NumeroVoie = dr.GetString(dr.GetOrdinal("numero_de_voie"));
                    
                    if (!dr.IsDBNull(dr.GetOrdinal("complement_numero_de_voie")))
                    {
                    masseur.ComplementNumeroVoie = dr.GetString(dr.GetOrdinal("complement_numero_de_voie"));

                    }
                    masseur.NomVoie = dr.GetString(dr.GetOrdinal("nom_de_voie"));
                    masseur.DatePremierEnregistrement = dr.GetDateTime(dr.GetOrdinal("date_premier_enregistrement"));
                    if (!dr.IsDBNull(dr.GetOrdinal("date_depart_definitif")))
                    {
                    masseur.DateDepartDefinitif = dr.GetDateTime(dr.GetOrdinal("date_depart_definitif"));
                    }
                    masseur.IdSexe = dr.GetInt32(dr.GetOrdinal("id_sexe"));
                    masseur.IdEvolutionPro = dr.GetInt32(dr.GetOrdinal("id_evolution_professionnelle"));
                    masseur.IdAptitude = dr.GetInt32(dr.GetOrdinal("id_aptitude"));
                    if (!dr.IsDBNull(dr.GetOrdinal("id_ville")))
                    {
                        masseur.IdVille = dr.GetInt32(dr.GetOrdinal("id_ville"));
                    }
                    }
                }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return masseur;
        }

        public int GetLastIdMass()
        {
            int id = 0;
            IDbCommand cmd = GetCommand("select Max(id_masseur) as max_id from masseur ");

            try
            {
                cmd.Connection.Open();

                IDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    id = dr.GetInt32(dr.GetOrdinal("max_id"));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return id;
        
        }

        public DataTable GetAll()
        {

            DataTable masseurs = new DataTable();

            IDbCommand cmd = this.GetCommand(@"SELECT m.id_masseur, m.nom, m.prenom, m.date_de_naissance, m.adresse_mail, m.telephone, m.numero_de_voie, 
                                              m.complement_numero_de_voie, m.nom_de_voie, 
                                              v.libelle_cp, v.libelle_ville, 
                                              m.date_premier_enregistrement, m.date_depart_definitif, 
                                              s.libelle_sexe, 
                                              nivcomp.libelle_niveau_competence, cat.libelle_categorie
                                              FROM nomads.masseur m
                                              join nomads.ville_cp v on m.id_ville = v.id_ville
                                              join nomads.sexe s on m.id_sexe = s.id_sexe
                                              join nomads.evolution_professionnelle_masseur evo on m.id_evolution_professionnelle = evo.id_suivi_competences
                                              join nomads.niveau_competence nivcomp on evo.id_niveau_competence = nivcomp.id_statut
                                              join nomads.aptitude apti on m.id_aptitude = apti.id_aptitude
                                              join nomads.categorie cat on apti.id_categorie = cat.id_categorie;");


            MySqlDataAdapter da = new MySqlDataAdapter();
            da.SelectCommand = (MySqlCommand)cmd;
            da.Fill(masseurs);

            return masseurs;

        }

        public void AddMassToDB(MasseurEntity nvMasseur)
        {
            IDbCommand cmd = GetCommand(@"INSERT INTO masseur (nom, prenom, date_de_naissance, 
                                          adresse_mail, telephone, numero_de_voie, complement_numero_de_voie, nom_de_voie, date_premier_enregistrement,
                                          id_sexe, id_evolution_professionnelle, id_aptitude, id_ville)VALUES(@nom, @prenom, @date_de_naissance,
                                          @adresse_mail, @telephone, @numero_de_voie, @complement_numero_de_voie, @nom_de_voie, @date_premier_enregistrement,
                                           @id_sexe, @id_evolution_professionnelle, @id_aptitude, @id_ville)");
            cmd.Parameters.Add(new MySqlParameter("@nom", nvMasseur.Nom));
            cmd.Parameters.Add(new MySqlParameter("@prenom", nvMasseur.Prenom));
            cmd.Parameters.Add(new MySqlParameter("@date_de_naissance", nvMasseur.DateNaissance));
            cmd.Parameters.Add(new MySqlParameter("@adresse_mail", nvMasseur.AdresseMail));
            cmd.Parameters.Add(new MySqlParameter("@telephone", nvMasseur.NumeroTelephone));
            cmd.Parameters.Add(new MySqlParameter("@numero_de_voie", nvMasseur.NumeroVoie));
            cmd.Parameters.Add(new MySqlParameter("@complement_numero_de_voie", nvMasseur.ComplementNumeroVoie));
            cmd.Parameters.Add(new MySqlParameter("@nom_de_voie", nvMasseur.NomVoie));
            cmd.Parameters.Add(new MySqlParameter("@date_premier_enregistrement", nvMasseur.DatePremierEnregistrement));
            cmd.Parameters.Add(new MySqlParameter("@id_sexe", nvMasseur.IdSexe));
            cmd.Parameters.Add(new MySqlParameter("@id_evolution_professionnelle", nvMasseur.IdEvolutionPro));
            cmd.Parameters.Add(new MySqlParameter("@id_aptitude", nvMasseur.IdAptitude));
            cmd.Parameters.Add(new MySqlParameter("@id_ville", nvMasseur.IdVille));

            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();

            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
        }

        public DataTable GetById(int idMasseur) // Surcharge??
        {
            DataTable masseur = new DataTable();

            IDbCommand cmd = this.GetCommand(@"SELECT m.id_masseur, m.nom, m.prenom, m.date_de_naissance, m.adresse_mail, m.telephone, m.numero_de_voie, 
                                              m.complement_numero_de_voie, m.nom_de_voie, v.libelle_cp, v.libelle_ville, m.date_premier_enregistrement, 
                                              m.date_depart_definitif, s.libelle_sexe, nivcomp.libelle_niveau_competence, cat.libelle_categorie
                                              FROM nomads.masseur m
                                              join nomads.ville_cp v on m.id_ville = v.id_ville
                                              join nomads.sexe s on m.id_sexe = s.id_sexe
                                              join nomads.evolution_professionnelle_masseur evo on m.id_evolution_professionnelle = evo.id_suivi_competences
                                              join nomads.niveau_competence nivcomp on evo.id_niveau_competence = nivcomp.id_statut
                                              join nomads.aptitude apti on m.id_aptitude = apti.id_aptitude
                                              join nomads.categorie cat on apti.id_categorie = cat.id_categorie
                                              WHERE m.id_masseur = @idMasseur;");
            cmd.Parameters.Add(new MySqlParameter("@idMasseur", idMasseur));

            MySqlDataAdapter da = new MySqlDataAdapter();
            da.SelectCommand = (MySqlCommand)cmd;
            da.Fill(masseur);

            return masseur;

        }




        public DataTable GetMasseurList()
        {
            DataTable dtListeMasseurs = new DataTable();

            IDbCommand cmd = this.GetCommand(@"SELECT m.id_masseur, m.nom, m.prenom, m.date_premier_enregistrement, m.date_depart_definitif, 
                                                 p.libelle, p.date_prestation, 
                                                 modi.motif,
                                                 nivcomp.libelle_niveau_competence, cat.libelle_categorie
                                              FROM nomads.masseur m 
			                                     left join inscription_masseurs inscrim on m.id_masseur = inscrim.id_masseur
			                                     left join prestation p on inscrim.id_prestation = p.id_prestation
			                                     left join indisponibilite_masseur i on i.id_masseur = m.id_masseur
                                                 left join motif_indisponibilite_masseur modi on i.id_motif_indisponibilite_masseur = modi.id_motif_indisponibilite_masseur
                                                 join nomads.evolution_professionnelle_masseur evo on m.id_evolution_professionnelle = evo.id_suivi_competences
                                                 join nomads.niveau_competence nivcomp on evo.id_niveau_competence = nivcomp.id_statut
                                                 join nomads.aptitude apti on m.id_aptitude = apti.id_aptitude
                                                 join nomads.categorie cat on apti.id_categorie = cat.id_categorie
                                              ;");


            return dtListeMasseurs;
        }


        public List<MasseurEntity> GetAllInListByIdCategorie(int idCategorie)
        {
            IDbCommand cmd;

            List<MasseurEntity> listeTousMasseurs = new List<MasseurEntity>();
            if (idCategorie < 1)
            {
                cmd = GetCommand(@"select * from masseur m
                                    left join indisponibilite_masseur ind on ind.id_masseur = m.id_masseur
                                    where m.date_depart_definitif is null
                                    and(ind.date_fin_indisponibilite < sysdate() or ind.date_fin_indisponibilite is null)");
            }
            else
            {
                cmd = GetCommand(@"select m.* from masseur m
                                            left join aptitude apt  on  apt.id_aptitude=m.id_aptitude
                                            left join indisponibilite_masseur ind on ind.id_masseur=m.id_masseur
                                            where apt.id_categorie=@id_categorie and m.date_depart_definitif is null 
                                            and (ind.date_fin_indisponibilite< sysdate() or ind.date_fin_indisponibilite is null)");

                cmd.Parameters.Add(new MySqlParameter("@id_categorie", idCategorie));
            }

            try
            {
                cmd.Connection.Open();

                IDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    listeTousMasseurs.Add(DataReaderToEntity(dr));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return listeTousMasseurs;
        }
        public List<MasseurEntity> GetMassDispoInListByIdCategorie(int idCategorie, DateTime datePrestation)
        {
            IDbCommand cmd;

            List<MasseurEntity> listeTousMasseurs = new List<MasseurEntity>();
            if (idCategorie < 1)
            {
                cmd = GetCommand(@"select * from masseur m
                                    left join indisponibilite_masseur ind on ind.id_masseur = m.id_masseur
                                    where m.date_depart_definitif is null
                                    and(ind.date_fin_indisponibilite < @date_prestation or ind.date_fin_indisponibilite is null)");
                cmd.Parameters.Add(new MySqlParameter("@date_prestation", datePrestation));
            }
            else
            {
                cmd = GetCommand(@"select m.* from masseur m
                                            left join aptitude apt  on  apt.id_aptitude=m.id_aptitude
                                            left join indisponibilite_masseur ind on ind.id_masseur=m.id_masseur
                                            where apt.id_categorie=@id_categorie and m.date_depart_definitif is null 
                                            and (ind.date_fin_indisponibilite< @date_prestation or ind.date_fin_indisponibilite is null)");

                cmd.Parameters.Add(new MySqlParameter("@id_categorie", idCategorie));
                cmd.Parameters.Add(new MySqlParameter("@date_prestation", datePrestation));

            }

            try
            {
                cmd.Connection.Open();

                IDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    listeTousMasseurs.Add(DataReaderToEntity(dr));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return listeTousMasseurs;
        }

        public DataTable GetMassListByPresta (int idPrestation)
        {
            DataTable massPrest = new DataTable();

            IDbCommand cmd = this.GetCommand(@"SELECT m.nom, m.prenom
                                             FROM masseur m
                                             join inscription_masseurs inscr on m.id_masseur = inscr.id_masseur
                                             join prestation p on inscr.id_prestation = p.id_prestation
                                             where p.id_prestation =@idPrestation
                                             ;");
            cmd.Parameters.Add(new MySqlParameter("@idPrestation", idPrestation));

            MySqlDataAdapter da = new MySqlDataAdapter();
            da.SelectCommand = (MySqlCommand) cmd;
            da.Fill(massPrest);

            return massPrest;
        }

        public void AddMassToPresta(int idMass, int idPrestation)
        {
            IDbCommand cmd = GetCommand(@"INSERT INTO inscription_masseurs (date_inscription, id_masseur, id_prestation)
                                             VALUES(@dateInscri, @idMasseur, @idPrestation)");
            cmd.Parameters.Add(new MySqlParameter("@dateInscri", DateTime.Now));
            cmd.Parameters.Add(new MySqlParameter("@idMasseur", idMass));
            cmd.Parameters.Add(new MySqlParameter("@idPrestation", idPrestation));

            try
            {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();

            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
        }

        public int GetSumMadsBought(int idMasseur)
        {
              int achat = 0;

            IDbCommand cmd = this.GetCommand(@"SELECT ifnull(sum(t.nombre_de_pack * typ.valeur),0)
                                                     FROM masseur m
                                                     join transaction t on m.id_masseur = t.id_masseur
                                                     join type_pack_mads typ on t.id_type_pack = typ.id_type_pack
                                                     where m.id_masseur = @idMasseur;");

            cmd.Parameters.Add(new MySqlParameter("@idMasseur", idMasseur));

            try
            {
                cmd.Connection.Open();

                achat = Convert.ToInt32(cmd.ExecuteScalar());  // A NOTER!!!!!!!!!!!!!!!!!!!!

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                cmd.Connection.Close();
            }

            return achat;
        }

        public int GetSumMadsSpent(int idMasseur)
        {
            int depense = 0;

            IDbCommand cmd = GetCommand(@"SELECT IFNULL(sum(p.cout_mads),0)
                                        FROM masseur m
                                       join inscription_masseurs i on m.id_masseur = i.id_masseur
                                       join prestation p on i.id_prestation = p.id_prestation
                                       where m.id_masseur = @idmasseur;");

            cmd.Parameters.Add(new MySqlParameter("@idMasseur", idMasseur));

            try
            {
                cmd.Connection.Open();

                depense = Convert.ToInt32(cmd.ExecuteScalar());  // A NOTER!!!!!!!!!!!!!!!!!!!!
            }
            catch (Exception e2)
            {
                throw e2;
            }
            finally
            {
                cmd.Connection.Close();
            }

            return depense;
        }



        private MasseurEntity DataReaderToEntity(IDataReader dr)
        {
            MasseurEntity masseur = new MasseurEntity
            {
                IdMasseur = dr.GetInt32(dr.GetOrdinal("id_masseur")),
                Nom = dr.GetString(dr.GetOrdinal("nom")),
                Prenom = dr.GetString(dr.GetOrdinal("prenom")),
                DateNaissance = dr.GetDateTime(dr.GetOrdinal("date_de_naissance")),
                AdresseMail = dr.GetString(dr.GetOrdinal("adresse_mail")),
                NumeroTelephone = dr.GetString(dr.GetOrdinal("telephone")),
                NumeroVoie = dr.GetString(dr.GetOrdinal("numero_de_voie")),
            };
            if (!dr.IsDBNull(dr.GetOrdinal("complement_numero_de_voie")))
            {
                masseur.ComplementNumeroVoie = dr.GetString(dr.GetOrdinal("complement_numero_de_voie"));

            }
            masseur.NomVoie = dr.GetString(dr.GetOrdinal("nom_de_voie"));
            masseur.DatePremierEnregistrement = dr.GetDateTime(dr.GetOrdinal("date_premier_enregistrement"));
            if (!dr.IsDBNull(dr.GetOrdinal("date_depart_definitif")))
            {
                masseur.DateDepartDefinitif = dr.GetDateTime(dr.GetOrdinal("date_depart_definitif"));
            }
            masseur.IdSexe = dr.GetInt32(dr.GetOrdinal("id_sexe"));
            masseur.IdEvolutionPro = dr.GetInt32(dr.GetOrdinal("id_evolution_professionnelle"));
            masseur.IdAptitude = dr.GetInt32(dr.GetOrdinal("id_aptitude"));
            if (!dr.IsDBNull(dr.GetOrdinal("id_ville")))
            {
                masseur.IdVille = dr.GetInt32(dr.GetOrdinal("id_ville"));
            }
            return masseur;
        }



    }





    // public List<MasseurEntity> GetAll()
    //{   

    //    List<MasseurEntity> listeTousMasseurs = new List<MasseurEntity>();

    //    IDbCommand cmd = GetCommand("select * from masseur");

    //    try
    //    {
    //        cmd.Connection.Open();

    //        IDataReader dr = cmd.ExecuteReader();

    //        while(dr.Read())
    //        {
    //            listeTousMasseurs.Add(DataReaderToEntity(dr));
    //        }
    //    }
    //    catch (Exception exc)
    //    {
    //        throw exc;
    //    }
    //    finally
    //    {
    //        cmd.Connection.Close();
    //    }
    //    return listeTousMasseurs;
    //}

    //private MasseurEntity DataReaderToEntity(IDataReader dr)
    //{
    //    MasseurEntity masseur = new MasseurEntity();
    //    masseur.IdMasseur = dr.GetInt32(dr.GetOrdinal("id_masseur"));
    //    masseur.Nom = dr.GetString(dr.GetOrdinal("nom"));
    //    masseur.Prenom= dr.GetString(dr.GetOrdinal("prenom"));
    //    masseur.DateNaissance= dr.GetDateTime(dr.GetOrdinal("date_de_naissance"));
    //    masseur.AdresseMail= dr.GetString(dr.GetOrdinal("adresse_mail"));
    //    masseur.NumeroTelephone = dr.GetString(dr.GetOrdinal("telephone"));
    //    masseur.NumeroVoie = dr.GetString(dr.GetOrdinal("numero_de_voie"));
    //    if(!dr.IsDBNull(dr.GetOrdinal("complement_numero_de_voie")))
    //        {
    //    masseur.ComplementNumeroVoie = dr.GetString(dr.GetOrdinal("complement_numero_de_voie"));

    //    }
    //    masseur.NomVoie = dr.GetString(dr.GetOrdinal("nom_de_voie"));
    //    masseur.DatePremierEnregistrement = dr.GetDateTime(dr.GetOrdinal("date_premier_enregistrement"));
    //    if (!dr.IsDBNull(dr.GetOrdinal("date_depart_definitif")))
    //    {
    //        masseur.DateDepartDefinitif = dr.GetDateTime(dr.GetOrdinal("date_depart_definitif"));
    //    }
    //    masseur.IdSexe=dr.GetInt32(dr.GetOrdinal("id_sexe"));
    //    masseur.IdEvolutionPro=dr.GetInt32(dr.GetOrdinal("id_evolution_professionnelle"));
    //    masseur.IdAptitude=dr.GetInt32(dr.GetOrdinal("id_aptitude"));
    //    if (!dr.IsDBNull(dr.GetOrdinal("id_ville")))
    //    {
    //        masseur.IdVille = dr.GetInt32(dr.GetOrdinal("id_ville"));
    //    }
    //    return masseur;
    // }
}

