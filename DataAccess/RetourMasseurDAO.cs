﻿using fr.afcepf.ai107.nomads.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fr.afcepf.ai107.nomads.DataAccess
{
    public class RetourMasseurDAO : DAO
    {
      public void Insert(RetourExperienceMasseurEntity retourMasseur, int idTypeEvaluationMasseur)
        {
            IDbCommand cmd = GetCommand(@"INSERT INTO retour_experience_masseur
                                        (commentaire, chiffre_affaire, nombre_masses, date_commentaire, id_masseur, id_prestation, id_type_evaluation_masseur) VALUES 
                                        (@commentaire, @chiffre_affaire, @nombre_masses, @date_commentaire, @id_masseur, @id_prestation, @id_type_evaluation_masseur)");

            cmd.Parameters.Add(new MySqlParameter("@commentaire", retourMasseur.Commentaire));
            cmd.Parameters.Add(new MySqlParameter("@chiffre_affaire", retourMasseur.ChiffreDaffaire));
            cmd.Parameters.Add(new MySqlParameter("@nombre_masses", retourMasseur.NombreMasses));
            cmd.Parameters.Add(new MySqlParameter("@date_commentaire", DateTime.Now));
            cmd.Parameters.Add(new MySqlParameter("@id_masseur", retourMasseur.IdMasseur));
            cmd.Parameters.Add(new MySqlParameter("@id_prestation", retourMasseur.IdPrestation));
            cmd.Parameters.Add(new MySqlParameter("@id_type_evaluation_masseur", idTypeEvaluationMasseur));
                                
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            catch(Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
        }

      
    }
}
