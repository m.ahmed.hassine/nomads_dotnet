﻿using fr.afcepf.ai107.nomads.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fr.afcepf.ai107.nomads.DataAccess
{
    public class ContratMasseurDAO : DAO
    {
        public void SetEndContratMasseur(ContratMasseurEntity contratMasseur)
        {
            IDbCommand cmd = GetCommand(@"UPDATE contrat_masseur 
                                          SET date_fin_contrat_masseur=@date_fin_contrat_masseur,
                                              id_motif_fin_contrat_masseur=@id_motif_fin_contrat_masseur
                                        WHERE id_contrat_masseur=@id_contrat_masseur");
            cmd.Parameters.Add(new MySqlParameter("@date_fin_contrat_masseur", DateTime.Now));
            cmd.Parameters.Add(new MySqlParameter("@id_contrat_masseur", contratMasseur.IdContratMasseur));
            cmd.Parameters.Add(new MySqlParameter("@id_motif_fin_contrat_masseur", contratMasseur.IdMotifFinContrat));
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();

            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
        }
        public int GetLastIdContratMasseur()
        {
            ContratMasseurEntity contratMasseur = new ContratMasseurEntity();
            int id = 0;
            IDbCommand cmd = GetCommand("select Max(id_contrat_masseur) as max_id from contrat_masseur ");

            try
            {
                cmd.Connection.Open();

                IDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    id = dr.GetInt32(dr.GetOrdinal("max_id"));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return id;
        }
        public void AddContratMasseur(ContratMasseurEntity contratMasseur)
        {
            IDbCommand cmd = GetCommand(@"INSERT INTO contrat_masseur (date_debut_contrat_masseur,id_masseur) 
                                        VALUES(@date_debut_contrat_masseur,@id_masseur)");

            cmd.Parameters.Add(new MySqlParameter("@date_debut_contrat_masseur", DateTime.Now));
            cmd.Parameters.Add(new MySqlParameter("@id_masseur", contratMasseur.IdMasseur));
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
        }
        public ContratMasseurEntity GetContratMasseurById(int idContratMasseur)
        {
            ContratMasseurEntity contrat = new ContratMasseurEntity();

            IDbCommand cmd = GetCommand("select * from contrat_masseur where id_contrat_masseur = @id ");
            cmd.Parameters.Add(new MySqlParameter("@id", idContratMasseur));
            try
            {
                cmd.Connection.Open();

                IDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    contrat.DateDebutContratMasseur = dr.GetDateTime(dr.GetOrdinal("date_debut_contrat_masseur"));
                    contrat.IdMotifFinContrat = dr.GetInt32(dr.GetOrdinal("id_motif_fin_contrat_masseur"));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return contrat;
        }
    }
}
