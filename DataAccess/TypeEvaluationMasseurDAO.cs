﻿using fr.afcepf.ai107.nomads.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fr.afcepf.ai107.nomads.DataAccess
{
    public class TypeEvaluationMasseurDAO: DAO
    {
       public List<TypeEvaluationMasseurEntity> GetListeEvaluationMasseur()
        {
            IDbCommand cmd = GetCommand(@"Select * 
                                         From type_evaluation_masseur;");

            List<TypeEvaluationMasseurEntity> liste = new List<TypeEvaluationMasseurEntity>();
            try
            {
                cmd.Connection.Open();

                IDataReader dr = cmd.ExecuteReader();


                while (dr.Read())
                {
                    TypeEvaluationMasseurEntity type = new TypeEvaluationMasseurEntity();
                    type.IdTypeEvaluationMasseur = dr.GetInt32(dr.GetOrdinal("id_type_evaluation_masseur"));
                    type.LibelleTypeEvaluationMasseur = dr.GetString(dr.GetOrdinal("libelle_evaluation_masseur"));
                    liste.Add(type);
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return liste;
        }

    }
}
