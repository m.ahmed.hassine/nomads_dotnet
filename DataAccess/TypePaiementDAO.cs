﻿using fr.afcepf.ai107.nomads.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fr.afcepf.ai107.nomads.DataAccess
{
    public class TypePaiementDAO :DAO
    {
        public int GetLastIdTypePaiement()
        {
            
            int id = 0;
            IDbCommand cmd = GetCommand("select Max(id_type_paiement) as max_id from type_paiement ");

            try
            {
                cmd.Connection.Open();

                IDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    id = dr.GetInt32(dr.GetOrdinal("max_id"));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return id;
        }
        public TypePaiementEntity GetTypePaiementById(int idTypePaiement)
        {
            TypePaiementEntity typePaiement = new TypePaiementEntity();

            IDbCommand cmd = GetCommand("select * from type_paiement where id_type_paiement = @id_type_paiement ");
            cmd.Parameters.Add(new MySqlParameter("@id_type_paiement", idTypePaiement));
            try
            {
                cmd.Connection.Open();

                IDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    typePaiement.MontantFacturation = dr.GetDouble(dr.GetOrdinal("montant_facturation"));
                    typePaiement.DateValidation = dr.GetDateTime(dr.GetOrdinal("date_validation"));
                    typePaiement.DateEmission = dr.GetDateTime(dr.GetOrdinal("date_emission"));
                    typePaiement.IdLibelleTypePaiement = dr.GetInt32(dr.GetOrdinal("id_libelle_type_paiement"));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return typePaiement;
        }

        public void AddNewTypePaiement(TypePaiementEntity typePaiement)
        {
            IDbCommand cmd = GetCommand(@"INSERT INTO type_paiement (id_libelle_type_paiement, date_emission, date_validation,
                                          montant_facturation)VALUES(@id_libelle_type_paiement, @date_emission, 
                                          @date_validation, @montant_facturation)");

            cmd.Parameters.Add(new MySqlParameter("@id_libelle_type_paiement", typePaiement.IdLibelleTypePaiement));
            cmd.Parameters.Add(new MySqlParameter("@date_emission", typePaiement.DateEmission));
            cmd.Parameters.Add(new MySqlParameter("@date_validation", typePaiement.DateValidation));
            cmd.Parameters.Add(new MySqlParameter("@montant_facturation", typePaiement.MontantFacturation));
           

            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
        }
    }
}
