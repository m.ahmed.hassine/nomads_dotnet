﻿using fr.afcepf.ai107.nomads.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fr.afcepf.ai107.nomads.DataAccess
{
    public class MotifFinContratPartenaireDAO : DAO
    {
        public List<MotifFinContratPartenaireEntity> GetListeLibelleMotifFinContratPartenaire()
        {
            List<MotifFinContratPartenaireEntity> listeMotifFinContratPartenaireEntity = new List<MotifFinContratPartenaireEntity>();

            IDbCommand cmd = GetCommand("select * from motif_fin_contrat_partenaire ");

            try
            {
                cmd.Connection.Open();

                IDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    listeMotifFinContratPartenaireEntity.Add(DataReaderToEntity(dr));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return listeMotifFinContratPartenaireEntity;
        }
        private MotifFinContratPartenaireEntity DataReaderToEntity(IDataReader dr)
        {
            MotifFinContratPartenaireEntity motif = new MotifFinContratPartenaireEntity
            {
                IdMotifFinContratPartenaire = dr.GetInt32(dr.GetOrdinal("id_motif_fin_contrat_partenaire")),
                LibelleMotifFinContratPartenaire = dr.GetString(dr.GetOrdinal("libelle_motif_fin_contrat_partenaire")),
            };
            return motif;
        }
    }
}
