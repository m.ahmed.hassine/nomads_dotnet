﻿using System;
using System.ComponentModel;

namespace fr.afcepf.ai107.nomads.Entities
{
    public class PrestationEntity
    {
        public int Id { get; set; }
        public string Libelle { get; set; }
        public DateTime DatePrestation { get; set; }
        public int NombreMasseurs { get; set; }
        public int? NombreChaisesErgonomiques { get; set; }
        public int CoutMads { get; set; }
        public double MontantRemuneration { get; set; }
        public DateTime DateAnnulation { get; set; }
        public DateTime DatePaiement { get; set; }
        public string NumeroVoie { get; set; }
        public string ComplementNumVoie { get; set; }
        public string NomVoie { get; set; }
        public int IdPartenaire { get; set; }
        public int IdRecurrence { get; set; }
        public int IdMotifAnnulation { get; set; }
        public int IdTypePaiement { get; set; }
        public int IdVille { get; set; }
        public string Specificites { get; set; }
        public string HeureDebut { get; set; }
        public string HeureFin { get; set; }

        public PrestationEntity()
        {

        }

        public PrestationEntity(int id, string libelle)
        {
            Id = id;
            Libelle = libelle;
        }


    }

   
}
