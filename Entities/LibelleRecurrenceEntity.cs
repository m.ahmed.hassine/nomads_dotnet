﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fr.afcepf.ai107.nomads.Entities
{
    public class LibelleRecurrenceEntity
    {
        public int Id { get; set; }
        public string Libelle { get; set; }
    }
}
