﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fr.afcepf.ai107.nomads.Entities
{
    public class SexeEntity
    {
        public int IdSexe { get; set; }
        public string LibelleSexe { get; set; }
    }
}
