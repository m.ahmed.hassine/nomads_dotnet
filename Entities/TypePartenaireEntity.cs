﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fr.afcepf.ai107.nomads.Entities
{
    public class TypePartenaireEntity
    {
        public int IdTypePartenaire { get; set; }
        public string LibelleTypePartenaire { get; set; }
    }
}
