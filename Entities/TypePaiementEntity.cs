﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fr.afcepf.ai107.nomads.Entities
{
    public class TypePaiementEntity
    {
        public int IdTypePaiementEntity { get; set; }
        public int IdLibelleTypePaiement { get; set; }
        public DateTime DateEmission { get; set; }
        public double MontantFacturation { get; set; }
        public DateTime DateValidation { get; set; }
    }
}
