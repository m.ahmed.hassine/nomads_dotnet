﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fr.afcepf.ai107.nomads.Entities
{
    public class AptitudeEntity
    {
        public int IdAptitude { get; set; }
        public int IdCategorie { get; set; }
        public DateTime DateDebut { get; set; }
        public DateTime DateFin { get; set; }
        public int IdMasseur { get; set; }
    }
}
