﻿namespace fr.afcepf.ai107.nomads.Entities
{
    public class RecurrenceEntity
    {
        public int IdRecurrence { get; set; }
        public int IdLibelleRecurrence { get; set; }
        public int IdLibelleDureeRecurrence { get; set; }
        public int Pas { get; set; }
        public int DureeRecurrence { get; set; }
        public int Delta { get; set; }
    }
}
