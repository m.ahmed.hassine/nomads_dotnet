﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fr.afcepf.ai107.nomads.Entities
{
    public class MotifFinContratMasseurEntity
    {
        public int IdMotifFinContratMasseur { get; set; }
        public string LibelleMotifFinContratMasseur { get; set; }

    }
}
