﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fr.afcepf.ai107.nomads.Entities
{
   public class TypeEvaluationMasseurEntity
    {
        public int IdTypeEvaluationMasseur { get; set; }
        public string LibelleTypeEvaluationMasseur { get; set; }
    }
}
