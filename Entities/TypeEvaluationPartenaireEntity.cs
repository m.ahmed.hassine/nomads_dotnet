﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fr.afcepf.ai107.nomads.Entities
{
   public class TypeEvaluationPartenaireEntity
    {
            public int IdTypeEvaluationPartenaire { get; set; }
            public string LibelleTypeEvaluationPartenaire { get; set; }
       
    }
}
