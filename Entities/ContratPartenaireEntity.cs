﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fr.afcepf.ai107.nomads.Entities
{
    public class ContratPartenaireEntity
    {
        public int IdContratPartenaire { get; set; }
        public DateTime DateDebutContratPartenaire { get; set; }
        public DateTime DateFinContratPartenaire { get; set; }
        public int IdPartenaire { get; set; }
        public int IdMotifFinContrat { get; set; }
    }
}
