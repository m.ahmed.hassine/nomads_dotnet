﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fr.afcepf.ai107.nomads.Entities
{
    public class MotifFinContratPartenaireEntity
    {
        public int IdMotifFinContratPartenaire { get; set; }
        public string LibelleMotifFinContratPartenaire { get; set; }
    }
}
