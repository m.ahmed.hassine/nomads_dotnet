﻿using System;

namespace fr.afcepf.ai107.nomads.Entities
{
    public class PartenaireEntity
    {
        public int IdPartenaire { get; set; }
        public string Nom { get; set; }
        public string NumeroVoie { get; set; }
        public string ComplementNumeroVoie { get; set; }
        public string NomVoie { get; set; }
        public int IdVille { get; set; }
        public string LibelleVille { get; set; }
        public string LibelleCP { get; set; }
        public string AdresseMail { get; set; }
        public string NumeroTelephone { get; set; }
        public string RaisonSociale { get; set; }
        public DateTime DatePremierEnregistrement { get; set; }
        public int IdTypePartenaire { get; set; }

        public PartenaireEntity() 
        {
        }

        public PartenaireEntity(int id, string nom)
        {
            IdPartenaire = id;
            Nom = nom;
        }





    }
}
