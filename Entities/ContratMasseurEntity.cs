﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fr.afcepf.ai107.nomads.Entities
{
    public class ContratMasseurEntity
    {
        public int IdContratMasseur { get; set; }
        public DateTime DateDebutContratMasseur { get; set; }
        public DateTime DateFinContratMasseur { get; set; }
        public int IdMasseur { get; set; }
        public int IdMotifFinContrat { get; set; }
    }
}
