﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fr.afcepf.ai107.nomads.Entities
{
    class InscriptionMasseurEntity
    {

        public int Id { get; set; }
        public DateTime DateInscription { get; set; }
        public DateTime DateDesinscription { get; set; }
        public string TitreFichierJoint { get; set; }
        public int IdMasseur { get; set; }
        public int IdPrestation { get; set; }
        public int IdMotifDesinscription { get; set; }
    }
}
