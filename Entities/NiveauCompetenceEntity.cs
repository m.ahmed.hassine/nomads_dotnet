﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fr.afcepf.ai107.nomads.Entities
{
    public class NiveauCompetenceEntity
    {
        public int IdStatut { get; set; }
        public string LibelleNiveauCompetence { get; set; }
    }
}
