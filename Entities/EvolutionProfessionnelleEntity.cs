﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fr.afcepf.ai107.nomads.Entities
{
    public class EvolutionProfessionnelleEntity
    {
        public int IdSuiviCompetences { get; set; }
        public int IdNiveauCompetences { get; set; }
        public DateTime DateEvolution { get; set; }
        public int IdMasseur { get; set; }
    }
}
