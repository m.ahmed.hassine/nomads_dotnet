﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fr.afcepf.ai107.nomads.Entities
{
    public class VilleEntity
    {
        public int IdVille { get; set; }
        public string LibelleVille { get; set; }
        public string LibelleCP { get; set; }
    }
}
