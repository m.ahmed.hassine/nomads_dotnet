﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fr.afcepf.ai107.nomads.Entities
{
   public class RetourExperiencePartenaireEntity
    {
        public int IdRexPartenaire { get; set; }

        public string Commentaire { get; set; }

        public DateTime Date { get; set; }

        public int IdPartenaire { get; set; }

        public int IdPrestation { get; set; }

        public int IdTypeEvaluationPartenaire { get; set; }

    }
}
