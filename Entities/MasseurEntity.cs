﻿using System;

namespace fr.afcepf.ai107.nomads.Entities
{
    public class MasseurEntity
    {
        public int IdMasseur { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public String NomEtPrenom
        {
            get { return this.Nom + " " + this.Prenom; }
        }
        public DateTime DateNaissance { get; set; } 
        public string AdresseMail { get; set; }
        public string NumeroTelephone { get; set; }
        public string NumeroVoie { get; set; } 
        public string ComplementNumeroVoie { get; set; }
        public string NomVoie { get; set; }
        public DateTime DatePremierEnregistrement { get; set; }
        public DateTime DateDepartDefinitif { get; set; } 
        public int IdSexe { get; set; }
        public int IdEvolutionPro { get; set; }
        public int IdAptitude { get; set; }
        public int IdVille { get; set; }
        
        





    }

}
