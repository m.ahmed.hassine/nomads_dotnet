﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fr.afcepf.ai107.nomads.Entities
{
    public class CategorieEntity
    {
        public int IdCategorie { get; set; }
        public string LibelleCategorie { get; set; }
    }
}
