﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fr.afcepf.ai107.nomads.Entities
{
    public class RetourExperienceMasseurEntity
    {

        public int IdRexMasseur { get; set; }

        public string Commentaire { get; set; }

        public DateTime Date { get; set;  }

        public double ChiffreDaffaire { get; set; }

        public int NombreMasses { get; set; }

        public int IdMasseur { get; set; }

        public int IdPrestation { get; set; }

        public int IdTypeEvaluationMasseur { get; set; }
    }
}
