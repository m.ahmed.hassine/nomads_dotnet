﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Arche.Master" AutoEventWireup="true" CodeBehind="AnnulationMasseur.aspx.cs" Inherits="fr.afcepf.ai107.nomads.Presentation.AnnulationMasseur" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/TestStyleBtn.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="principal" runat="server">

    Vous êtes sur le point d'annuler votre participation à : <asp:Label ID="lblNomPresta" runat="server" Text="//nomPresta" CssClass="lbl"></asp:Label> , prévue le : <asp:Label ID="lblDate" runat="server" Text="//date" CssClass="lbl"></asp:Label>

    Pour quelle raison vous désinscrivez-vous?
    <asp:RadioButtonList ID="rbRaisonDesinscription" runat="server" Width="134px" CssClass="rbl">
   
    </asp:RadioButtonList>
<%--    <asp:Label ID="lblTest" runat="server" Text="Label" CssClass="lbl"></asp:Label>--%>
       
    <asp:Button ID="btnValid" runat="server" Text="Valider l'annulation" OnClick="btnValid_Click" CssClass="btnVal"/>
</asp:Content>
