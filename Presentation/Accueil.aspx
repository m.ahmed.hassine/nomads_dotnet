﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Arche.Master" AutoEventWireup="true" CodeBehind="Accueil.aspx.cs" Inherits="fr.afcepf.ai107.nomads.Presentation.Accueil" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style6 {
            top: 0;
            left: 0;
            height: 63vh;
            min-width: 100%;
            min-height: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Principal" runat="server">
    <asp:Panel runat="server">
        <div>
            <article>
                <h1 style="font-family: Apple Chancery, cursive; color: #164c73; font-size: xx-large; text-align: center; background: transparent">Bienvenue Alexandra</h1>
                <img src="images/Accueil1.jpg" class="auto-style6" />
            </article>
            <a class="weatherwidget-io" href="https://forecast7.com/fr/48d862d35/paris/" data-label_1="PARIS" data-theme="marine">PARIS</a>
            <script>
                !function (d, s, id) { var js, fjs = d.getElementsByTagName(s)[0]; if (!d.getElementById(id)) { js = d.createElement(s); js.id = id; js.src = 'https://weatherwidget.io/js/widget.min.js'; fjs.parentNode.insertBefore(js, fjs); } }(document, 'script', 'weatherwidget-io-js');
            </script>
            
        </div>

    </asp:Panel>
</asp:Content>
