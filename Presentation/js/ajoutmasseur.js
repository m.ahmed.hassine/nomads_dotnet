$(function () {
	$(document).ready(function () {
		'use strict';
		
		$.datetimepicker.setLocale('fr');
		$('#birth_date').datetimepicker({
			timepicker: false,
			datepicker: true,
			format: 'd-m-Y'
		});	
				
	}); /* END DOM Ready */
});
