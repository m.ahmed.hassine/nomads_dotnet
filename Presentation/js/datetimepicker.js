$(document).ready(function () {
            'use strict';
            $.datetimepicker.setLocale('fr');
            $('#txtDatePrestation').datetimepicker({
                timepicker: false,
                datepicker: true,
				format: 'd-m-Y'
            });
        });