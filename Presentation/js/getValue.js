$(function getValue() {
	$(document).ready(function() {

          var NbrMasseurs = document.getElementById("NbrMasseurs").value;
		  var last_valid_selection = null;

          $('#principal_DDLListeMasseurs').change(function(event) {

            if ($(this).val().length > NbrMasseurs) {

              $(this).val(last_valid_selection);
            } else {
              last_valid_selection = $(this).val();
            }
	    });
    }); /* END DOM Ready */
});
