$(function () {
	$(document).ready(function () {
		'use strict';
		$('#principal_DDLListeMasseurs').multiselect({
			includeSelectAllOption: true
		
		});
		$.datetimepicker.setLocale('fr');
		$('.txt-data-picker-date').datetimepicker({
            timepicker: false,
            datepicker: true,
			format: 'd-m-Y'
		});	

		$('.datatable-table').DataTable();
	}); /* END DOM Ready */
});