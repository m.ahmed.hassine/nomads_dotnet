﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Arche.Master" AutoEventWireup="true" CodeBehind="EditionPrestation.aspx.cs" Inherits="fr.afcepf.ai107.nomads.Presentation.WebForm3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />

    <link href="css/StyleCreationPresentation.css" rel="stylesheet" />
    <link href="css/TestStyleBtn.css" rel="stylesheet" />
    
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>  
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js"></script>  
    <script src="https://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/js/bootstrap-multiselect.js" type="text/javascript"></script> 

    <script type="text/javascript" src="js/presentation.js"></script>
    <style type="text/css">
        .auto-style5 {
            width: 550px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="principal" runat="server">
    <asp:Label ID="Label1" runat="server" Text="Gestion prestations > Ajouter une prestation " Font-Bold="True" Font-Italic="True" Font-Size="Medium" ForeColor="#333333"></asp:Label>
     <article id="SelectionPartenaireMasseur">
        <article id="SectionPartenaire">
            <table>
                
                <tr>
                    <td>
                        <asp:Label ID="lblLibelleTypePaiement" runat="server"  Text="Type de paiement: "></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="DDLLibeleTypePaiement" AutoPostBack="true"  runat="server" OnSelectedIndexChanged="DDLLibeleTypePaiement_SelectedIndexChanged"></asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LblMontantFacturation" runat="server" Text="Montant de Facturation: "></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="TxtMontantFacturation" Enabled="false" Width="80" runat="server"></asp:TextBox>
                    </td>
                </tr>
            </table>

        </article>
        <article id="SectionMasseur">
    <table style="background-color: #E2E2E2" class="auto-style5">
                <tr>
                    <td>
                        <asp:Label ID="NbrMasseurs" runat="server" Text="Nombre des masseurs : "></asp:Label></td>
                        
                    <td>
                        <asp:TextBox ID="TxtNbrMasseurs" Width="80" runat="server"></asp:TextBox></td>
                </tr>

      
                
            </table>

        </article>
    </article>
    <article id="SectionInfos">
        <section id="LibelleEtCalendrier">
    <table style="background-color: #E2E2E2" class="auto-style5">
                <tr>
                    <td>
                        <asp:Label ID="LblLibelle" runat="server" Text="Nom de la prestation : "></asp:Label>
                        <asp:TextBox ID="TxtLibelle" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <asp:Label ID="lblDate" runat="server" Text="Date de la prestation: "></asp:Label>
                    <asp:TextBox ID="txtDatePrestation" runat="server"></asp:TextBox>
                    <asp:ImageButton ID="ImageCalendar" runat="server" Height="33px" ImageUrl="~/images/Calendar.png"  Width="41px" />
                    <asp:Calendar ID="Calendar1" Visible="false" runat="server" Height="272px" Width="434px" DayNameFormat="Full" ></asp:Calendar>
                </tr>
            </table>
        </section>

        <section id="Infos">
    <table style="background-color: #E2E2E2" class="auto-style5">
                <tr>
                    <td>
                        <asp:Label ID="LblCoutMads" runat="server" Text="Coût en Mads: "></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="TxtCoutMads" Enabled="false" Width="80" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LblMontantRemuneration" runat="server" Text="Montant de rémunération : "></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="TxtMontantRemuneration" AutoPostBack="true" Enabled="false" Width="80" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LblNbrChaisesErgonomique" runat="server" Text="Nombre des chaises ergonomiques : "></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="TxtNbrChaisesErgonomique" Width="80" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LblHeureDebut" runat="server" Text="Heure de début : "></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="TxtHeureDebut" Width="80" TextMode="Time" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LblHeureFin" runat="server" Text="Heure de fin : "></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="TxtHeureFin" Width="80" TextMode="Time" runat="server"></asp:TextBox>
                    </td>
                </tr>

   
            </table>

        </section>
        <section id="adresse">
    <table style="background-color: #E2E2E2" class="auto-style5">
                <tr>
                    <asp:Label ID="LblLieuPrestation" runat="server" Text="Lieu de la prestation : "></asp:Label></tr>
                <tr>
                    <td><asp:TextBox ID="TxtNumeroVoie" Width="50" runat="server"></asp:TextBox>
                    <asp:TextBox ID="TxtComplementNum" Width="50" runat="server"></asp:TextBox>
                    <asp:TextBox ID="TxtNomVoie" Width="300" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        <asp:DropDownList ID="DDLLibelleVille" AutoPostBack="true" OnSelectedIndexChanged="DDLLibelleVille_SelectedIndexChanged" Width="150" runat="server"></asp:DropDownList>
                        <asp:DropDownList ID="DDLLibelleCP" AutoPostBack="true" OnSelectedIndexChanged="DDLLibelleCP_SelectedIndexChanged"  Width="150" runat="server"></asp:DropDownList>
                    </td>
                </tr>
               <!-- <tr>
                    <td><asp:TextBox ID="TxtLibelleVille" Width="150" runat="server"></asp:TextBox>
                        <asp:TextBox ID="TxtLibelleCP" Width="150" runat="server"></asp:TextBox></></td>
                </tr> -->
            </table>
        </section>
    </article>
    <article id="SectionSpecificites">
    <table style="background-color: #E2E2E2" class="auto-style5">
            <tr>
                <td> <asp:Label ID="LblSpecificites" runat="server" width="1000px" Text="Spécificités : "></asp:Label></td>
                <td> <asp:TextBox ID="TxtSpecificites" Height="100" TextMode="MultiLine" runat="server"></asp:TextBox></td>
                <td> <asp:Button ID="BtnValider" runat="server" OnClick="BtnValider_Click" Text="Valider" /> </td>
            </tr>
        </table>

    </article>


</asp:Content>
