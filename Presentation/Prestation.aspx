﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Arche.Master" AutoEventWireup="true" CodeBehind="Prestation.aspx.cs" Inherits="fr.afcepf.ai107.nomads.Presentation.Prestation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/StylePrestation.css" rel="stylesheet" />
    <link href="css/TestStyleBtn.css" rel="stylesheet" />
    <script type="text/javascript">
        function validation(int i) {
            alert("Votre inscription a bien été prise en compte");
            window.location.replace("FicheMasseur.aspx?id=" + i)
        }
    </script>
</asp:Content>



<asp:Content ID="Content2" ContentPlaceHolderID="principal" runat="server">
        <center>
            <h2 id="h2Nom">&nbsp;</h2>

        </center>

    <table style="width: 100%;">
        <tr>
            <td>
                &nbsp;</td>
            <td><asp:Label ID="lblNomPrestation"  runat="server" Text="Nom de la prestation : " BackColor="White" BorderColor="#666666" BorderStyle="Solid" Font-Bold="True" Font-Overline="False" Font-Strikeout="False" Font-Underline="False" ForeColor="#0000CC" Font-Size="X-Large"></asp:Label></td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>&nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
             <asp:Label ID="Label2" runat="server"  Text="Adresse de la prestation:" Font-Bold="True" Font-Size="Large" ForeColor="#0000CC"></asp:Label>
                <br />
             <asp:Label ID="lblPartenaire" runat="server" Text="//Part"></asp:Label>
                <br />
             <asp:Label ID="lblAdresse" runat="server" Text="//A"></asp:Label>
                <br />
             <asp:Label ID="lblVille" runat="server" Text="//BB"></asp:Label>
            </td>
            <td>&nbsp;</td>
            <td>
            <asp:Label ID="Label4" runat="server" Text="Ils y sont déja:" Font-Bold="True" Font-Size="Large" ForeColor="#0000CC"></asp:Label>
             </td>
        </tr>
        <tr>
            <td style="clip: rect(2px, auto, auto, auto)">
             <asp:Label ID="Label7" runat="server"  Text="Numéro de téléphone: " Font-Bold="True" Font-Size="Large" ForeColor="#0000CC"></asp:Label>
                             <asp:Label ID="lblNumTel" runat="server" Text="//TTT"></asp:Label>

            </td>
            <td>&nbsp;</td>
            <td>
             <asp:GridView ID="gvMasseursInscrits" runat="server" CssClass="grid" ></asp:GridView>
             </td>
        </tr>
      <!--  <tr>
            <td> <asp:Label ID="Label3" runat="server" Text="Spécification de la prestation: " Font-Bold="True" Font-Size="Large" ForeColor="#0000CC"></asp:Label>

              &nbsp;Ajouter le lblSpecification</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr> -->
          <tr>
            <td> &nbsp;</td>
            <td>&nbsp;</td>
            <td>
                <asp:Label ID="Label5" runat="server" Text="Nombre de places disponibles: " Font-Bold="True" Font-Size="Large" ForeColor="#0000CC"></asp:Label>
             <asp:Label ID="lblNbPlacesDispo" runat="server" Text="//NbDispo"></asp:Label>
              </td>
        </tr>
          <tr>
            <td>   
                &nbsp;</td>
            <td>&nbsp;</td>
            <td>
           <asp:Label ID="Label6" runat="server" Text="Cout en Mads: " Font-Bold="True" Font-Size="Large" ForeColor="#0000CC"></asp:Label>
           <asp:Label ID="lblCoutMads" runat="server" Text="//Coût"></asp:Label>

              </td>
        </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>           
            <asp:Label ID="lblMqMads" runat="server" Text="//manqueMads"></asp:Label>
              </td>
        </tr>
          <tr>
            <td>
              </td>
            <td>&nbsp;</td>
            <td>        <asp:Button ID="btnInscription" runat="server" OnClick="btnInscription_Click" Text="S'inscrire" BackColor="#DDF4E7" BorderStyle="Solid" Font-Bold="True" Font-Size="Large" ForeColor="#006600" Height="36px" Width="162px" BorderWidth="1px" />
              </td>
        </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>      
                   
            </asp:Content>
