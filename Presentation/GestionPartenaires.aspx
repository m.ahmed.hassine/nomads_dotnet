﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Arche.Master" AutoEventWireup="true" CodeBehind="GestionPartenaires.aspx.cs" Inherits="fr.afcepf.ai107.nomads.Presentation.GestionPartenaires" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <link href="css/StyleGestionPartenaires.css" rel="stylesheet" />
    <link href="css/TestStyleBtn.css" rel="stylesheet" />
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="principal" runat="server">

    <form id="general">
        <asp:Label ID="Label1" runat="server" Text="Gestion partenaires > Voir liste partenaire " Font-Bold="True" Font-Italic="True" Font-Size="Medium" ForeColor="#333333"></asp:Label>
        <div id="titre">
            <h2 style="color: #0000FF; font-family: Calibri; font-size: x-large">Ci dessous vous trouverez toute la liste de nos partenaires : </h2>
            <p style="color: #0000FF; font-family: Calibri; font-size: large">&nbsp;</p>
        </div>
        <!-- <div id="table">
            <asp:GridView ID="gvListeMasseurs" runat="server"></asp:GridView>

        </div> -->
         <div style="overflow: auto">
         <asp:Panel ID="Panel1" runat="server" ScrollBars="Vertical" Height="450" width="1300">
            <asp:Repeater ID="rptPartenaireInfoMin" runat="server" OnItemCommand="rptPartenaireInfoMin_ItemCommand">
                <HeaderTemplate>
                      <table style="width: 100%;">
                    <table class="partenaire" border="1">
                        <tr>
                            <th>
                                <h2>Nom</h2>
                            </th>
                            <th>
                                <h2>Raison Sociale</h2>
                            </th>
                            <th>
                                <h2>Adresse</h2>
                            </th>
                            <th>
                                <h2>Détail</h2>
                            </th>
                        </tr>
                </HeaderTemplate>

                <ItemTemplate>

                    <tr>
                        <td>
                            <header><%# DataBinder.Eval(Container.DataItem, "nom")%></header>
                        </td>
                        <td>
                            <p><%# DataBinder.Eval(Container.DataItem, "raison_sociale")%> </p>
                        </td>
                        <td>
                            <p>Adresse : <%# DataBinder.Eval(Container.DataItem, "numero_de_voie")%> <%# DataBinder.Eval(Container.DataItem, "complement_numero_voie")%>  <%# DataBinder.Eval(Container.DataItem, "nom_de_voie")%> </p>
                        </td>
                        <td>
                            <a href="FichePartenaire.aspx?id=<%# DataBinder.Eval(Container.DataItem, "id_partenaire")%>">Voir détails</a>
                        </td>
                    </tr>
                </ItemTemplate>

                <FooterTemplate>
                    </table>

                </FooterTemplate>
            </asp:Repeater>
         </asp:Panel>
        </div>





    </form>

</asp:Content>
