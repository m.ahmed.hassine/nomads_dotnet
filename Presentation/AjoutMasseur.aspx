﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Arche.Master" MaintainScrollPositionOnPostback="true" AutoEventWireup="true" CodeBehind="AjoutMasseur.aspx.cs" Inherits="fr.afcepf.ai107.nomads.Presentation.AjoutMasseur" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/jquery.datetimepicker.css" />
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

    <script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js'></script>

    <script type="text/javascript" src="js/jquery.datetimepicker.full.js"></script>
    <script type="text/javascript" src="js/ajoutmasseur.js"></script>
    <script src="js/index.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="principal" runat="server">
    <asp:Label ID="Label1" runat="server" Text="Gestion masseurs > Ajouter un masseur" Font-Bold="True" Font-Italic="True" Font-Size="Large" CssClass="lbl"></asp:Label>
    <br />
    <section class="container-fluid" style="background-color: #E2E2E2; font-size: large;">
        <form class="needs-validation" method="post" id="reg_form">
            <fieldset>
                <div class="form-group">
                    
                    <div class="form-row">
                        <div class="col-md-2">
                            <label for="DDLSexe">Sexe</label>
                            <asp:DropDownList ID="DDLSexe" CssClass="form-control" runat="server"></asp:DropDownList>
                        </div>
                        <div class="col-md-2">
                            <label for="DDLCategorie">Catégorie</label>
                            <asp:DropDownList ID="DDLCategorie" runat="server" CssClass="form-control" AutoPostBack="True"></asp:DropDownList>
                        </div>
                        <div class="col-md-2">
                            <label for="DDLNiveauCompetence">Niveau de compétences</label>
                            <asp:DropDownList ID="DDLNiveauCompetence" CssClass="form-control" runat="server" AutoPostBack="True"></asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-2">
                        <label for="first_name">Prénom</label>
                        <input type="text" class="form-control" name="first_name" placeholder="Prénom" required>
                        
                    </div>
                    <div class="col-md-2">
                        <label for="vlast_name">Nom</label>
                        <input type="text" class="form-control" name="last_name" placeholder="Nom" required>
                        
                    </div>
                    <div class="col-md-2">
                        <label for="birth_date">Date de naissance</label>
                        <input type="text" class="form-control" name="birth_date" placeholder="01-01-1980" required>
                        
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-2">
                        <label for="phone">Téléphone</label>
                        <input type="text" class="form-control" name="phone" placeholder="0601020304" required>
                        
                    </div>
                    <div class="col-md-4">
                        <label for="mail_adress">Adresse mail</label>
                        <input type="text" class="form-control" name="mail_adress" placeholder="Adresse mail"  aria-describedby="emailHelp" required>
                       
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-1">
                        <label for="number_adress">Numéro</label>
                        <input type="text" class="form-control" name="number_adress" placeholder="10" required>
                        
                    </div>
                    <div class="col-md-1">
                        <label for="complement_number">Comp. num</label>
                        <input type="text" class="form-control" name="complement_number" placeholder="Bis">
                        
                    </div>
                    <div class="col-md-4">
                        <label for="street">Nom de la voie</label>
                        <input type="text" class="form-control" name="street" placeholder="Avenue de la république">
                        
                    </div>

                </div>
                <div class="form-row">
                    <div class="col-md-3">
                         <label for="DDLLibelleVille">Ville</label>
                        <asp:DropDownList ID="DDLLibelleVille" AutoPostBack="true" CssClass="form-control"  OnSelectedIndexChanged="DDLLibelleVille_SelectedIndexChanged" runat="server"></asp:DropDownList>
                    </div>
                    <div class="col-md-3">
                        <label for="DDLLibelleCP">Code Postal</label>
                        <asp:DropDownList ID="DDLLibelleCP" AutoPostBack="true" CssClass="form-control" OnSelectedIndexChanged="DDLLibelleCP_SelectedIndexChanged" runat="server"></asp:DropDownList>
                    </div>
                </div>

                <div class="form-row mt-2 mb-2 ">
                    <div class="col-md-2  offset-md-4 text-right">
                        <asp:Button ID="BtnAjouter" CssClass="btn btn-primary" runat="server" OnClick="BtnAjouter_Click" Text="Ajouter" ></asp:Button>
                    </div>
                </div>
            </fieldset>
        </form>

    </section>
</asp:Content>
