﻿using fr.afcepf.ai107.nomads.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace fr.afcepf.ai107.nomads.Presentation
{
    public partial class FichePartenaire : System.Web.UI.Page
    {
        private int idPartenaire;
        protected void Page_Load(object sender, EventArgs e)
        {
            int.TryParse(Request["id"], out idPartenaire);  // Récupération de l'id dans l'entête de la requête

            GestionPartenaireBU bu = new GestionPartenaireBU();
            if (!IsPostBack)
            {

                // MasseurEntity masseur = bu.GetInfoMasseurById(idMasseur);
                #region INFORMATIONS DU PARTENAIRE

                DataTable dt = bu.GetInfoPartenairebyId(idPartenaire);

                
                lblNomPartenaire.Text = dt.Rows[0][1].ToString();
                
                lblRaisonSociale.Text = dt.Rows[0][2].ToString();

                lblNumTelephonePartenaire.Text= dt.Rows[0][4].ToString();
                lblAdresseMailPartenaire.Text = dt.Rows[0][3].ToString();
                // lblNom.Text = dt.Rows[0][1].ToString().ToUpper();
                //lblLibelleVille.Text = dt.Rows[0][2].ToString();


                // lblPrenom.Text = dt.Rows[0][0].ToString();

                //  string nom = DataBinder.Eval(Container.DataItem, "nom");
                //TODO: récupérer le bon ID
                #endregion



                //CatalogueBU catBu = new CatalogueBU();

                //DataTable dtCatBu = catBu.GetListePrestationByMasseur(idPartenaire);
                GestionPartenaireBU buPart = new GestionPartenaireBU();
                DataTable dtPrestPart = buPart.GetListePrestationByPartenaire(idPartenaire);
                gvPresta.DataSource = dtPrestPart;
                gvPresta.DataBind();

                btnRetourPrestaEnCours.Visible = false;

            }


            // gvPresta.Columns["id_prestation"].Visible = false;

        }

        protected void btnInscrption_Click(object sender, EventArgs e)
        {
            Session["Partenaire"] = idPartenaire;

            Response.Redirect("TableauDeBord.aspx"); //gestionPrestation.aspx

        }

        protected void btnHistoPresta_Click(object sender, EventArgs e)
        {
            CatalogueBU catBu = new CatalogueBU();

            DataTable dtCatBu = catBu.GetListePrestationByPartenaire(idPartenaire, DateTime.Now);
            //gvPresta.DataSource = dtCatBu;
            //gvPresta.DataBind();

            gtest.DataSource = dtCatBu;
            gtest.DataBind();
            gvPresta.Visible = false;
            btnInscrption.Visible = false;
            btnHistoPresta.Visible = false;
            btnRetourPrestaEnCours.Visible = true;
        }

        protected void gtest_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            String idPresta = (String)e.CommandArgument;
            Session["Partenaire"] = idPartenaire;
            Session["Prestation"] = idPresta;
            Response.Redirect("RetourExperiencePartenaire.aspx");

        }


        protected void btnRetourPrestaEnCours_Click(object sender, EventArgs e)
        {
            Response.Redirect("FichePartenaire.aspx?id=" + idPartenaire);
        }
    }
}