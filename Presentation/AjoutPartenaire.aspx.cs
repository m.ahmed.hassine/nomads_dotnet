﻿using fr.afcepf.ai107.nomads.Business;
using fr.afcepf.ai107.nomads.Entities;
using System;
using System.Collections.Generic;

namespace fr.afcepf.ai107.nomads.Presentation
{
    public partial class AjoutPartenaire : System.Web.UI.Page
    { private CatalogueBU bu = new CatalogueBU();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                List<TypePartenaireEntity> typePartenaire = bu.GetAllTypePartenaire();
                typePartenaire.Insert(0, new TypePartenaireEntity());
                DDLTypePartenaire.DataTextField = "LibelleTypePartenaire";
                DDLTypePartenaire.DataValueField = "IdTypePartenaire";
                DDLTypePartenaire.DataSource = typePartenaire;
                DDLTypePartenaire.DataBind();

                List<VilleEntity> ville = bu.GetListeVille();
                DDLLibelleVille.DataTextField = "LibelleVille";
                DDLLibelleVille.DataValueField = "IdVille";
                DDLLibelleVille.DataSource = ville;
                DDLLibelleVille.DataBind();
                DDLLibelleCP.DataTextField = "LibelleCP";
                DDLLibelleCP.DataValueField = "IdVille";
                DDLLibelleCP.DataSource = ville;
                DDLLibelleCP.DataBind();
            }
            }
        protected void DDLLibelleVille_SelectedIndexChanged(object sender, EventArgs e)
        {
            DDLLibelleCP.SelectedValue = DDLLibelleVille.SelectedValue;
        }
        protected void DDLLibelleCP_SelectedIndexChanged(object sender, EventArgs e)
        {
            DDLLibelleVille.SelectedValue = DDLLibelleCP.SelectedValue;
        }
        protected void BtnAjouter_Click(object sender, EventArgs e)
        {
            ContratPartenaireEntity contratPartenaire = new ContratPartenaireEntity();
            PartenaireEntity partenaire = new PartenaireEntity();
            partenaire.Nom = Request.Form["name"];
            partenaire.RaisonSociale = Request.Form["raison_sociale"];
            partenaire.NumeroVoie = Request.Form["number_adress"];
            partenaire.ComplementNumeroVoie = Request.Form["complement_number"];
            partenaire.NomVoie = Request.Form["street"];
            partenaire.IdVille = int.Parse(DDLLibelleVille.SelectedValue);
            partenaire.NumeroTelephone = Request.Form["phone"];
            partenaire.AdresseMail = Request.Form["mail_adress"];
            partenaire.DatePremierEnregistrement = DateTime.Now;
            partenaire.IdTypePartenaire = int.Parse(DDLTypePartenaire.SelectedValue);
            bu.AddPartenaire(partenaire);
            contratPartenaire.IdPartenaire = bu.GetLastIdContratPartenaire();
            contratPartenaire.DateDebutContratPartenaire = DateTime.Now;
            bu.AddContratPartenaire(contratPartenaire);
            System.Windows.Forms.MessageBox.Show("Partenaire ajouté avec succès !");
            Response.Redirect("GestionPartenaires.aspx");

        }
    }
}