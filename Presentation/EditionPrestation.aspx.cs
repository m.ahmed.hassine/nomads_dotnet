﻿using fr.afcepf.ai107.nomads.Business;
using fr.afcepf.ai107.nomads.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace fr.afcepf.ai107.nomads.Presentation
{
    public partial class WebForm3 : System.Web.UI.Page
    {
        private int idPrestation;
        protected void Page_Load(object sender, EventArgs e)
        {
            int.TryParse(Request["id_prestation"], out idPrestation);


            if (!IsPostBack)
            {
                CatalogueBU bu = new CatalogueBU();
                

                List<LibelleTypePaiementEntity> libelleTypePaiement = bu.GetListeLibelleTypePaiement();
                libelleTypePaiement.Insert(0, new LibelleTypePaiementEntity());
                DDLLibeleTypePaiement.DataTextField = "Libelle";
                DDLLibeleTypePaiement.DataValueField = "IdLibelleTypePaiement";
                DDLLibeleTypePaiement.DataSource = libelleTypePaiement;
                DDLLibeleTypePaiement.DataBind();


                List<VilleEntity> listeVille = bu.GetListeVille();
                listeVille.Insert(0, new VilleEntity());
                DDLLibelleVille.DataTextField = "LibelleVille";
                DDLLibelleVille.DataValueField = "IdVille";
                DDLLibelleVille.DataSource = listeVille;
                DDLLibelleVille.DataBind();

                DDLLibelleCP.DataTextField = "LibelleCP";
                DDLLibelleCP.DataValueField = "IdVille";
                DDLLibelleCP.DataSource = listeVille;
                DDLLibelleCP.DataBind();


                PrestationEntity machin = new PrestationEntity();
                machin = bu.GetPrestationById(idPrestation);

                TxtNbrMasseurs.Text = machin.NombreMasseurs.ToString();
                TxtLibelle.Text = machin.Libelle;
                txtDatePrestation.Text = machin.DatePrestation.ToString();
                TxtCoutMads.Text = machin.CoutMads.ToString();
                TxtMontantRemuneration.Text = machin.MontantRemuneration.ToString();
                TxtNbrChaisesErgonomique.Text = machin.NombreChaisesErgonomiques.ToString();
                TxtHeureDebut.Text = machin.HeureDebut;
                TxtHeureFin.Text = machin.HeureFin;
                TxtNumeroVoie.Text = machin.NumeroVoie;
                TxtComplementNum.Text = machin.ComplementNumVoie;
                TxtNomVoie.Text = machin.NomVoie;
                TxtSpecificites.Text = machin.Specificites;
                DDLLibelleVille.SelectedValue = machin.IdVille.ToString();

             
            }




        }

        protected void BtnValider_Click(object sender, EventArgs e)
        {
            CatalogueBU bu = new CatalogueBU();
            PrestationEntity prestation = new PrestationEntity();
            
            
            //prestation

            //libelle
            prestation.Libelle = TxtLibelle.Text;
            //date
            DateTime datePrestation;
            DateTime.TryParse(Request.Form["datePrestation"], out datePrestation);
            prestation.DatePrestation = datePrestation;
            //coutmads
            int coutMads;
            int.TryParse(TxtCoutMads.Text, out coutMads);
            prestation.CoutMads = coutMads;
            //heure début
            TimeSpan heureDebut;
            TimeSpan.TryParse(TxtHeureDebut.Text, out heureDebut);
            prestation.HeureDebut = heureDebut.ToString();
            //heurefin
            TimeSpan heureFin;
            TimeSpan.TryParse(TxtHeureFin.Text, out heureFin);
            prestation.HeureFin = heureFin.ToString();
            //montant rémunération
            double montantRemuneration;
            double.TryParse(TxtMontantRemuneration.Text, out montantRemuneration);
            prestation.MontantRemuneration = montantRemuneration;
            //nbre chaises egro
            int nbrChaisesErgonomiques;
            int.TryParse(TxtNbrChaisesErgonomique.Text, out nbrChaisesErgonomiques);
            prestation.NombreChaisesErgonomiques = nbrChaisesErgonomiques;
            // nombre masseurs
            int nbrMasseurs;
            int.TryParse(TxtNbrMasseurs.Text, out nbrMasseurs);
            prestation.NombreMasseurs = nbrMasseurs;
            //adresse complète et specififites
            prestation.NumeroVoie = TxtNumeroVoie.Text;
            prestation.ComplementNumVoie = TxtComplementNum.Text;
            prestation.NomVoie = TxtNomVoie.Text;
            if (DDLLibelleVille.SelectedIndex > 0)
            {
                prestation.IdVille = int.Parse(DDLLibelleVille.SelectedValue);
            }
           
            prestation.Specificites = TxtSpecificites.Text;

            prestation.Id = this.idPrestation;


            bu.UpdateInfoPrestation(prestation);
           


        }

        protected void DDLLibelleVille_SelectedIndexChanged(object sender, EventArgs e)
        {
            DDLLibelleCP.SelectedValue = DDLLibelleVille.SelectedValue;
        }

        protected void DDLLibelleCP_SelectedIndexChanged(object sender, EventArgs e)
        {
            DDLLibelleVille.SelectedValue = DDLLibelleCP.SelectedValue;
        }

        protected void DDLLibeleTypePaiement_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DDLLibeleTypePaiement.SelectedIndex == 0)
            {
                TxtMontantRemuneration.Text = "";
                TxtMontantFacturation.Text = "";
                TxtCoutMads.Text = "";
                TxtCoutMads.Enabled = false;
                TxtMontantFacturation.Enabled = false;
                TxtMontantRemuneration.Enabled = false;
            }
            if (DDLLibeleTypePaiement.SelectedIndex == 1)
            {
                TxtMontantRemuneration.Text = "";
                TxtMontantFacturation.Text = "";
                TxtCoutMads.Enabled = true;
                TxtMontantFacturation.Enabled = false;
                TxtMontantRemuneration.Enabled = false;
            }
            if (DDLLibeleTypePaiement.SelectedIndex == 2)
            {
                TxtCoutMads.Text = "";
                TxtCoutMads.Enabled = false;
                TxtMontantFacturation.Enabled = true;
                TxtMontantRemuneration.Enabled = true;
            }
        }
    }
}