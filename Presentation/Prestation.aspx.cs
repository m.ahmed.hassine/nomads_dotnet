﻿using fr.afcepf.ai107.nomads.Business;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace fr.afcepf.ai107.nomads.Presentation
{
    public partial class Prestation : System.Web.UI.Page
    {
        private int idPrestation;
        protected void Page_Load(object sender, EventArgs e)
        {
            int.TryParse(Request["id"], out idPrestation);

            CatalogueBU catbu = new CatalogueBU();

            int idm = idPrestation - 1; // l'indice dans la data table commence à 0 et non pas à 1
            DataTable detailPresta = catbu.GetPrestationDetails();

            lblNomPrestation.Text = detailPresta.Rows[idm][1].ToString();
            lblPartenaire.Text = detailPresta.Rows[idm][17].ToString();
            lblAdresse.Text = detailPresta.Rows[idm][12].ToString() + detailPresta.Rows[idm][13].ToString() + detailPresta.Rows[idm][14].ToString();
            lblVille.Text = detailPresta.Rows[idm][15].ToString() + detailPresta.Rows[idm][16].ToString();
            lblNumTel.Text = detailPresta.Rows[idm][21].ToString();


            int nbOcuPresta = catbu.GetNbreOccurencePresta(idPrestation);

            int nbMaxMasseurs = int.Parse(detailPresta.Rows[idm][5].ToString());

            int nbPlDispo = nbMaxMasseurs - nbOcuPresta;
            lblNbPlacesDispo.Text = nbPlDispo.ToString();


            GestionMasseurBU gesMBu = new GestionMasseurBU();

            gvMasseursInscrits.DataSource = gesMBu.GetListeMasseurByPrestation(idPrestation);
            gvMasseursInscrits.DataBind();


            

            if (Session["Masseur"] == null)
            {
              //  Label1.Text = null; // LABEL DE TEST ___ MASQUER LE BTN S INSCRIRE MAIS AFFICHER UN AUTRE BTN "INSCRIRE UN MASSEUR"? ATTENTION, PAS D'ID MASSEUR SI PAS DE SESSION MASSEUR
                btnInscription.Visible = false;
            }
            else
            { 
                string sess = Session["Masseur"].ToString();
              //  Label1.Text ="Test SessionMasseur : " + sess; // LABEL DE TEST
            }




                // Gestion de l'affichage du bouton d'inscription

                if (nbPlDispo<1)
            {
                btnInscription.Visible = false;
            }

            //   int soldeMads = (int) Session["Solde"];
            lblCoutMads.Text = "Coût en Mads : " + detailPresta.Rows[idm][7].ToString();
            int coutMads = Int32.Parse(detailPresta.Rows[idm][7].ToString());

            int soldeMads;
            if (Session["Solde"] == null)
            {
               // soldeMads = 0; // MASQUER LE BTN S INSCRIRE MAIS AFFICHER UN AUTRE BTN "INSCRIRE UN MASSEUR"? ATTENTION, PAS D'ID MASSEUR SI PAS DE SESSION MASSEUR
                btnInscription.Visible = false;
             }
            else
            {
               soldeMads = (int)Session["Solde"];
                if (coutMads > soldeMads )
                {
                    btnInscription.Visible = false;
                    lblMqMads.Visible = true;
                    lblMqMads.Text = "Tu n'as malheureusement pas assez de Mads pour cette Prestation!"; // Ajouter "Pas de panique, tu peux refaire le plein ICI"??? Gestion achat mads?
                }
                else
                {
                    lblMqMads.Visible = false;
                }
            }

        }

        protected void btnInscription_Click(object sender, EventArgs e)
        {
            GestionMasseurBU gesMass = new GestionMasseurBU();

            // Revérification du nombre de places disponibles
            CatalogueBU catbu = new CatalogueBU();
            int nbOcuPresta = catbu.GetNbreOccurencePresta(idPrestation);
            int idm = idPrestation - 1;
            DataTable detailPresta = catbu.GetPrestationDetails();
            int nbMaxMasseurs = int.Parse(detailPresta.Rows[idm][5].ToString());
            int nbPlDispo = nbMaxMasseurs - nbOcuPresta;

            int.TryParse(Request["id"], out idPrestation);
            int IdMass = (int)Session["Masseur"];
           
            if (nbPlDispo>0)
            {
                gesMass.AjouterMasseurPresta(IdMass, idPrestation);

                System.Windows.Forms.MessageBox.Show("Inscription validée !");

            }
            else
            {
                System.Windows.Forms.MessageBox.Show("Malheureusement, les dernières places ont été réservées...");
            }

                Response.Redirect("FicheMasseur.aspx?id=" + IdMass);
        }
    }
}