﻿using fr.afcepf.ai107.nomads.Business;
using fr.afcepf.ai107.nomads.Entities;
using System;
using System.Collections.Generic;
using System.Web.UI;

namespace fr.afcepf.ai107.nomads.Presentation
{
    public partial class CreationPrestation : System.Web.UI.Page
    {
        private PartenaireEntity partenaire;
        private VilleEntity ville;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CatalogueBU bu = new CatalogueBU();
                
                List<PartenaireEntity> partenaires = bu.GetListePartenaires();
                partenaires.Insert(0, new PartenaireEntity());
                ListePartenaires.DataTextField = "Nom";
                ListePartenaires.DataValueField = "IdPartenaire";
                ListePartenaires.DataSource = partenaires;
                ListePartenaires.DataBind();

                List<LibelleTypePaiementEntity> libelleTypePaiement = bu.GetListeLibelleTypePaiement();
                libelleTypePaiement.Insert(0, new LibelleTypePaiementEntity());
                DDLLibeleTypePaiement.DataTextField = "Libelle";
                DDLLibeleTypePaiement.DataValueField = "IdLibelleTypePaiement";
                DDLLibeleTypePaiement.DataSource = libelleTypePaiement;
                DDLLibeleTypePaiement.DataBind();

                partenaires.Insert(0, new PartenaireEntity());
                ListePartenaires.DataTextField = "Nom";
                ListePartenaires.DataValueField = "IdPartenaire";
                ListePartenaires.DataSource = partenaires;
                ListePartenaires.DataBind();

                List<MasseurEntity> masseurs = bu.GetListeMasseursByIdTypePartenaire(-1);
                DDLListeMasseurs.DataTextField = "NomEtPrenom";
                DDLListeMasseurs.DataValueField = "IdMasseur";
                DDLListeMasseurs.DataSource = masseurs;
                DDLListeMasseurs.DataBind();

                List<TypePartenaireEntity> typePartenaire = bu.GetAllTypePartenaire();
                typePartenaire.Insert(0, new TypePartenaireEntity());
                DDLTypePartenaire.DataTextField = "LibelleTypePartenaire";
                DDLTypePartenaire.DataValueField = "IdTypePartenaire";
                DDLTypePartenaire.DataSource = typePartenaire;
                DDLTypePartenaire.DataBind();

                List<LibelleRecurrenceEntity> libelleRecurrence = bu.GetListeLibelleRecurrence();
                libelleRecurrence.Insert(0, new LibelleRecurrenceEntity());
                DDLLibelleRecurrence.DataTextField = "Libelle";
                DDLLibelleRecurrence.DataValueField = "Id";
                DDLLibelleRecurrence.DataSource = libelleRecurrence;
                DDLLibelleRecurrence.DataBind();

                List<LibelleDureeRecurrenceEntity> dureeRecurrence = bu.GetListeLibelleDureeRecurrence();
                dureeRecurrence.Insert(0, new LibelleDureeRecurrenceEntity());
                DDLDureeRecurrence.DataTextField = "Libelle";
                DDLDureeRecurrence.DataValueField = "Id";
                DDLDureeRecurrence.DataSource = dureeRecurrence;
                DDLDureeRecurrence.DataBind();
                
                List<VilleEntity> listeVille = bu.GetListeVille();
                listeVille.Insert(0, new VilleEntity());
                DDLLibelleVille.DataTextField = "LibelleVille";
                DDLLibelleVille.DataValueField = "IdVille";
                DDLLibelleVille.DataSource = listeVille;
                DDLLibelleVille.DataBind();

                DDLLibelleCP.DataTextField = "LibelleCP";
                DDLLibelleCP.DataValueField = "IdVille";
                DDLLibelleCP.DataSource = listeVille;
                DDLLibelleCP.DataBind();

            }
            

        }
        protected void DDLTypePartenaire_SelectedIndexChanged(object sender, EventArgs e)
        {
            CatalogueBU bu = new CatalogueBU();
            List<MasseurEntity> masseurs = bu.GetListeMasseursByIdTypePartenaire(int.Parse(DDLTypePartenaire.SelectedValue));
            DDLListeMasseurs.DataTextField = "NomEtPrenom";
            DDLListeMasseurs.DataValueField = "IdMasseur";
            DDLListeMasseurs.DataSource = masseurs;
            DDLListeMasseurs.DataBind();

            List<PartenaireEntity> partenaires = bu.GetListePartenairesByIdCategorie(int.Parse(DDLTypePartenaire.SelectedValue));
            ListePartenaires.DataTextField = "Nom";
            ListePartenaires.DataValueField = "IdPartenaire";
            ListePartenaires.DataSource = partenaires;
            ListePartenaires.DataBind();
        }
        protected void datePrestation_OnChange(object sender, EventArgs e)
        {
            CatalogueBU bu = new CatalogueBU();
            DateTime datePrestation = DateTime.Parse(txtDatePrestation.Text);
           List<MasseurEntity> masseurs = bu.GetListeMasseursDispoInListByIdTypePartenaire(int.Parse(DDLTypePartenaire.SelectedValue), datePrestation);
            DDLListeMasseurs.DataTextField = "NomEtPrenom";
            DDLListeMasseurs.DataValueField = "IdMasseur";
            DDLListeMasseurs.DataSource = masseurs;
            DDLListeMasseurs.DataBind();
        }
            protected void ListePartenaires_SelectedIndexChanged(object sender, EventArgs e)
        {
                
                
                CatalogueBU bu = new CatalogueBU();
                TxtLibelle.Text = bu.GetPartenairesById(int.Parse(ListePartenaires.SelectedValue)).Nom;
                partenaire = bu.GetPartenairesById(int.Parse(ListePartenaires.SelectedValue));
                
                ville = bu.GetVilleById(partenaire.IdVille);
                TxtNumeroVoie.Text = partenaire.NumeroVoie;
                TxtComplementNum.Text = partenaire.ComplementNumeroVoie;
                TxtNomVoie.Text = partenaire.NomVoie;

                DDLLibelleVille.SelectedValue = partenaire.IdVille.ToString();
                DDLLibelleCP.SelectedValue = partenaire.IdVille.ToString();
            
        }
        protected void BtnValider_Click(object sender, EventArgs e)
        {
            EnregistrerDonnees();

            System.Windows.Forms.MessageBox.Show("Prestation ajoutée !");

            Response.Redirect("VoirPrestation.aspx");
        }
        protected void EnregistrerDonnees()
        {
            CatalogueBU bu = new CatalogueBU();
            PrestationEntity prestation = new PrestationEntity();
            RecurrenceEntity recurrence = new RecurrenceEntity();
            TypePaiementEntity typePaiement = new TypePaiementEntity();

            typePaiement.IdLibelleTypePaiement = int.Parse(DDLLibeleTypePaiement.SelectedValue);
            typePaiement.DateValidation = DateTime.Now;
            typePaiement.DateEmission = DateTime.Now;
            bu.AjoutNouveauTypePaiement(typePaiement);
            prestation.IdTypePaiement = bu.GetLastIdTypePaiement();

            DateTime datePrestation = DateTime.Parse(txtDatePrestation.Text);
            prestation.DatePrestation = datePrestation;

            prestation.Libelle = TxtLibelle.Text;

            if (int.Parse(DDLLibeleTypePaiement.SelectedValue) == 1)
            {
            int coutMads;
            int.TryParse(TxtCoutMads.Text, out coutMads);
            prestation.CoutMads = coutMads;
            }
            
            if (int.Parse(DDLLibeleTypePaiement.SelectedValue) == 2)
            {
            typePaiement.MontantFacturation = double.Parse(TxtMontantFacturation.Text);
            double montantRemuneration;
            double.TryParse(TxtMontantRemuneration.Text, out montantRemuneration);
            prestation.MontantRemuneration = montantRemuneration;
            }

            TimeSpan heureDebut;
            TimeSpan.TryParse(TxtHeureDebut.Text, out heureDebut);
            prestation.HeureDebut = heureDebut.ToString();

            TimeSpan heureFin;
            TimeSpan.TryParse(TxtHeureFin.Text, out heureFin);
            prestation.HeureFin = heureFin.ToString();

            int nbrChaisesErgonomiques;
            int.TryParse(TxtNbrChaisesErgonomique.Text, out nbrChaisesErgonomiques);
            prestation.NombreChaisesErgonomiques = nbrChaisesErgonomiques;

            int nbrMasseurs;
            int.TryParse(TxtNbrMasseurs.Text, out nbrMasseurs);
            prestation.NombreMasseurs = nbrMasseurs;

            prestation.NumeroVoie = TxtNumeroVoie.Text;
            prestation.ComplementNumVoie = TxtComplementNum.Text;
            prestation.NomVoie = TxtNomVoie.Text;

            prestation.IdVille = int.Parse(DDLLibelleVille.SelectedValue);
            prestation.IdPartenaire = int.Parse(ListePartenaires.SelectedValue);
            prestation.Specificites = TxtSpecificites.Text;

            if (DDLLibelleRecurrence.SelectedIndex > 0)
            {
                int delta;
                int.TryParse(TxtDelta.Text, out delta);
                recurrence.Delta = delta;

                int pas;
                int.TryParse(TxtPas.Text, out pas);
                recurrence.Pas = pas;

                int dureeRecurrence;
                int.TryParse(TxtDureeRecurrence.Text, out dureeRecurrence);
                recurrence.DureeRecurrence = dureeRecurrence;
                if (int.Parse(DDLLibelleRecurrence.SelectedValue) > 0)
                { 
                recurrence.IdLibelleRecurrence = int.Parse(DDLLibelleRecurrence.SelectedValue);
                }
                recurrence.IdLibelleDureeRecurrence = int.Parse(DDLDureeRecurrence.SelectedValue);
                bu.AjoutRecurrence(recurrence);
                prestation.IdRecurrence = bu.GetLastIdRecurrence();

            }
            bu.CreerPrestation(prestation);
        }
        protected void BtnAjoutPartenaire_Click(object sender, EventArgs e)
        {
            Response.Redirect("AjoutPartenaire.aspx");
        }

        protected void DDLLibeleTypePaiement_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DDLLibeleTypePaiement.SelectedIndex == 0)
            {
                TxtMontantRemuneration.Text = "";
                TxtMontantFacturation.Text = "";
                TxtCoutMads.Text = "";
                TxtCoutMads.Enabled = false;
                TxtMontantFacturation.Enabled = false;
                TxtMontantRemuneration.Enabled = false;
            }
            if (DDLLibeleTypePaiement.SelectedIndex == 1)
            {
                TxtMontantRemuneration.Text = "";
                TxtMontantFacturation.Text = "";
                TxtCoutMads.Enabled = true;
                TxtMontantFacturation.Enabled = false;
                TxtMontantRemuneration.Enabled = false;
            }
            if (DDLLibeleTypePaiement.SelectedIndex == 2)
            {
                TxtCoutMads.Text = "";
                TxtCoutMads.Enabled = false;
                TxtMontantFacturation.Enabled = true;
                TxtMontantRemuneration.Enabled = true;
            }
        }

        protected void DDLLibelleRecurrence_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DDLLibelleRecurrence.SelectedIndex == 0)
            {
                TxtPas.Enabled = false;
                TxtDelta.Enabled = false;
                TxtDureeRecurrence.Enabled = false;
                DDLDureeRecurrence.Enabled = false;
            }
            else
            {
                TxtPas.Text = "";
                TxtDelta.Text = "";
                TxtDureeRecurrence.Text = "";
                TxtPas.Enabled = true;
                TxtDelta.Enabled = true;
                TxtDureeRecurrence.Enabled = true;
                DDLDureeRecurrence.Enabled = true;
            }
            
        }

        protected void DDLLibelleVille_SelectedIndexChanged(object sender, EventArgs e)
        {
            DDLLibelleCP.SelectedValue = DDLLibelleVille.SelectedValue;
        }

        protected void DDLLibelleCP_SelectedIndexChanged(object sender, EventArgs e)
        {
            DDLLibelleVille.SelectedValue = DDLLibelleCP.SelectedValue;
        }

        protected void BtnEnregistrer_Click(object sender, EventArgs e)
        {
            EnregistrerDonnees();
            System.Windows.Forms.MessageBox.Show("Prestation enregistrée temporairement !");

            Response.Redirect("VoirPrestation.aspx");
        }
    }
}