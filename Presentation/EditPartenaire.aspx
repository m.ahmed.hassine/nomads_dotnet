﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Arche.Master" AutoEventWireup="true" CodeBehind="EditPartenaire.aspx.cs" Inherits="fr.afcepf.ai107.nomads.Presentation.EditPartenaire" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="principal" runat="server">
    <asp:Label ID="Label1" runat="server" Text="Gestion partenaires > Editer/Désactiver un partenaire" Font-Italic="True" Font-Overline="False" Font-Size="Large" ForeColor="#333333" Font-Bold="True"></asp:Label>
    <br />
    <section class="container-fluid" style="background-color: #E2E2E2; font-size: large;">
        <form class="needs-validation" method="post" id="reg_form">
            <fieldset>
                <div class="form-row">
                    <div class="col-md-2">
                        <label id="TypePartenaire" for="DDLTypePartenaire">Type Partenaire</label>
                        <asp:DropDownList ID="DDLTypePartenaire" class="form-control" AutoPostBack="true" runat="server"></asp:DropDownList>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-2">
                        <label for="name">Nom</label>
                        <asp:TextBox ID="name" Cssclass="form-control" runat="server"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <label for="raison_sociale">Raison Sociale</label>
                        <asp:TextBox ID="raison_sociale" Cssclass="form-control" runat="server"></asp:TextBox>
                    </div>
                    </div>
                <div class="form-row">
                        <div class="col-md-2">
                            <label for="phone">Téléphone</label>
                            <asp:TextBox ID="phone" Cssclass="form-control" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-4">
                            <label for="mail_adress">Adresse mail</label>
                            <asp:TextBox ID="mail_adress" Cssclass="form-control" aria-describedby="emailHelp" runat="server"></asp:TextBox>
                        </div>
                    </div>
                <div class="form-row">
                        <div class="col-md-1">
                            <label for="number_adress">Numéro</label>
                            <asp:TextBox ID="number_adress" Cssclass="form-control" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-1">
                            <label for="complement_number">Comp. num</label>
                        <asp:TextBox ID="complement_number" Cssclass="form-control" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-4">
                            <label for="street">Nom de la voie</label>
                        <asp:TextBox ID="street" Cssclass="form-control" runat="server"></asp:TextBox>
                        </div>

                    </div>
                <div class="form-row">
                    <div class="col-md-3">
                         <label for="DDLLibelleVille">Ville</label>
                        <asp:DropDownList ID="DDLLibelleVille" AutoPostBack="true" CssClass="form-control"  OnSelectedIndexChanged="DDLLibelleVille_SelectedIndexChanged" runat="server"></asp:DropDownList>
                    </div>
                    <div class="col-md-3">
                        <label for="DDLLibelleCP">Code Postal</label>
                        <asp:DropDownList ID="DDLLibelleCP" AutoPostBack="true" CssClass="form-control" OnSelectedIndexChanged="DDLLibelleCP_SelectedIndexChanged" runat="server"></asp:DropDownList>
                    </div>
                </div>
                <div class="form-row mt-2">
                        <div class="col-md-2">
                            <asp:Button ID="BtnMAJ" CssClass="btn btn-success" runat="server" OnClick="BtnMAJ_Click" Text="Mettre à Jour"></asp:Button>
                        </div>
                        <div class="col-md-2 offset-2 text-right">
                            <asp:Button ID="BtnDesactiver" CssClass="btn btn-danger" runat="server" OnClick="BtnDesactiver_Click" Text="Désactiver"></asp:Button>
                        </div>
               </div>
               <div class="form-row mt-2 mb-2">
                        <div class="col-md-2 offset-3">
                            <asp:DropDownList ID="DDLMotif" Cssclass="form-control" Visible="false" runat="server"></asp:DropDownList>
                        </div>
                        <div class="col-md-1 text-right">
                            <asp:Button ID="BtnValider" CssClass="btn btn-primary" Visible="false" OnClick="BtnValider_Click" runat="server" Text="    Valider  "></asp:Button>
                        </div>
                    </div>

            </fieldset>
        </form>
    </section>  
</asp:Content>
