﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Arche.Master" AutoEventWireup="true" CodeBehind="MajAnnulationPrestation.aspx.cs" Inherits="fr.afcepf.ai107.nomads.Presentation.WebForm1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

    <script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js'></script>

    <link href="css/StyleAnnulationPrestation.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="principal" runat="server">
    <header id="lol">
        <asp:Label ID="Label1" runat="server" Text="Gestion prestations > Annuler prestation" Font-Bold="True" Font-Italic="True" Font-Size="Medium" ForeColor="#333333"></asp:Label>
    </header>
    <center>
        <section id="sec">
          <div id="head">
                <asp:Label ID="lblPrestation" runat="server" Text="Gestion des prestations"></asp:Label>
          </div>
         <br />
         <br />
         <div id="label" class="form-row offset-3">
                <asp:Label ID="lbl_recherche" CssClass="lbl" runat="server" Text="Rechercher par :"></asp:Label>
         </div>
         <div id ="rech" class="form-row">
            <span id="esp1" class="col-md-2 offset-2 ">
                <asp:Label ID="Label2" CssClass="lbl" runat="server" Text="Partenaire :"></asp:Label>
            </span>
            <div class="input-group col-md-2">
              <asp:TextBox id="txtPartenaire" Cssclass="form-control" runat="server"></asp:TextBox>
            </div>
            <span id="esp2" class="col-md-2">
                <asp:Label ID="Label3" runat="server" CssClass="lbl" Text="Nom de prestation :"></asp:Label>
            </span>
            <span id="est2" class="col-md-2">
                <asp:DropDownList ID="DdlLibelle" CssClass="form-control" runat="server" AutoPostBack ="true"></asp:DropDownList>
            </span>
            <asp:Button ID="btnOk" runat="server" CssClass="btn btn-success"  OnClick="btnOk_Click" Text="Valider" />

        </div>
       <br />
       <br />
       <div id="gr">
           <asp:GridView AutoGenerateColumns="false" CssClass="grid" AlternatingRowStyle-VerticalAlign="Middle" RowStyle-HorizontalAlign ="Center"
            ID="gridListe" runat="server" OnRowCommand ="gridListe_RowCommand">
                <Columns>
                    <asp:BoundField DataField="nom_partenaire" HeaderText="Partenaires" />
                    <asp:BoundField DataField="libelle_prestation" HeaderText="Prestations à venir" />
                    <asp:BoundField DataField="DATE_FORMAT(a.date_prestation,GET_FORMAT(date,'EUR'))" HeaderText="Dates" />
                    <asp:BoundField DataField="nombre_masseurs" HeaderText="Nombre de masseurs<br/> requis" HtmlEncode="false" />
                    <asp:TemplateField>
                        <ItemTemplate>
                             <asp:Button ID="btnAnnuler" runat="server" CommandName="annuler" CssClass="btn btn-danger" CommandArgument='<%# Bind("id_prestation") %>'
                                 Text="Annuler" />
                        </ItemTemplate>
                    </asp:TemplateField>
        
                </Columns>
            </asp:GridView>
       </div> 
    </section>
  </center>
</asp:Content>
