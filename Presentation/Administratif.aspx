﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Arche.Master" AutoEventWireup="true" CodeBehind="Administratif.aspx.cs" Inherits="fr.afcepf.ai107.nomads.Presentation.Administratif" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/TestStyleBtn.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="principal" runat="server">
    <p>
        <table>
            <tr>
              <td>
            <br />
            <asp:Button ID="btnRetourExperienceMasseur" runat="server" Text="Effectuer un retour experience masseur" PostBackUrl="https://localhost:44311/RetourExperienceMasseur.aspx" CssClass="Btn_Accueil" ForeColor="#000099" />
            <br />
              </td>
            </tr>

            <tr>
              <td>
            <br />
        <asp:Button ID="btnRetourExperiencePartenaire" runat="server" Text="Effectuer un retour experience partenaire" PostBackUrl="https://localhost:44311/RetourExperiencePartenaire.aspx" CssClass="Btn_Accueil" ForeColor="#006600" />
            <br />
              </td>
            </tr>
        </table>
    
  
</asp:Content>
