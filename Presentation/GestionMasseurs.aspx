﻿<%@ Page Title="Page Gestion Masseurs" Language="C#" MasterPageFile="~/Arche.Master" AutoEventWireup="true" CodeBehind="GestionMasseurs.aspx.cs" Inherits="fr.afcepf.ai107.nomads.Presentation.GestionMasseurs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/StyleGestionMasseurs.css" rel="stylesheet" />
    <link href="css/TestStyleBtn.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="principal" runat="server">
    <form id="general">
        <asp:Label ID="Label1" runat="server" Text="Gestion masseurs > Voir liste masseur" CssClass="lbl" Font-Bold="True" Font-Italic="True" Font-Size="Medium" ForeColor="#333333" ></asp:Label>
           <div>
            <h2 style="color: #0000FF; font-family: Calibri; font-size: x-large">Ci dessous vous trouverez toute la liste de nos Super masseurs : </h2>
               <p style="color: #0000FF; font-family: Calibri; font-size: large">&nbsp;</p>
           </div>
           <!-- <div id="table">
            <asp:GridView ID="gvListeMasseurs" runat="server"></asp:GridView>

        </div> -->
          <div style="overflow: auto">
            <asp:Panel ID="Panel1" runat="server" ScrollBars="Vertical" Height="450px" width="1300px">
            <asp:Repeater ID="rptMasseurInfoMin" runat="server" >
                <HeaderTemplate >
                      <table style="width: 100%;">

                    <table class="masseur" border="1">
                        <tr>
                            <th>
                                <h2>Nom</h2>
                            </th>
                            <th>
                                <h2>Prénom</h2>
                            </th>
                            <th>
                                <h2>Adresse</h2>
                            </th>
                            <th>
                                <h2>Détail</h2>
                            </th>
                            </tr>
                  </HeaderTemplate>

                <ItemTemplate>

                    <tr>
                        <td>
                            <header><%# DataBinder.Eval(Container.DataItem, "nom")%></header>
                        </td>
                        <td>
                            <p><%# DataBinder.Eval(Container.DataItem, "prenom")%> </p>
                        </td>
                        <td>
                            <p><%# DataBinder.Eval(Container.DataItem, "numero_de_voie")%>  <%# DataBinder.Eval(Container.DataItem, "complement_numero_de_voie")%>  <%# DataBinder.Eval(Container.DataItem, "nom_de_voie")%>  <%# DataBinder.Eval(Container.DataItem, "libelle_cp")%>  <%# DataBinder.Eval(Container.DataItem, "libelle_ville")%></p>
                        </td>
                        <td>
                            <a href="FicheMasseur.aspx?id=<%# DataBinder.Eval(Container.DataItem, "id_masseur")%>">Voir détails</a>
                        </td>
                    </tr>
                </ItemTemplate>

                <FooterTemplate>
                    </table>

                </FooterTemplate>
            </asp:Repeater>
                </asp:Panel>
          
        </div>





    </form>


</asp:Content>
