﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Arche.Master" AutoEventWireup="true" CodeBehind="EditionMasseurs.aspx.cs" Inherits="fr.afcepf.ai107.nomads.Presentation.EditionMasseurs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/StyleGestionMasseurs.css" rel="stylesheet" />
    <link href="css/TestStyleBtn.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="principal" runat="server">
    <form id="general">
        <div>
            <h2 style="color: #0000FF; font-family: Calibri; font-size: x-large">Ci dessous vous trouverez toute la liste de nos Super masseurs : </h2>
            <p style="color: #0000FF; font-family: Calibri; font-size: large">&nbsp;</p>
        </div>
        <div style="overflow: auto">
            <asp:Panel ID="Panel1" runat="server" ScrollBars="Vertical" Height="450px" Width="1300px">
                <asp:Repeater ID="rptMasseurInfoMin" runat="server">
                    <HeaderTemplate>
                        <table style="width: 100%;">

                            <table class="masseur" border="1">
                                <tr>
                                    <th>
                                        <h2>Nom</h2>
                                    </th>
                                    <th>
                                        <h2>Prénom</h2>
                                    </th>
                                    <th>
                                        <h2>Catégorie</h2>
                                    </th>
                                    <th>
                                        <h2>Niveau de compétences</h2>
                                    </th>
                                </tr>
                    </HeaderTemplate>

                    <ItemTemplate>

                        <tr>
                            <td>
                                <header><%# DataBinder.Eval(Container.DataItem, "nom")%></header>
                            </td>
                            <td>
                                <p><%# DataBinder.Eval(Container.DataItem, "prenom")%> </p>
                            </td>
                            <td>
                                <p><%# DataBinder.Eval(Container.DataItem, "libelle_categorie")%> </p>
                            </td>
                            <td>
                                <p><%# DataBinder.Eval(Container.DataItem, "libelle_niveau_competence")%> </p>
                            </td>
                            <td>
                                <a href="EditMasseur.aspx?id=<%# DataBinder.Eval(Container.DataItem, "id_masseur")%>">Mettre à jour</a>
                            </td>
                        </tr>
                    </ItemTemplate>

                    <FooterTemplate>
                        </table>

                    </FooterTemplate>
                </asp:Repeater>
            </asp:Panel>

        </div>





    </form>
</asp:Content>