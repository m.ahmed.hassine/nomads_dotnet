﻿using fr.afcepf.ai107.nomads.Business;
using fr.afcepf.ai107.nomads.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace fr.afcepf.ai107.nomads.Presentation
{
    public partial class EditPartenaire : System.Web.UI.Page
    {
        private CatalogueBU bu = new CatalogueBU();
        private int idPartenaire;
        protected void Page_Load(object sender, EventArgs e)
        {
            int.TryParse(Request["id"], out idPartenaire);
            PartenaireEntity partenaire;
            if (!IsPostBack)
            {
                partenaire = bu.GetPartenairesById(idPartenaire);
                List<TypePartenaireEntity> typePartenaire = bu.GetAllTypePartenaire();
                DDLTypePartenaire.DataTextField = "LibelleTypePartenaire";
                DDLTypePartenaire.DataValueField = "IdTypePartenaire";
                DDLTypePartenaire.SelectedValue = partenaire.IdTypePartenaire.ToString();
                DDLTypePartenaire.DataSource = typePartenaire;
                DDLTypePartenaire.DataBind();
                List<VilleEntity> ville = bu.GetListeVille();
                DDLLibelleVille.DataTextField = "LibelleVille";
                DDLLibelleVille.DataValueField = "IdVille";
                DDLLibelleVille.SelectedValue = partenaire.IdVille.ToString();
                DDLLibelleVille.DataSource = ville;
                DDLLibelleVille.DataBind();
                DDLLibelleCP.DataTextField = "LibelleCP";
                DDLLibelleCP.DataValueField = "IdVille";
                DDLLibelleCP.SelectedValue = partenaire.IdVille.ToString();
                DDLLibelleCP.DataSource = ville;
                DDLLibelleCP.DataBind();

                name.Text = partenaire.Nom;
                raison_sociale.Text = partenaire.RaisonSociale;
                number_adress.Text = partenaire.NumeroVoie;
                complement_number.Text = partenaire.ComplementNumeroVoie;
                street.Text = partenaire.NomVoie;
                phone.Text = partenaire.NumeroTelephone;
                mail_adress.Text = partenaire.AdresseMail;
                }

        }
        protected void DDLLibelleVille_SelectedIndexChanged(object sender, EventArgs e)
        {
            DDLLibelleCP.SelectedValue = DDLLibelleVille.SelectedValue;
        }
        protected void DDLLibelleCP_SelectedIndexChanged(object sender, EventArgs e)
        {
            DDLLibelleVille.SelectedValue = DDLLibelleCP.SelectedValue;
        }
        protected void BtnMAJ_Click(object sender, EventArgs e)
        {
                PartenaireEntity partenaire = new PartenaireEntity();
                partenaire.Nom = name.Text;
                partenaire.RaisonSociale = raison_sociale.Text;
                partenaire.NumeroVoie = number_adress.Text;
                partenaire.ComplementNumeroVoie = complement_number.Text;
                partenaire.NomVoie = street.Text;
                partenaire.IdVille = int.Parse(DDLLibelleVille.SelectedValue);
                partenaire.NumeroTelephone = phone.Text;
                partenaire.AdresseMail = mail_adress.Text;
                partenaire.IdTypePartenaire = int.Parse(DDLTypePartenaire.SelectedValue);
                partenaire.IdPartenaire = idPartenaire;
                bu.MAJPartenaire(partenaire);
                System.Windows.Forms.MessageBox.Show("Partenaire mis à jour avec succès !");
                Response.Redirect("EditionPartenaires.aspx");

        }

        protected void BtnDesactiver_Click(object sender, EventArgs e)
        {
            CatalogueBU bu = new CatalogueBU();
            List<MotifFinContratPartenaireEntity> motifs = bu.GetListeLibelleMotifFinContratPartenaire();
            DDLMotif.DataTextField = "LibelleMotifFinContratPartenaire";
            DDLMotif.DataValueField = "IdMotifFinContratPartenaire";
            DDLMotif.DataSource = motifs;
            DDLMotif.DataBind();
            DDLMotif.Visible = true;
            BtnValider.Visible = true;
        }

        protected void BtnValider_Click(object sender, EventArgs e)
        {
            ContratPartenaireEntity contratPartenaire = new ContratPartenaireEntity();
            contratPartenaire.DateFinContratPartenaire = DateTime.Now;
            contratPartenaire.IdPartenaire = idPartenaire;
            contratPartenaire.IdMotifFinContrat = int.Parse(DDLMotif.SelectedValue);
            bu.SetEndContratPartenaire(contratPartenaire);
            System.Windows.Forms.MessageBox.Show("Partenaire désactivé avec succès !");
            Response.Redirect("EditionPartenaires.aspx");
            
        }
    }
}