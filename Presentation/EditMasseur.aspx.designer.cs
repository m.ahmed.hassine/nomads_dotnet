﻿//------------------------------------------------------------------------------
// <généré automatiquement>
//     Ce code a été généré par un outil.
//
//     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
//     le code est régénéré.
// </généré automatiquement>
//------------------------------------------------------------------------------

namespace fr.afcepf.ai107.nomads.Presentation
{


    public partial class EditionMasseur
    {

        /// <summary>
        /// Contrôle Label1.
        /// </summary>
        /// <remarks>
        /// Champ généré automatiquement.
        /// Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label1;

        /// <summary>
        /// Contrôle DDLSexe.
        /// </summary>
        /// <remarks>
        /// Champ généré automatiquement.
        /// Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList DDLSexe;

        /// <summary>
        /// Contrôle DDLCategorie.
        /// </summary>
        /// <remarks>
        /// Champ généré automatiquement.
        /// Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList DDLCategorie;

        /// <summary>
        /// Contrôle DDLNiveauCompetence.
        /// </summary>
        /// <remarks>
        /// Champ généré automatiquement.
        /// Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList DDLNiveauCompetence;

        /// <summary>
        /// Contrôle first_name.
        /// </summary>
        /// <remarks>
        /// Champ généré automatiquement.
        /// Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox first_name;

        /// <summary>
        /// Contrôle last_name.
        /// </summary>
        /// <remarks>
        /// Champ généré automatiquement.
        /// Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox last_name;

        /// <summary>
        /// Contrôle birth_date.
        /// </summary>
        /// <remarks>
        /// Champ généré automatiquement.
        /// Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox birth_date;

        /// <summary>
        /// Contrôle phone.
        /// </summary>
        /// <remarks>
        /// Champ généré automatiquement.
        /// Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox phone;

        /// <summary>
        /// Contrôle mail_adress.
        /// </summary>
        /// <remarks>
        /// Champ généré automatiquement.
        /// Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox mail_adress;

        /// <summary>
        /// Contrôle number_adress.
        /// </summary>
        /// <remarks>
        /// Champ généré automatiquement.
        /// Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox number_adress;

        /// <summary>
        /// Contrôle complement_number.
        /// </summary>
        /// <remarks>
        /// Champ généré automatiquement.
        /// Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox complement_number;

        /// <summary>
        /// Contrôle street.
        /// </summary>
        /// <remarks>
        /// Champ généré automatiquement.
        /// Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox street;

        /// <summary>
        /// Contrôle DDLLibelleVille.
        /// </summary>
        /// <remarks>
        /// Champ généré automatiquement.
        /// Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList DDLLibelleVille;

        /// <summary>
        /// Contrôle DDLLibelleCP.
        /// </summary>
        /// <remarks>
        /// Champ généré automatiquement.
        /// Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList DDLLibelleCP;

        /// <summary>
        /// Contrôle BtnMAJ.
        /// </summary>
        /// <remarks>
        /// Champ généré automatiquement.
        /// Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button BtnMAJ;

        /// <summary>
        /// Contrôle BtnDesactiver.
        /// </summary>
        /// <remarks>
        /// Champ généré automatiquement.
        /// Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button BtnDesactiver;

        /// <summary>
        /// Contrôle DDLMotif.
        /// </summary>
        /// <remarks>
        /// Champ généré automatiquement.
        /// Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList DDLMotif;

        /// <summary>
        /// Contrôle BtnValider.
        /// </summary>
        /// <remarks>
        /// Champ généré automatiquement.
        /// Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button BtnValider;
    }
}
