﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Arche.Master" AutoEventWireup="true" CodeBehind="EditionPartenaires.aspx.cs" Inherits="fr.afcepf.ai107.nomads.Presentation.EditionPartenaires" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/StyleGestionMasseurs.css" rel="stylesheet" />
    <link href="css/TestStyleBtn.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="principal" runat="server">
    <form id="general">
        <div>
            <h2 style="color: #0000FF; font-family: Calibri; font-size: x-large">Ci dessous vous trouverez toute la liste de nos partenaires : </h2>
            <p style="color: #0000FF; font-family: Calibri; font-size: large">&nbsp;</p>
        </div>
        <div style="overflow: auto">
            <asp:Panel ID="Panel1" runat="server" ScrollBars="Vertical" Height="450px" Width="1300px">
                <asp:Repeater ID="rptPartenaireInfoMin" runat="server">
                    <HeaderTemplate>
                        <table style="width: 100%;">

                            <table class="masseur" border="1">
                                <tr>
                                    <th>
                                        <h2>Nom</h2>
                                    </th>
                                    <th>
                                        <h2>Raison Sociale</h2>
                                    </th>
                                    <th>
                                        <h2>Type Partenaire</h2>
                                    </th>
                                    <th>
                                        <h2>Ville</h2>
                                    </th>
                                </tr>
                    </HeaderTemplate>

                    <ItemTemplate>

                        <tr>
                            <td>
                                <header><%# DataBinder.Eval(Container.DataItem, "nom")%></header>
                            </td>
                            <td>
                                <p><%# DataBinder.Eval(Container.DataItem, "raison_sociale")%> </p>
                            </td>
                            <td>
                                <p><%# DataBinder.Eval(Container.DataItem, "libelle_type_partenaire")%> </p>
                            </td>
                            <td>
                                <p><%# DataBinder.Eval(Container.DataItem, "libelle_ville")%> </p>
                            </td>
                            <td>
                                <a href="EditPartenaire.aspx?id=<%# DataBinder.Eval(Container.DataItem, "id_partenaire")%>">Mettre à jour</a>
                            </td>
                        </tr>
                    </ItemTemplate>

                    <FooterTemplate>
                        </table>

                    </FooterTemplate>
                </asp:Repeater>
            </asp:Panel>

        </div>





    </form>
</asp:Content>