﻿using fr.afcepf.ai107.nomads.Business;
using fr.afcepf.ai107.nomads.Entities;
using System;
using System.Collections.Generic;
using System.Web.UI;

namespace fr.afcepf.ai107.nomads.Presentation
{
    public partial class RetourExperiencePartenaire : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CatalogueBU bu = new CatalogueBU();
                
                lblIdPartenaire.Text = Session["Partenaire"].ToString();
                lblIdPrestation.Text = Session["Prestation"].ToString();
                
                List<TypeEvaluationPartenaireEntity> liste = bu.GetListeTypeEvaluationPartenaire();
                rbTypeEvaluationPartenaire.DataTextField = "LibelleTypeEvaluationPartenaire";
                rbTypeEvaluationPartenaire.DataValueField = "IdTypeEvaluationPartenaire";
                rbTypeEvaluationPartenaire.DataSource = liste;
                rbTypeEvaluationPartenaire.DataBind();
            }
        }

           protected void btnValiderPartenaire_Click(object sender, EventArgs e)
                       
        {
            CatalogueBU bu = new CatalogueBU();

            RetourExperiencePartenaireEntity retourPartenaire = new RetourExperiencePartenaireEntity();
            retourPartenaire.Commentaire = txtCommentairePartenaire.Text;

            int idPartenaire;
            int.TryParse(lblIdPartenaire.Text, out idPartenaire);
            retourPartenaire.IdPartenaire = idPartenaire;

            int idPrestation;
            int.TryParse(lblIdPrestation.Text, out idPrestation);
            retourPartenaire.IdPrestation = idPrestation;

            int idTypeEvaluationPartenaire;
            int.TryParse(lblSelectionEvaluationPartenaire.Text, out idTypeEvaluationPartenaire);
            retourPartenaire.IdTypeEvaluationPartenaire = idTypeEvaluationPartenaire;

            int idTypeEvalPartenaire;
            Int32.TryParse(rbTypeEvaluationPartenaire.SelectedValue, out idTypeEvalPartenaire);


            bu.AjouterCommentairePartenaire(retourPartenaire, idTypeEvalPartenaire);

            System.Windows.Forms.MessageBox.Show("Commentaire partenaire ajouté !");


            Response.Redirect("Accueil.aspx");
        }
    }
}