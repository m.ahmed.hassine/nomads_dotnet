﻿<%@ Page Title="Page Fiche Partenaire" Language="C#" MasterPageFile="~/Arche.Master" AutoEventWireup="true" CodeBehind="FichePartenaire.aspx.cs" Inherits="fr.afcepf.ai107.nomads.Presentation.FichePartenaire" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <link href="css/StyleFichePartenaire.css" rel="stylesheet" />
    <link href="css/TestStyleBtn.css" rel="stylesheet" />

        <style type="text/css">
            .auto-style6 {
                width: 100px;
                height: 88px;
            }
            .auto-style7 {
                width: 329px;
            }
            .auto-style8 {
                width: 357px;
            }
            .auto-style9 {
                width: 332px;
            }
            .auto-style10 {
                width: 360px;
            }
            .auto-style11 {
                width: 493px;
            }
        </style>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="principal" runat="server">
    <form id="contenu">
        <asp:Panel runat="server">
            <div class="fond"><table>
                <tr><td style="font-family: 'Arial Black'; color: #0000FF">
             <img class="auto-style6" src="images/avatar_partenaire.png" alt="photo avatar partenaire" />

                    <br />
                    Fiche Partenaire</td>
                    <td></td>
                    <td class="auto-style10">
                        <ul>
                           <li> <asp:Label ID="Label1" runat="server" Text="Nom: " CssClass="lbl2"></asp:Label> <asp:Label ID="lblNomPartenaire" runat="server" Text="Label" CssClass="lbl1"></asp:Label></li> 
                            <li> <asp:Label ID="Label2" runat="server" Text="Raison sociale: " CssClass="lbl2"></asp:Label><asp:Label ID="lblRaisonSociale" runat="server" Text="Label" CssClass="lbl1"></asp:Label>  </li>
                        </ul>
                    </td>
                    <td class="auto-style11">
                        <ul>
                            <li> <asp:Label ID="Label3" runat="server" Text="N°Téléphone: " CssClass="lbl2" ></asp:Label> <asp:Label ID="lblNumTelephonePartenaire" runat="server" Text="label" CssClass="lbl1" ></asp:Label></li>
                            <li> <asp:Label ID="Label4" runat="server" Text="Adresse mail: " CssClass="lbl2" ></asp:Label> <asp:Label ID="lblAdresseMailPartenaire" runat="server" Text="label" CssClass="lbl1"  ></asp:Label></li>
                        </ul>

                    </td>
                                       
                    <td class="auto-style9">
                        <asp:Button class="bouton" ID="btnHistoPresta" runat="server" Text="Voir mes prestations passées" OnClick="btnHistoPresta_Click" CssClass="bouton" />
                          <br />
                        <br />              
                        <asp:Button class="bouton" ID="btnRetourPrestaEnCours" runat="server" Text="Retour à mes prestations en cours" OnClick="btnRetourPrestaEnCours_Click" CssClass="bouton" />

                    </td>
                </tr>
            </table>

            </div>    
            </asp:Panel>
  
   <center>
       <div id="PrestaEnCours" style="font-family: Arial; font-size: small">
        <h2>Mes prestations en cours</h2>
    <asp:GridView AutoGenerateColumns="false" ID="gvPresta" runat="server" class="greedPresta" emptydatatext="Vous n'avez actuellement aucune prestation prévue!" CssClass="grid">
        <columns> 
        <asp:BoundField DataField="libelle" HeaderText="Nom de la prestation" />
        <asp:BoundField DataField="date_prestation" HeaderText="Date de la prestation" />
        <asp:BoundField DataField="heure_debut" HeaderText="Heure de début" />
        <asp:BoundField DataField="heure_fin" HeaderText="Heure de fin" />
        <asp:BoundField DataField="nombre_masseurs" HeaderText="Nombre de masseurs demandés" />
        </columns>
    </asp:GridView>
        
     <!--   <asp:Button class="bouton" ID="btnInscrption" runat="server" Text="M'inscrire à une nouvelle prestation" OnClick="btnInscrption_Click"/>-->
<%-- ------------------------------------------------------------------------------------------------------------------------------------------------------------------ --%>

        <asp:GridView AutoGenerateColumns="false" ID="gtest" runat="server" OnRowCommand="gtest_RowCommand" emptydatatext="Vous n'avez aucune prestation passée!" CssClass="grid"  > <%-- AlternatingRowStyle-CssClass="alt" PagerStyle-CssClass="pgr"--%> 
             <columns> 
        <asp:BoundField DataField="libelle" HeaderText="Nom de la prestation" />
        <asp:BoundField DataField="date_prestation" HeaderText="Date de la prestation" />
       <asp:BoundField DataField="heure_debut" HeaderText="Heure de début" />
        <asp:BoundField DataField="heure_fin" HeaderText="Heure de fin" />
        <asp:BoundField DataField="nombre_masseurs" HeaderText="Nombre de masseurs demandés" />
        <asp:TemplateField>
            <ItemTemplate>
                <asp:Button CssClass="btnVal" ID="btnRetourExp" runat="server" CommandName="Rex" CommandArgument='<%#Bind ("id_prestation") %>' Text="Faire mon retour d'expérience" />
            </ItemTemplate>
        </asp:TemplateField>

        </columns>

        </asp:GridView>
        
    </div>
</center>
    
</form>


</asp:Content>
