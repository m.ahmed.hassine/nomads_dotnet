﻿using fr.afcepf.ai107.nomads.Business;
using fr.afcepf.ai107.nomads.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace fr.afcepf.ai107.nomads.Presentation
{
    public partial class AjoutMasseur : System.Web.UI.Page
    {   private CatalogueBU bu = new CatalogueBU();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                
                List<SexeEntity> sexe = bu.GetListeSexe();
                sexe.Insert(0, new SexeEntity());
                DDLSexe.DataTextField = "LibelleSexe";
                DDLSexe.DataValueField = "IdSexe";
                DDLSexe.DataSource = sexe;
                DDLSexe.DataBind();
                
                List<CategorieEntity> categorie = bu.GetAllCategorie();
                DDLCategorie.DataTextField = "LibelleCategorie";
                DDLCategorie.DataValueField = "IdCategorie";
                DDLCategorie.DataSource = categorie;
                DDLCategorie.DataBind();
                
                List<NiveauCompetenceEntity> niveauCompetence = bu.GetListeNiveauCompetence();
                DDLNiveauCompetence.DataTextField = "LibelleNiveauCompetence";
                DDLNiveauCompetence.DataValueField = "IdStatut";
                DDLNiveauCompetence.DataSource = niveauCompetence;
                DDLNiveauCompetence.DataBind();

                List<VilleEntity> ville = bu.GetListeVille();
                DDLLibelleVille.DataTextField = "LibelleVille";
                DDLLibelleVille.DataValueField = "IdVille";
                DDLLibelleVille.DataSource = ville;
                DDLLibelleVille.DataBind();
                DDLLibelleCP.DataTextField = "LibelleCP";
                DDLLibelleCP.DataValueField = "IdVille";
                DDLLibelleCP.DataSource = ville;
                DDLLibelleCP.DataBind();
            }
        }
        protected void DDLLibelleVille_SelectedIndexChanged(object sender, EventArgs e)
        {
            DDLLibelleCP.SelectedValue = DDLLibelleVille.SelectedValue;
        }

        protected void DDLLibelleCP_SelectedIndexChanged(object sender, EventArgs e)
        {
            DDLLibelleVille.SelectedValue = DDLLibelleCP.SelectedValue;
        }

        protected void BtnAjouter_Click(object sender, EventArgs e)
        {
            AptitudeEntity aptitude = new AptitudeEntity();
            EvolutionProfessionnelleEntity evolutionProfessionnelle = new EvolutionProfessionnelleEntity();
            MasseurEntity nvMasseur = new MasseurEntity();
            ContratMasseurEntity contratMasseur = new ContratMasseurEntity();
            nvMasseur.Nom = Request.Form["first_name"];
            nvMasseur.Prenom = Request.Form["last_name"];
            nvMasseur.NumeroVoie = Request.Form["number_adress"];
            nvMasseur.ComplementNumeroVoie = Request.Form["complement_number"];
            nvMasseur.NomVoie = Request.Form["street"];
            nvMasseur.DateNaissance = DateTime.Parse(Request.Form["birth_date"]);
            nvMasseur.IdVille = int.Parse(DDLLibelleVille.SelectedValue);
            nvMasseur.NumeroTelephone = Request.Form["phone"];
            nvMasseur.AdresseMail = Request.Form["mail_adress"];
            nvMasseur.DatePremierEnregistrement = DateTime.Now;
            nvMasseur.IdSexe = int.Parse(DDLSexe.SelectedValue);

            aptitude.IdMasseur = bu.GetLastIdMasseur() + 1;
            aptitude.IdCategorie = int.Parse(DDLCategorie.SelectedValue);
            aptitude.DateDebut = DateTime.Now;
            bu.AddAptitude(aptitude);

            evolutionProfessionnelle.IdNiveauCompetences = int.Parse(DDLNiveauCompetence.SelectedValue);
            evolutionProfessionnelle.DateEvolution = DateTime.Now;
            evolutionProfessionnelle.IdMasseur = bu.GetLastIdMasseur();
            bu.AddEvolutionProfessionnelle(evolutionProfessionnelle);

            nvMasseur.IdAptitude = bu.GetLastIdAptitude();
            nvMasseur.IdEvolutionPro = bu.GetLastIdEvolutionProfessionnelle();
            bu.AddMasseur(nvMasseur);
            contratMasseur.IdMasseur = bu.GetLastIdMasseur();
            contratMasseur.DateDebutContratMasseur = DateTime.Now;
            bu.AddContratMasseur(contratMasseur);
            
            System.Windows.Forms.MessageBox.Show("Masseur ajouté avec succès !");

            Response.Redirect("GestionMasseurs.aspx");

        }
        /*
        protected void BtnTest_Click(object sender, EventArgs e)
        {
            TxtNom.Text = "Dupont";
            TxtPrenom.Text = "Jean";
            txtDateNaissance.Text = "15/01/1983";
            TxtNumeroVoie.Text = "10";
            TxtComplementNum.Text = "bis";
            TxtNomVoie.Text = "Avenue des chalands";
            TxtAdresseMail.Text = "j.dupont@gmail.com";
            TxtTelephone.Text = "06 06 06 04 08";
        }
        */
    }
}