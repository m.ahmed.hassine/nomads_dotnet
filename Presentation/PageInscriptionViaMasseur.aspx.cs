﻿using fr.afcepf.ai107.nomads.Business;
using fr.afcepf.ai107.nomads.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.DataVisualization.Charting;
using MySql.Data.MySqlClient;

namespace fr.afcepf.ai107.nomads.Presentation
{
    public partial class TableauDeBord : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CatalogueBU bu = new CatalogueBU();
                int idMasseur = (int)(Session["Masseur"]);

                gvPrestation.DataSource = bu.GetPrestationDetailsDispoByIDAndDate(idMasseur, DateTime.Now);
                gvPrestation.DataBind();
            }

          //  Session["MasseurOri"] = Session["Masseur"];
        }

        
        }

    }
