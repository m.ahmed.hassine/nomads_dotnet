﻿<%@ Page Title="Page Tableau de bord" Language="C#" MasterPageFile="~/Arche.Master" AutoEventWireup="true" CodeBehind="PageInscriptionViaMasseur.aspx.cs" Inherits="fr.afcepf.ai107.nomads.Presentation.TableauDeBord" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="css/StyleInscriptionViaMasseur.css" rel="stylesheet" />
    <link href="css/TestStyleBtn.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="principal" runat="server">
    <asp:HiddenField ID="id_prestation" runat="server" />
    
    <asp:GridView ID="gvPrestation" runat="server"  CssClass="Grid" AutoGenerateColumns="false" emptydatatext="Il n'y a pour le moment aucune prestation planifiée!" > <%--BackColor="#999999" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" CellPadding="3" CellSpacing="2" Height="156px" Width="762px" AllowSorting="True" CaptionAlign="Top" GridLines="Horizontal" HorizontalAlign="Left"--%>

        <Columns>
       <asp:BoundField DataField="libelle" HeaderText="Nom de la prestation" />
        <asp:BoundField DataField="DATE_FORMAT(pr.date_prestation, GET_FORMAT(DATE, 'EUR'))" HeaderText="Date de la Prestation" />
             <asp:BoundField DataField="heure_debut" HeaderText="Heure de début" />
        <asp:BoundField DataField="heure_fin" HeaderText="Heure de fin" />
             <asp:BoundField DataField="cout_mads" HeaderText="Cout d'inscription <br/> (mads)" HtmlEncode="false" />
        <asp:BoundField DataField="montant_remuneration" HeaderText="Montant de la <br/> rémunération" HtmlEncode="false" />
        <asp:HyperLinkField 
                Text="Voir détails"
                DataNavigateUrlFormatString="Prestation.aspx?id={0}"
                DataNavigateUrlFields="id_prestation" />        
            <%--<asp:CheckBoxField AccessibleHeaderText="Choix" />--%>
    </Columns>
    <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
    <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
  <%--  <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
    <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
    <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
    <SortedAscendingCellStyle BackColor="#FFF1D4" />
    <SortedAscendingHeaderStyle BackColor="#B95C30" />
    <SortedDescendingCellStyle BackColor="#F1E5CE" />
    <SortedDescendingHeaderStyle BackColor="#93451F" />--%>
</asp:GridView>
        </center>
       
    
</div>


   

</asp:Content>
