﻿using fr.afcepf.ai107.nomads.Business;
using fr.afcepf.ai107.nomads.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace fr.afcepf.ai107.nomads.Presentation
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CatalogueBU bu = new CatalogueBU();
           
            if (!IsPostBack)
            {
            
                List<PartenaireEntity> part = new List<PartenaireEntity>();
                part = bu.GetListePartenairesForPresta();
                part.Insert(0, new PartenaireEntity(-1, "Tous les partenaires"));
                ddlPresta.DataTextField = "Nom";
                ddlPresta.DataValueField = "IdPartenaire";
                ddlPresta.DataSource = part;
                ddlPresta.DataBind();


                List<PartenaireEntity> liste = new List<PartenaireEntity>();
                liste = bu.GetListePartenairesForHistoPresta();
                liste.Insert(0, new PartenaireEntity(-1, "Tous les Partenaires"));
               ddlHisto.DataTextField = "Nom";
                ddlHisto.DataValueField = "IdPartenaire";
                ddlHisto.DataSource = liste;
                ddlHisto.DataBind();


                GridView1.DataSource = bu.GetListPrestationWithNomPartenaire();
                GridView1.DataBind();

                ddlHisto.Visible = false;
                lblhisto.Visible = false;
                btnRetour.Visible = false;
                btnokHisto.Visible = false;

           


            }
        }

        protected void btnHisto_Click(object sender, EventArgs e)
        {

            CatalogueBU bu = new CatalogueBU();
            GridView2.DataSource = bu.GetListHistoPrestationWithNomPartenaire();
            GridView2.DataBind();
            ddlHisto.Visible = true;

            GridView1.Visible = false;
            lblTitre.Visible = false;
            ddlPresta.Visible = false;
            btnHisto.Visible = false;
            btnOk.Visible = false;
            lblhisto.Visible = true;
            btnRetour.Visible = true;
            GridView2.Visible = true;
            btnokHisto.Visible = true;
           
           
      
        }

        protected void btnRetour_Click(object sender, EventArgs e)
        {
            CatalogueBU bu = new CatalogueBU();
            GridView1.Visible = true;
            lblTitre.Visible = true;
            ddlPresta.Visible = true;
            btnHisto.Visible = true;
            btnOk.Visible = true;
            lblhisto.Visible = false;
            GridView2.Visible = false;
            btnRetour.Visible = false;
           ddlHisto.Visible = false;
            btnokHisto.Visible = false;
            GridView2.DataSource = bu.GetListHistoPrestationWithNomPartenaire();
            GridView2.DataBind();
        
        }

        protected void ddlPresta_SelectedIndexChanged(object sender, EventArgs e)
        {

            
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            CatalogueBU bu = new CatalogueBU();
            int idPartenaire = int.Parse(ddlPresta.SelectedValue);
            GridView1.DataSource = bu.GetGrid(idPartenaire);
            GridView1.DataBind();
        }

        protected void ddlHisto_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btnokHisto_Click(object sender, EventArgs e)
        {
            CatalogueBU bu = new CatalogueBU();
            int idPartenaire = int.Parse(ddlHisto.SelectedValue);
            GridView2.DataSource = bu.GetGridHisto(idPartenaire);
            GridView2.DataBind(); 
        }

        protected void ddlDate_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btnokHisto_Click1(object sender, EventArgs e)
        {

        }
    }
}