﻿using fr.afcepf.ai107.nomads.Business;
using fr.afcepf.ai107.nomads.Entities;
using System;
using System.Collections.Generic;
using System.Web.Services.Description;
using System.Web.UI;

namespace fr.afcepf.ai107.nomads.Presentation
{
    public partial class RetourExperienceMasseur : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           if(!IsPostBack)
            {
            CatalogueBU bu = new CatalogueBU();

            lblIdMasseur.Text = Session["Masseur"].ToString();
            lblIdPrestation.Text = Session["Prestation"].ToString();

            List<TypeEvaluationMasseurEntity> liste = bu.GetListeTypeEvaluation();
            rbTypeEvaluationMasseur.DataTextField = "LibelleTypeEvaluationMasseur";
            rbTypeEvaluationMasseur.DataValueField = "IdTypeEvaluationMasseur";
            rbTypeEvaluationMasseur.DataSource = liste;
            rbTypeEvaluationMasseur.DataBind();

          
            }
            
        }

       /* protected void bllTypeEvaluationMasseur_Click(object sender, System.Web.UI.WebControls.BulletedListEventArgs e)
        {
          Response.Write("Vous avez sélectionné: " + bllTypeEvaluationMasseur.Items[e.Index].Text);
            lblSelectionEvaluationMasseur.Text = "Vous avez sélectionné: " + bllTypeEvaluationMasseur.Items[e.Index].ToString();
           //Ajouter condition si Excellent --> 1, Bien -->2, Moyen -->3, Médiocre -->4
        }*/

        protected void btnValiderMasseur_Click(object sender, EventArgs e)
        {
            CatalogueBU bu = new CatalogueBU();

            RetourExperienceMasseurEntity retourMasseur = new RetourExperienceMasseurEntity();
            List<TypeEvaluationMasseurEntity> liste = bu.GetListeTypeEvaluation();

            retourMasseur.Commentaire = txtCommentaireMasseur.Text;
                            
            double caf;
            double.TryParse(txtChiffreDaffaire.Text, out caf);
            retourMasseur.ChiffreDaffaire = caf;

            int nbreMasses;
            int.TryParse(txtNombreMasses.Text, out nbreMasses);
            retourMasseur.NombreMasses = nbreMasses;

            int idMasseur;
            int.TryParse(lblIdMasseur.Text, out idMasseur);
            retourMasseur.IdMasseur = idMasseur;

            int idPrestation;
            int.TryParse(lblIdPrestation.Text, out idPrestation);
            retourMasseur.IdPrestation = idPrestation;

            /*int idTypeEvaluationMasseur;
            int.TryParse(lblSelectionEvaluationMasseur.Text, out idTypeEvaluationMasseur);
            retourMasseur.IdTypeEvaluationMasseur = idTypeEvaluationMasseur;*/

            int idTypeEvalMasseur;
            Int32.TryParse(rbTypeEvaluationMasseur.SelectedValue, out idTypeEvalMasseur);

            //lblTest.Text = rbRaisonDesinscription.SelectedValue.ToString();

            bu.AjouterCommentaireMasseur(retourMasseur, idTypeEvalMasseur);

            System.Windows.Forms.MessageBox.Show("Commentaire masseur ajouté !");
            
            Response.Redirect("Accueil.aspx");

        }

        protected void btnAnnulation_Click(object sender, EventArgs e)
        {
            Response.Redirect("Accueil.aspx");
        }
    }
}