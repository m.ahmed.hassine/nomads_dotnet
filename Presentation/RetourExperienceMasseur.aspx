﻿<%@ Page Title="Page Retour Experience Masseur" Language="C#" MasterPageFile="~/Arche.Master" AutoEventWireup="true" CodeBehind="RetourExperienceMasseur.aspx.cs" Inherits="fr.afcepf.ai107.nomads.Presentation.RetourExperienceMasseur" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/TestStyleBtn.css" rel="stylesheet" />
    <style type="text/css">
<link href="css/TestStyleBtn.css" rel="stylesheet" />
        .auto-style5 {
            width: 227px;
        }
        .auto-style6 {
            width: 229px;
        }
        .auto-style7 {
            width: 646px;
        }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="principal" runat="server">
    <link href="css/StyleRetourExperience.css" rel="stylesheet" />
    <article class="articleMasseur"> Retour d'expérience du Masseur </article>
     <asp:Label ID="lblIdMasseur" runat="server" Text="lblIdMasseur" CssClass="lbl" ForeColor="White"></asp:Label>
    <asp:Label ID="lblIdPrestation" runat="server" Text="lblIdPrestation" CssClass="lbl" ForeColor="White"></asp:Label>
     <br />
    <table class="auto-style14">
        <tr>
            <td class="auto-style5">

<asp:Label ID="lblCommentaireMasseur" runat="server" Text="Commentaire du Masseur: " CssClass="lbl"></asp:Label>
            </td>
            <td class="auto-style6">

<asp:TextBox ID="txtCommentaireMasseur" runat="server" CssClass="txt"></asp:TextBox>

            </td>
            <td class="auto-style7">

               
                <asp:Label ID="lblSelectionEvaluationMasseur" runat="server" Text="Veuillez selectionner votre niveau de satisfaction: " CssClass="lbl"></asp:Label>

                <asp:RadioButtonList ID="rbTypeEvaluationMasseur" runat="server" CssClass="rbl">
                </asp:RadioButtonList>


                <br />
                <asp:Label ID="Label1" runat="server" Text="Vous avez selectionné: " CssClass="lbl"></asp:Label>


            </td>
        </tr>
        <tr>
            <td class="auto-style8">

<asp:Label ID="lblChiffreDaffaire" runat="server" Text="Chiffre d'affaire: " CssClass="lbl"></asp:Label>
            </td>
            <td class="auto-style9">

                <asp:TextBox ID="txtChiffreDaffaire" runat="server" CssClass="txt"></asp:TextBox>
            </td>
            <td class="auto-style7">

                </td>
        </tr>
        <tr>
            <td class="auto-style12">
<asp:Label ID="lblNombreMasses" runat="server" Text="Nombre de massés: " CssClass="lbl"></asp:Label>
            </td>
            <td class="auto-style11">
                <asp:TextBox ID="txtNombreMasses" runat="server" CssClass="txt"></asp:TextBox>
            </td>
            <td class="auto-style7">
                </td>
        </tr>
    </table>
    <br />
    <asp:Button ID="btnValiderMasseur" runat="server" Text="Valider Commentaire" OnClick="btnValiderMasseur_Click" CssClass="btnVal" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
   <asp:Button ID="btnAnnulation" runat="server" Text="Annuler Commentaire" OnClick="btnAnnulation_Click" CssClass="btnVal"/>
    <br />
    <br/>
  </asp:Content>
