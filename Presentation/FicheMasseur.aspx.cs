﻿using fr.afcepf.ai107.nomads.Business;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace fr.afcepf.ai107.nomads.Presentation
{
    public partial class FicheMasseur : System.Web.UI.Page
    {
        private int idMasseur;
        protected void Page_Load(object sender, EventArgs e)
        {
            int.TryParse(Request["id"], out idMasseur);  // Récupération de l'id dans l'entête de la requête

            GestionMasseurBU bu = new GestionMasseurBU();
            if (!IsPostBack)
            {
                
                #region INFORMATIONS DU MASSEUR

                DataTable dt = bu.GetInfoMasseurbyId(idMasseur);

                lblNom.Text = dt.Rows[0][1].ToString().ToUpper();
                lblPrenom.Text = dt.Rows[0][2].ToString();
                lblAdresse.Text = dt.Rows[0][6].ToString() + " " + dt.Rows[0][7].ToString() + " " + dt.Rows[0][8].ToString() + " " + dt.Rows[0][9].ToString() + " " + dt.Rows[0][10].ToString();
                lblCategorie.Text = dt.Rows[0][15].ToString();
                lblCompetence.Text = dt.Rows[0][14].ToString();


                int soldeMads = calculSoldeMads(idMasseur);

                lblMads.Text = "Mes Mads : " + soldeMads;

                #endregion

                CatalogueBU catBu = new CatalogueBU();

                DataTable dtCatBu = catBu.GetListePrestationAVenirByMasseur(idMasseur, DateTime.Now);
                gvPresta.DataSource = dtCatBu;
                gvPresta.DataBind();

                btnRetourPrestaEnCours.Visible = false;
                lblTitreHisto.Visible = false;

            }


        }

        protected void btnInscrption_Click(object sender, EventArgs e)
        {
            Session["Masseur"] = idMasseur;
            Session["Solde"] = calculSoldeMads(idMasseur);

            Response.Redirect("PageInscriptionViaMasseur.aspx"); //gestionPrestation.aspx

        }

        protected void histoPresta_Click(object sender, EventArgs e)
        {
            CatalogueBU catBu = new CatalogueBU();

            DataTable dtCatBu = catBu.GetListePrestationAnterieureByMasseur(idMasseur, DateTime.Now);
            
            gtest.DataSource = dtCatBu;
            gtest.DataBind();
            gvPresta.Visible = false;
            btnInscrption.Visible = false;
            btnHistoPresta.Visible = false;
            btnRetourPrestaEnCours.Visible = true;
            lblTitreEnCours.Visible = false;
            lblTitreHisto.Visible = true;
        }

        protected void gtest_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            String idPresta = (String)e.CommandArgument;
            Session["Masseur"] = idMasseur;
            Session["Prestation"] = idPresta;
            Response.Redirect("RetourExperienceMasseur.aspx");

        }


        protected void btnRetourPrestaEnCours_Click(object sender, EventArgs e)
        {
            Response.Redirect("FicheMasseur.aspx?id=" + idMasseur);
        }

        protected int calculSoldeMads (int idMasseur)
        {
            GestionMasseurBU bu = new GestionMasseurBU();
            int madsAchetes = bu.GetSommeAchatMads(idMasseur);

            int madsDepenses = bu.GetSommeDepenseMads(idMasseur);

            int calculSoldeMads = madsAchetes - madsDepenses;

            return calculSoldeMads;
        }

        protected void gvPresta_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            String idPresta = (String)e.CommandArgument;
            //Session["Masseur"] = idMasseur;
            //Session["Prestation"] = idPresta;
            Response.Redirect("AnnulationMasseur.aspx?id=" + idPresta + "&idm=" + idMasseur); // Double Query à vérifier
        }
    }
}