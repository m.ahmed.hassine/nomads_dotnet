﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Arche.Master" maintainScrollPositionOnPostBack = "true" AutoEventWireup="true" CodeBehind="CreationPrestation.aspx.cs" Inherits="fr.afcepf.ai107.nomads.Presentation.CreationPrestation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="https://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="css/jquery.datetimepicker.css"/>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script type="text/javascript" src="js/bootstrap-multiselect.js"></script> 
    <script type="text/javascript" src="js/jquery.datetimepicker.full.js"></script>
    <link href="css/StyleCreationPrestation.css" rel="stylesheet" />
    <link href="css/StyleAjoutMasseur.css" rel="stylesheet" />
    <link href="css/TestStyleBtn.css" rel="stylesheet" />
    <script type="text/javascript" src="js/presentation.js"></script>
    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="principal" runat="server">
    <asp:Label ID="Label1" runat="server" Text="Gestion prestations > Ajouter une prestation " Font-Bold="True" Font-Italic="True" Font-Size="Large" CssClass="lbl"></asp:Label>
    <br />
    <section class="container-fluid" style="background-color: #E2E2E2; font-size: large;">
    <div class="form-group">
        <div class="form-row">
             <div class="col-md-2">
                <Label for="DDLTypePartenaire">Type de partenaire</Label>
                <asp:DropDownList ID="DDLTypePartenaire" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="DDLTypePartenaire_SelectedIndexChanged" runat="server"></asp:DropDownList>
             </div>
             <div class="col-md-2">       
                <Label for="ListePatrenaires">Liste des partenaires</Label>
                <asp:DropDownList ID="ListePartenaires" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ListePartenaires_SelectedIndexChanged" runat="server"></asp:DropDownList>
             </div>    
             <div class="col-md-2 mt-2"> 
                 <label for="BtnAjoutPartenaire"></label>
                <asp:Button ID="BtnAjoutPartenaire" CssClass="btn btn-success" OnClick="BtnAjoutPartenaire_Click" OnClientClick="SetTarget();" runat="server" Text="Ajouter un nouveau partenaire" />
             </div>
        </div>
        <div class="form-row  mt-2">
            <div class="col-md-2">
                <Label for="TxtLibelle">Nom de la prestation</Label>
                <asp:TextBox ID="TxtLibelle" CssClass="form-control" runat="server"></asp:TextBox>
            </div>
            <div class="col-md-2">
                <Label for="txtDatePrestation">Date de la prestation</Label>
                <asp:TextBox ID="txtDatePrestation" AutoCompleteType="Disabled" CssClass="form-control" OnTextChanged="datePrestation_OnChange" runat="server"></asp:TextBox>
            </div>
            <div class="col-md-2">
                <Label for="TxtHeureDebut">Heure de début</Label>
                        <asp:TextBox ID="TxtHeureDebut" CssClass="form-control" TextMode="Time" runat="server"></asp:TextBox>
            </div>
            <div class="col-md-2">
                 <Label for="TxtHeureFin">Heure de fin</Label>                   
                 <asp:TextBox ID="TxtHeureFin" CssClass="form-control" TextMode="Time" runat="server"></asp:TextBox>
            </div>
        </div>
            <div class="form-row mt-3">
                <div class="col-md-1">
                    <Label for="TxtNumeroVoie">Num</Label>
                    <asp:TextBox ID="TxtNumeroVoie" CssClass="form-control" runat="server"></asp:TextBox>
                </div>
                <div class="col-md-1">
                    <Label for="TxtComplementNum">Comp. Num</Label>
                    <asp:TextBox ID="TxtComplementNum" CssClass="form-control" runat="server"></asp:TextBox>
                </div>
                <div class="col-md-4">
                    <Label for="TxtNomVoie">Voie</Label>
                    <asp:TextBox ID="TxtNomVoie" CssClass="form-control" runat="server"></asp:TextBox>
                </div>
                <div class="col-md-2">
                    <Label for="DDLLibelleVille">Ville</Label>
                    <asp:DropDownList ID="DDLLibelleVille" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="DDLLibelleVille_SelectedIndexChanged" runat="server"></asp:DropDownList>
                </div>
                <div class="col-md-2">
                    <Label for="DDLLibelleCP">Code postale</Label>
                    <asp:DropDownList ID="DDLLibelleCP" maintainScrollPositionOnPostBack = "true" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="DDLLibelleCP_SelectedIndexChanged" runat="server"></asp:DropDownList>
                </div>   
            </div>
            <div class="form-row mt-3">
                <div class="col-md-2">
                    <Label for="DDLLibelleTypePaiement">Type de paiement</Label>
                    <asp:DropDownList ID="DDLLibeleTypePaiement" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="DDLLibeleTypePaiement_SelectedIndexChanged" runat="server"></asp:DropDownList>
                </div>
                <div class="col-md-2">
                    <Label for="TxtCoutMads">Coût en Mads</Label>
                    <asp:TextBox ID="TxtCoutMads" CssClass="form-control" Enabled="false" runat="server"></asp:TextBox>
                </div>
                <div class="col-md-2 mb-3">
                    <Label for="TxtMontantRemuneration">Montant de rémunération</Label>
                    <asp:TextBox ID="TxtMontantRemuneration" CssClass="form-control" AutoPostBack="true" Enabled="false" runat="server"></asp:TextBox>
                </div>
                <div class="col-md-2">
                    <Label for="TxtMontantFacturation">Montant de Facturation</Label>
                    <asp:TextBox ID="TxtMontantFacturation" CssClass="form-control" Enabled="false" runat="server"></asp:TextBox>
                </div>
            </div>
            
        
        <div class="form-row">
            
        </div>

        <div class="form-row">
            <div class="col-md-2">
                <Label for="TxtNbrChaisesErgonomique">Nbre des chaises ergo.</Label>
                <asp:TextBox ID="TxtNbrChaisesErgonomique" CssClass="form-control" runat="server"></asp:TextBox>
            </div>
            <div class="col-md-2">
                <Label for="TxtNbrMasseurs">Nombre des masseurs</Label>
                <asp:TextBox ID="TxtNbrMasseurs" CssClass="form-control" runat="server"></asp:TextBox>
            </div>
            <div class="col-md-3">
                <Label for="DDLListeMasseurs">Liste des masseurs à préselectionner</Label>
                <asp:ListBox ID="DDLListeMasseurs" CssClass="form-control" SelectionMode="Multiple" runat="server"></asp:ListBox>
            </div> 
        </div>
        <div class="form-row">
           <div class="col-md-2">
                        <Label for="DDLLibelleRecurrence">Récurrence</Label>
                        <asp:DropDownList ID="DDLLibelleRecurrence" CssClass="form-control" OnSelectedIndexChanged="DDLLibelleRecurrence_SelectedIndexChanged" AutoPostBack="true" runat="server"></asp:DropDownList>
            </div>
            <div class="col-md-2">   
                        <Label for="TxtPas">Pas</Label>
                        <asp:TextBox ID="TxtPas" CssClass="form-control" Enabled="false" runat="server"></asp:TextBox>
             </div>
            <div class="col-md-2">
                        <Label for="TxtDureeRecurence">Durée de Récurrence</Label>
                        <asp:TextBox ID="TxtDureeRecurrence" CssClass="form-control" Enabled="false" runat="server"></asp:TextBox>
            </div>
            <div class="col-md-2 mt-2">
                        <label for="DDLDureeRecurrence"> </label>
                        <asp:DropDownList ID="DDLDureeRecurrence" CssClass="form-control" Enabled="false" runat="server"></asp:DropDownList>
            </div>
            <div class="col-md-2">
                        <Label for="TxtDelta">Delta</Label>
                        <asp:TextBox ID="TxtDelta" CssClass="form-control"  Enabled="false" runat="server"></asp:TextBox>
            </div>
       </div>       
    <div class="form-row">
           <div class="col-md-10">
                <Label for="TxtSpecificites">Spécificités</Label>
                <asp:TextBox ID="TxtSpecificites" CssClass="form-control" Height="100" TextMode="MultiLine" runat="server"></asp:TextBox>
            </div>
    </div> 
    <div class="form-row mt-2 mb-2 ">
        <div class="col-md-2">
            <asp:Button ID="BtnEnregistrer" CssClass="btn btn-warning" runat="server" OnClick="BtnEnregistrer_Click" Text="Enregistrer" />   
        </div>
        <div class="col-md-2 mb-2 offset-md-6 text-right">
            <asp:Button ID="BtnValider" CssClass="btn btn-success" runat="server" OnClick="BtnValider_Click" Text="Valider" />  
        </div>
    </div>
</section>
    <script type = "text/javascript">
         function SetTarget() {
            document.forms[0].target = "_blank";
         }
    </script>
</div>
</asp:Content>
