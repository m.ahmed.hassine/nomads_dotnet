﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Arche.Master" AutoEventWireup="true" CodeBehind="VoirPrestation.aspx.cs" Inherits="fr.afcepf.ai107.nomads.Presentation.WebForm2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     
     <link href="css/StyleVoirPresta.css" rel="stylesheet" />
    <link href="css/TestStyleBtn.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="principal" runat="server">
    <asp:Label ID="Label1" runat="server" Text="Gestion prestations > Voir liste prestation (en cours/ historique)" Font-Bold="True" Font-Italic="True" Font-Size="Medium" ForeColor="#333333"></asp:Label>
    
     
    <center>
     <section id="sec">
         <tr>
         <div id="head"> 
   <asp:Label ID="lblTitre" runat="server" Text="Prestations en cours" CssClass="lbl"></asp:Label> 
        </div>
             </tr>
            <br />
         <tr> 
            
         <div id="rech">
   <td>  <asp:DropDownList ID="ddlPresta" CssClass="ddl" runat="server"></asp:DropDownList> </td>
<td><asp:Button ID="btnOk" CssClass="btnVal" runat="server" Text="Valider" OnClick="btnOk_Click"></asp:Button></td>
          </div>
             </tr>
             <br />
         <tr>
         <div id="gr">
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns ="false" CssClass="grid" >
        <Columns>
        <asp:BoundField DataField="nom_partenaire" HeaderText="Partenaires" />
        <asp:BoundField DataField="libelle" HeaderText="Prestations à venir" />
        <asp:BoundField DataField="DATE_FORMAT(a.date_prestation,GET_FORMAT(date,'EUR'))" HeaderText="Dates" />
        </Columns>
    </asp:GridView>
        </div>
       </tr>
          <br />
             <br />
         <tr>
         <div id="btn">
    <asp:Button ID="btnHisto" CssClass="bouton" runat="server" Text="Historique des prestations" OnClick="btnHisto_Click" />
             </div>
     </section>      
      
    <!-- PARTIE HISTORIQUE  --> 
   <section id="sel">
    <div id="head">
    <asp:Label ID="lblhisto" runat="server" Text="Prestations passées" CssClass="lbl"></asp:Label>
    </div>
    <br />
    <br />
    <div id="rech">
<asp:DropDownList ID="ddlHisto" runat="server" OnSelectedIndexChanged="ddlHisto_SelectedIndexChanged" CssClass="ddl"></asp:DropDownList>
<asp:Button ID="btnokHisto" CssClass="btnVal" runat="server" Text="Valider" OnClick="btnokHisto_Click"></asp:Button>
        </div>
         <br />
             <br />
         <div id="gr">
    <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns ="false" CssClass="grid" >
         <Columns>
        <asp:BoundField DataField="nom_partenaire" HeaderText="Partenaires" />
        <asp:BoundField DataField="libelle" HeaderText="Prestations terminées" />
        <asp:BoundField DataField="DATE_FORMAT(a.date_prestation,GET_FORMAT(date,'EUR'))" HeaderText="Dates" />
        </Columns>
    </asp:GridView>
        </div>
         <br />
             <br />
          <div id="btn">
    <asp:Button ID="btnRetour" CssClass="bouton" runat="server" OnClick="btnRetour_Click" Text="Revenir aux prestations en cours" />
    </div>
      </section>
    </center>

    

</asp:Content>
