﻿using fr.afcepf.ai107.nomads.Business;
using fr.afcepf.ai107.nomads.Entities;
using System;
using System.Collections.Generic;
using System.Web.Services.Description;
using System.Web.UI.WebControls;

namespace fr.afcepf.ai107.nomads.Presentation
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CatalogueBU bu = new CatalogueBU();

            if (!IsPostBack)
            {
                // drop down list libelle
                List<PrestationEntity> prestations = bu.GetListePrestation();
                prestations.Insert(0, new PrestationEntity(-1, ""));
                DdlLibelle.DataTextField = "Libelle";
                DdlLibelle.DataValueField = "Id";
                DdlLibelle.DataSource = prestations;
                DdlLibelle.DataBind();

                // GridView
                gridListe.DataSource = bu.GetListForGrid();
                gridListe.DataBind();
            }
        }
        protected void btnOk_Click(object sender, EventArgs e)
        {
            CatalogueBU bu = new CatalogueBU();
            // int idPartenaire = int.Parse(DdlTypePartenaire.SelectedValue);
            string nomPartenaire = Request.Form["txtPartenaire"];
            int idPrestation = int.Parse(DdlLibelle.SelectedValue);
            // Response.Write("ok" + idPrestation);
            // int idDate = int.Parse(DdlDate.SelectedValue);
            gridListe.DataSource = bu.Search(nomPartenaire, idPrestation);
            gridListe.DataBind();
        }
        protected void gridListe_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "annuler")
            {
               // Response.Write("annuler" + e.CommandArgument);
                CatalogueBU bu = new CatalogueBU();
                int idPrestation = Convert.ToInt32(e.CommandArgument);
                bu.UpdateDateAnnulationPrestation(idPrestation);
                System.Windows.Forms.MessageBox.Show("Prestation annulée avec succès!");
                gridListe.DataSource = bu.GetListForGrid();
                gridListe.DataBind();
            }
        }
    }
}