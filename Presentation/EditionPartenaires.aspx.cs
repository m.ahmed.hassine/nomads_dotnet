﻿using fr.afcepf.ai107.nomads.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace fr.afcepf.ai107.nomads.Presentation
{
    public partial class EditionPartenaires : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GestionPartenaireBU bu = new GestionPartenaireBU();

                rptPartenaireInfoMin.DataSource = bu.GetInfoPartenaires();
                rptPartenaireInfoMin.DataBind();
            }
        }
    }
}