﻿using fr.afcepf.ai107.nomads.Business;
using fr.afcepf.ai107.nomads.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace fr.afcepf.ai107.nomads.Presentation
{
    public partial class EditionMasseur : System.Web.UI.Page
    {
        private int idMasseur;
        private int ancienNiveau;
        private int ancienneCategorie;
        private CatalogueBU bu = new CatalogueBU();
        protected void Page_Load(object sender, EventArgs e)
        {
            int.TryParse(Request["id"], out idMasseur);


            if (!IsPostBack)
            {
                MasseurEntity masseur = bu.GetMasseurEntityById(idMasseur);
                List<SexeEntity> sexe = bu.GetListeSexe();
                sexe.Insert(0, new SexeEntity());
                DDLSexe.DataTextField = "LibelleSexe";
                DDLSexe.DataValueField = "IdSexe";
                DDLSexe.SelectedValue = masseur.IdSexe.ToString();
                DDLSexe.DataSource = sexe;
                DDLSexe.DataBind();

                List<CategorieEntity> categorie = bu.GetAllCategorie();
                DDLCategorie.DataTextField = "LibelleCategorie";
                DDLCategorie.DataValueField = "IdCategorie";
                DDLCategorie.SelectedValue = bu.GetIdCategorieByIdMasseur(idMasseur).ToString();
                ancienneCategorie = bu.GetIdCategorieByIdMasseur(idMasseur);
                DDLCategorie.DataSource = categorie;
                DDLCategorie.DataBind();

                List<NiveauCompetenceEntity> niveauCompetence = bu.GetListeNiveauCompetence();
                DDLNiveauCompetence.DataTextField = "LibelleNiveauCompetence";
                DDLNiveauCompetence.DataValueField = "IdStatut";
                DDLNiveauCompetence.SelectedValue = bu.GetIdNiveauCompetencesByIdMasseur(idMasseur).ToString();
                ancienNiveau = bu.GetIdNiveauCompetencesByIdMasseur(idMasseur);
                DDLNiveauCompetence.DataSource = niveauCompetence;
                DDLNiveauCompetence.DataBind();

                List<VilleEntity> ville = bu.GetListeVille();
                DDLLibelleVille.DataTextField = "LibelleVille";
                DDLLibelleVille.DataValueField = "IdVille";
                DDLLibelleVille.SelectedValue = masseur.IdVille.ToString();
                DDLLibelleVille.DataSource = ville;
                DDLLibelleVille.DataBind();
                DDLLibelleCP.DataTextField = "LibelleCP";
                DDLLibelleCP.DataValueField = "IdVille";
                DDLLibelleCP.SelectedValue = masseur.IdVille.ToString();
                DDLLibelleCP.DataSource = ville;
                DDLLibelleCP.DataBind();
                first_name.Text = masseur.Prenom;
                last_name.Text = masseur.Nom;
                number_adress.Text = masseur.NumeroVoie;
                complement_number.Text = masseur.ComplementNumeroVoie;
                street.Text = masseur.NomVoie;
                mail_adress.Text = masseur.AdresseMail;
                phone.Text = masseur.NumeroTelephone;
                birth_date.Text = masseur.DateNaissance.ToShortDateString();
            }
        }
        protected void DDLLibelleVille_SelectedIndexChanged(object sender, EventArgs e)
        {
            DDLLibelleCP.SelectedValue = DDLLibelleVille.SelectedValue;
        }
        protected void DDLLibelleCP_SelectedIndexChanged(object sender, EventArgs e)
        {
            DDLLibelleVille.SelectedValue = DDLLibelleCP.SelectedValue;
        }
        protected void BtnMAJ_Click(object sender, EventArgs e)
        {
            AptitudeEntity aptitude = new AptitudeEntity();
            EvolutionProfessionnelleEntity evolutionProfessionnelle = new EvolutionProfessionnelleEntity();
            MasseurEntity masseur = new MasseurEntity();
            masseur.Nom = last_name.Text;
            masseur.Prenom = first_name.Text;
            masseur.NumeroVoie = number_adress.Text;
            masseur.ComplementNumeroVoie = complement_number.Text;
            masseur.NomVoie = street.Text;
            masseur.DateNaissance = DateTime.Parse(birth_date.Text);
            masseur.IdVille = int.Parse(DDLLibelleVille.SelectedValue);
            masseur.NumeroTelephone = phone.Text;
            masseur.AdresseMail = mail_adress.Text;
            masseur.IdSexe = int.Parse(DDLSexe.SelectedValue);
            if (ancienneCategorie != int.Parse(DDLCategorie.SelectedValue))
            {
            aptitude.IdMasseur = idMasseur;
            aptitude.IdCategorie = int.Parse(DDLCategorie.SelectedValue);
            aptitude.DateDebut = DateTime.Now;
            bu.MAJAptitude(aptitude);
            masseur.IdAptitude = bu.GetLastIdAptitude();
            }
            else
            {
                masseur.IdAptitude = bu.GetMasseurEntityById(idMasseur).IdAptitude;
            }
            if (ancienNiveau != int.Parse(DDLNiveauCompetence.SelectedValue))
            {
            evolutionProfessionnelle.IdNiveauCompetences = int.Parse(DDLNiveauCompetence.SelectedValue);
            evolutionProfessionnelle.DateEvolution = DateTime.Now;
            evolutionProfessionnelle.IdMasseur = idMasseur;
            bu.AddEvolutionProfessionnelle(evolutionProfessionnelle);
            masseur.IdEvolutionPro = bu.GetLastIdEvolutionProfessionnelle();
            }
            else
            {
                masseur.IdEvolutionPro = bu.GetMasseurEntityById(idMasseur).IdEvolutionPro;
            }
            masseur.IdMasseur = idMasseur;
            bu.MAJMasseur(masseur);
            System.Windows.Forms.MessageBox.Show("Masseur mis à jour avec succès !");
            Response.Redirect("EditionMasseurs.aspx");

        }

        protected void BtnDesactiver_Click(object sender, EventArgs e)
        {
            CatalogueBU bu = new CatalogueBU();
            List<MotifFinContratMasseurEntity> motifs = bu.GetListeLibelleMotifFinContratMasseur();
            DDLMotif.DataTextField = "LibelleMotifFinContratMasseur";
            DDLMotif.DataValueField = "IdMotifFinContratMasseur";
            DDLMotif.DataSource = motifs;
            DDLMotif.DataBind();
            DDLMotif.Visible = true;
            BtnValider.Visible = true;

        }

        protected void BtnValider_Click(object sender, EventArgs e)
        {
            AptitudeEntity aptitude = new AptitudeEntity();
            aptitude.IdMasseur = idMasseur;
            aptitude.IdCategorie = int.Parse(DDLCategorie.SelectedValue);
            bu.SetFinAptitude(aptitude);
            ContratMasseurEntity contratMasseur = new ContratMasseurEntity();
            contratMasseur.DateFinContratMasseur = DateTime.Now;
            contratMasseur.IdMasseur = idMasseur;
            contratMasseur.IdMotifFinContrat = int.Parse(DDLMotif.SelectedValue);
            bu.SetEndContratMasseur(contratMasseur);
            System.Windows.Forms.MessageBox.Show("Masseur désactivé avec succès !");
            Response.Redirect("EditionMasseurs.aspx");
        }
    }
}