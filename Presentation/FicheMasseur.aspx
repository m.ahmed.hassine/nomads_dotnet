﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Arche.Master" AutoEventWireup="true" CodeBehind="FicheMasseur.aspx.cs" Inherits="fr.afcepf.ai107.nomads.Presentation.FicheMasseur" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/StyleFicheMasseur.css" rel="stylesheet" />
    <link href="css/TestStyleBtn.css" rel="stylesheet" />
      <style type="text/css">
            .auto-style6 {
                width: 100px;
                height: 88px;
            }
            .auto-style7 {
                width: 499px;
            }
            .auto-style8 {
                width: 330px;
            }
            .auto-style9 {
                width: 332px;
            }
          .auto-style10 {
              color: darkgreen;
              border-radius: 10px;
              font-size: large;
              border-style: solid;
              border-color: inherit;
              border-width: medium;
              padding: 2px 12px;
              background-color: slategrey;
          }
        </style>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="principal" runat="server">
    <form id="contenu">
    <asp:Panel runat="server">
        <div class="fond"><table>
               <tr><td style="font-family: 'Arial Black'; color: #006600">
             <img class="auto-style6" src="images/avatar_masseur.png" alt="photo avatar masseur" />
                   <br />
                   Fiche Masseur</td>
                   <td></td>
                    <td class="auto-style7">
                        <ul>
                        <li><asp:Label ID="Label1" runat="server" Text="Nom: " CssClass="lbl2"></asp:Label><asp:Label ID="lblNom" runat="server" Text="TTTTTxxxxxTTTTT" CssClass="lbl1"></asp:Label></li>
                        <li><asp:Label ID="Label2" runat="server" Text="Prénom: " CssClass="lbl2"></asp:Label>  <asp:Label ID="lblPrenom" runat="server" Text="prenom" CssClass="lbl1"></asp:Label></li>
                        <li><asp:Label ID="Label3" runat="server" Text="Adresse: " CssClass="lbl2"></asp:Label> <asp:Label ID="lblAdresse" runat="server" Text="Adresse à récup" CssClass="lbl1"></asp:Label></li>             
                        </ul>
                        </td>
                  <td class="auto-style8">
                      <ul>
                     
                    </ul>
                      </td>
                  <td class="auto-style9">
                      <ul>
                          <li><asp:Label ID="Label4" runat="server" Text="Catégorie: " CssClass="lbl2"></asp:Label><asp:Label ID="lblCategorie" runat="server" Text="Label" CssClass="lbl1"></asp:Label></li>
                     <li><asp:Label ID="Label5" runat="server" Text="Compétence: " CssClass="lbl2"></asp:Label><asp:Label ID="lblCompetence" runat="server" Text="Label" CssClass="lbl1"></asp:Label></li>
                    <li><asp:Label ID="Label6" runat="server" Text="Revenue -" CssClass="lbl2"></asp:Label><asp:Label ID="lblMads" runat="server" Text="Label" CssClass="lbl1"></asp:Label></li>
                     </ul>
                      </td>
                   <td class="auto-style9">
                                       <asp:Button ID="btnPropoPresta" runat="server" Text="Mes propositions de prestation!" CssClass="bouton" Width="300px" />
                        <br />
                       <br />
                 <asp:Button ID="btnHistoPresta" runat="server" Text="Voir mes prestations passées" OnClick="histoPresta_Click" CssClass="bouton" Width="300px"/>
                       <br />
                       <br />
                <asp:Button ID="btnRetourPrestaEnCours" runat="server" Text="Retour à mes prestations en cours" OnClick="btnRetourPrestaEnCours_Click" CssClass="bouton" Width="360px"/>
                       <br />
                       <br />
                      </td>
                       </tr>
                     </table>  
                    </div>
                </asp:Panel>

       <div id="PrestaEnCours" style="font-family: Arial; font-size: small">
        <h2 >
            <asp:Label ID="lblTitreEnCours" runat="server" Text="Mes prestations en cours" CssClass="lbl"></asp:Label>
        </h2>
           <center>
        <asp:GridView AutoGenerateColumns="false" ID="gvPresta" runat="server" class="greedPresta" HeaderStyle-BackColor="#999999" emptydatatext="Vous n'avez actuellement aucune prestation prévue!" OnRowCommand="gvPresta_RowCommand" CssClass="grid">
        <columns> 
        <asp:BoundField DataField="libelle" HeaderText="Nom de la prestation" />
        <asp:BoundField DataField="date_prestation" HeaderText="Date de la prestation" />
        <asp:BoundField DataField="nom" HeaderText="Partenaire" />
            <asp:TemplateField>
            <ItemTemplate>
                <asp:Button ID="btnDesinscription" CssClass="btntbl" runat="server" CommandName="Desinscri" CommandArgument='<%#Bind ("id_prestation") %>' Text="Me désinscrire de la prestation" />
            </ItemTemplate>
        </asp:TemplateField>
  
            
            

        </columns>
</asp:GridView>
      </center>        
        <asp:Button ID="btnInscrption" runat="server" Text="M'inscrire à une nouvelle prestation" OnClick="btnInscrption_Click" CssClass="bouton"/>

        <h2 
            <asp:Label ID="lblTitreHisto" runat="server" Text="Mes prestations archivées" CssClass="lbl"></asp:Label>
        </h2>   
           <center>

  <asp:GridView AutoGenerateColumns="false" ID="gtest" runat="server" OnRowCommand="gtest_RowCommand" HeaderStyle-BackColor="#999999" emptydatatext="Pas de prestation passée pour le moment" CssClass="grid">
             <columns> 
        <asp:BoundField DataField="libelle" HeaderText="Nom de la prestation" />
        <asp:BoundField DataField="date_prestation" HeaderText="Date de la prestation" />
        <asp:BoundField DataField="nom" HeaderText="Partenaire" />
        <asp:TemplateField>
            <ItemTemplate>
                <asp:Button ID="btnRetourExp" CssClass="btntbl" runat="server" CommandName="Rex" CommandArgument='<%#Bind ("id_prestation") %>' Text="Faire mon retour d'expérience" />
            </ItemTemplate>
        </asp:TemplateField>
                 
                
        </columns>

        </asp:GridView>
           </center>
      
        
    </div>


</form>
</asp:Content>
