﻿using fr.afcepf.ai107.nomads.Business;
using fr.afcepf.ai107.nomads.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace fr.afcepf.ai107.nomads.Presentation
{
    public partial class AnnulationMasseur : System.Web.UI.Page
    {
        private int idPrestation;
        private int idMasseur;

        protected void Page_Load(object sender, EventArgs e)
        {
            int.TryParse(Request["id"], out idPrestation);
            int.TryParse(Request["idm"], out idMasseur);

            if (!IsPostBack)
            {

            CatalogueBU bu = new CatalogueBU();
            PrestationEntity presta = bu.GetPrestationByIdPresta(idPrestation);
            

            lblNomPresta.Text = presta.Libelle;
            lblDate.Text = presta.DatePrestation.ToShortDateString();

            List < MotifDesinscriptionEntity > liste = bu.GetListeMotifDesinscription();
                rbRaisonDesinscription.DataTextField = "LibelleMotif";
                rbRaisonDesinscription.DataValueField = "IdMotifDesinscription";
                rbRaisonDesinscription.DataSource = liste;
                rbRaisonDesinscription.DataBind();


               
            }
        }

        protected void btnValid_Click(object sender, EventArgs e)
        {
            int idMotif;
                Int32.TryParse(rbRaisonDesinscription.SelectedValue, out idMotif);
 //lblTest.Text = rbRaisonDesinscription.SelectedValue.ToString(); // Recupération de l'ID OK

            GestionMasseurBU bu = new GestionMasseurBU();
            bu.DesinscrireMasseur(idPrestation, idMasseur, idMotif);

            System.Windows.Forms.MessageBox.Show("Annulation effectuée !");

            Response.Redirect("FicheMasseur.aspx?id="+idMasseur);
        }
    }
}