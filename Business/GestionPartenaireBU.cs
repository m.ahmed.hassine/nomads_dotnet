﻿using fr.afcepf.ai107.nomads.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fr.afcepf.ai107.nomads.Business
{
    public class GestionPartenaireBU
    {

        public DataTable GetInfoPartenaires()
        {
            PartenaireDAO dao = new PartenaireDAO();
            return dao.GetAll();
        }

        public DataTable GetInfoPartenairebyId(int idPartenaire)
        {
            PartenaireDAO dao = new PartenaireDAO();
            return dao.GetById(idPartenaire);
        }

        public DataTable GetListePartenaireByPrestation(int idPrestation)
        {
            PartenaireDAO dao = new PartenaireDAO();
            return dao.GetPartListByPresta(idPrestation);
        }

        public DataTable GetListePrestationByPartenaire(int idPartenaire)
        {
            PrestationDAO dao = new PrestationDAO();
            return dao.GetlistPrestaByIdPart(idPartenaire);
        }

        /*   public void AjouterPartenairePresta(int idPart, int idPrestation)
           {
               PartenaireDAO dao = new PartenaireDAO();
               dao.AddPartToPresta(idPart, idPrestation);
           }*/


        //public DataTable GetListeMasseurs()
        //{
        //    MasseurDAO dao = new MasseurDAO();
        //    return dao.GetMasseurList();
        //}





        //public List<MasseurEntity> GetListeMasseurs()
        //{
        //    MasseurDAO dao = new MasseurDAO();
        //    return dao.GetAll();
        //}
    }
}
