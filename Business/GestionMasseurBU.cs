﻿using fr.afcepf.ai107.nomads.DataAccess;
using fr.afcepf.ai107.nomads.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fr.afcepf.ai107.nomads.Business
{
    public class GestionMasseurBU
    {

        public DataTable GetInfoMasseurs()
        {
            MasseurDAO dao = new MasseurDAO();
            return dao.GetAll();
        }

        public DataTable GetInfoMasseurbyId(int idMasseur)
        {
            MasseurDAO dao = new MasseurDAO();
            return dao.GetById(idMasseur);
        }

        public DataTable GetListeMasseurByPrestation(int idPrestation)
        {
            MasseurDAO dao = new MasseurDAO();
            return dao.GetMassListByPresta(idPrestation);
        }

        public void AjouterMasseurPresta(int idMass, int idPrestation)
        {
            MasseurDAO dao = new MasseurDAO();
            dao.AddMassToPresta(idMass, idPrestation);
        }

        public int GetSommeAchatMads(int idMasseur)
        {
            MasseurDAO dao = new MasseurDAO();
            return dao.GetSumMadsBought(idMasseur);
        }

        public int GetSommeDepenseMads(int idMasseur)
        {
            MasseurDAO dao = new MasseurDAO();
            return dao.GetSumMadsSpent(idMasseur);
        }

        public void DesinscrireMasseur(int idPrestation, int idMasseur, int idMotif)
        {
            InscriptionMasseurDAO desinscri = new InscriptionMasseurDAO();

            desinscri.UnsubscribeMasseur(idPrestation, idMasseur, idMotif);
        }


        //public DataTable GetListeMasseurs()
        //{
        //    MasseurDAO dao = new MasseurDAO();
        //    return dao.GetMasseurList();
        //}





        //public List<MasseurEntity> GetListeMasseurs()
        //{
        //    MasseurDAO dao = new MasseurDAO();
        //    return dao.GetAll();
        //}
    }
}
