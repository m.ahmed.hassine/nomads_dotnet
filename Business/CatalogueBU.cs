﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using fr.afcepf.ai107.nomads.DataAccess;
using fr.afcepf.ai107.nomads.Entities;

namespace fr.afcepf.ai107.nomads.Business
{
    public class CatalogueBU
    {
        #region Masseurs
        public List<MasseurEntity> GetListeMasseursByIdCategorie(int idCategorie)
        {
            return new MasseurDAO().GetAllInListByIdCategorie(idCategorie);
        }
        public List<MotifFinContratMasseurEntity> GetListeLibelleMotifFinContratMasseur()
        {
            return new MotifFinContratMasseurDAO().GetListeLibelleMotifFinContratMasseur();
        }
        public void SetEndContratMasseur(ContratMasseurEntity contratMasseur)
        {
            new ContratMasseurDAO().SetEndContratMasseur(contratMasseur);
        }
        public int GetLastIdContratMasseur()
        {
           return new ContratMasseurDAO().GetLastIdContratMasseur();
        }
        public void AddContratMasseur(ContratMasseurEntity contratMasseur)
        {
            new ContratMasseurDAO().AddContratMasseur(contratMasseur);
        }
        public ContratMasseurEntity GetContratMasseurById(int idContratMasseur)
        {
            return new ContratMasseurDAO().GetContratMasseurById(idContratMasseur);
        }
        public List<MasseurEntity> GetListeMasseurs()
        {
            return new MasseurDAO().GetAllInListByIdCategorie(-1);
        }
        public void AddMasseur(MasseurEntity nvMasseur)
        {
            new MasseurDAO().AddMassToDB(nvMasseur);
        }

        public MasseurEntity GetMasseurEntityById(int idMasseur)
        {
            return new MasseurDAO().GetMassById(idMasseur);
        }

        public List<MasseurEntity> GetListeMasseursByIdTypePartenaire(int idTypePartenaire)
        {
            int idCategorie = 1;
            MasseurDAO dao = new MasseurDAO();
            if (idTypePartenaire > 2)
            {
                idCategorie = idTypePartenaire - 1;
            }
            return dao.GetAllInListByIdCategorie(idCategorie);
        }

        public int GetIdCategorieByIdMasseur(int idMasseur)
        {
            return new CategorieDAO().GetIdCategorieByIdMasseur(idMasseur);
        }

        public List<MasseurEntity> GetListeMasseursDispoInListByIdTypePartenaire(int idTypePartenaire, DateTime datePrestation)
        {
            int idCategorie = 1;
            MasseurDAO dao = new MasseurDAO();
            if (idTypePartenaire > 2)
            {
                idCategorie = idTypePartenaire - 1;
            }
            return dao.GetMassDispoInListByIdCategorie(idCategorie,datePrestation);
        }

        public int GetIdNiveauCompetencesByIdMasseur(int idMasseur)
        {
            return new EvolutionProfessionnelleDAO().GetIdNiveauCompetencesByIdMasseur(idMasseur);
        }

        public List<NiveauCompetenceEntity> GetListeNiveauCompetence()
        {
            return new NiveauCompetenceDAO().GetListeNiveauCompetence();
        }
        public List<SexeEntity> GetListeSexe()
        {
            return new SexeDAO().GetListeSexe();
        }

        public void AjouterCommentaireMasseur (RetourExperienceMasseurEntity retourMasseur, int idTypeEvalMasseur)
        {
            RetourMasseurDAO dao = new RetourMasseurDAO();
            dao.Insert(retourMasseur, idTypeEvalMasseur);
        }
        public List<CategorieEntity> GetAllCategorie()
        {
            return new CategorieDAO().GetAllCategorie();
        }
        public AptitudeEntity GetAptitudeById(int idAptitude)
        {
            return new AptitudeDAO().GetAptitudeById(idAptitude);
        }
        public int MAJAptitude(AptitudeEntity aptitude)
        {   //Retourne le nouveau idAptitude
            return new AptitudeDAO().UpDateAptitude(aptitude);
        }
        public void MAJMasseur(MasseurEntity masseur)
        {
            new MasseurDAO().UpdateMasseur(masseur);
        }
        public void AddAptitude(AptitudeEntity aptitude)
        {
            new AptitudeDAO().AddAptitude(aptitude);
        }

        public int GetLastIdAptitude()
        {
            return new AptitudeDAO().GetLastIdAptitude();
        }
        public int GetLastIdMasseur()
        {
            return new MasseurDAO().GetLastIdMass();
        }
        public EvolutionProfessionnelleEntity GetEvolutionProfessionnelleById(int idSuiviCompetences)
        {
            return new EvolutionProfessionnelleDAO().GetEvolutionProfessionnelleById(idSuiviCompetences);
        }

        public int GetNbreOccurencePresta(int idPrestation)
        {
            return new PrestationDAO().GetOccuNbr(idPrestation);
        }

        public void AddEvolutionProfessionnelle(EvolutionProfessionnelleEntity evolutionProfessionnelle)
        {
            new EvolutionProfessionnelleDAO().AddEvolutionProfessionnelle(evolutionProfessionnelle);
        }
        public int GetLastIdEvolutionProfessionnelle()
        {
            return new EvolutionProfessionnelleDAO().GetLastIdEvolutionProfessionnelle();
        }

        public void AddPartenaire(PartenaireEntity partenaire)
        {
            new PartenaireDAO().AddPartenaireToDB(partenaire);
        }

        public CategorieEntity GetCategorieByIdTypePartenaire(int idTypePartenaire)
        {
            return new CategorieDAO().GetCategorieByIdTypePartenaire(idTypePartenaire);
        }
        #endregion

        #region Prestations

        // liaison ddl grid dans voir presta
        public DataTable GetGrid(int IdPartenaire)
        {
            PrestationDAO dao = new PrestationDAO();
            return dao.SearchInGridviewGen(IdPartenaire);
        }
        public DataTable GetGridHisto(int IdPartenaire)
        {
            PrestationDAO dao = new PrestationDAO();
            return dao.SearchInGridviewGenHisto(IdPartenaire);
        }
        public void UpdateDateAnnulationPrestation(int idPrestation)
        {
            PrestationDAO dao = new PrestationDAO();
            dao.UpdateDateAnnulation(idPrestation);
        }


        public DataTable Search(string nomPartenaire, int idPrestation)
        {
            PrestationDAO dao = new PrestationDAO();
            return dao.SearchInGridview(nomPartenaire, idPrestation);
        }

        public void SetFinAptitude(AptitudeEntity aptitude)
        {
            new AptitudeDAO().SetEndAptitude(aptitude);
        }

        public List<PrestationEntity> GetPrestationByIdPartner (int idPartenaire)
        {
            PrestationDAO dao = new PrestationDAO();
            return dao.GetPrestationByIdPartenaire(idPartenaire);
        }




        public List<PrestationEntity> GetListePrestation()
        {
            PrestationDAO dao = new PrestationDAO();
            return dao.GetAll();

        }
        // pour le grid de voir presta
        public DataTable GetListPrestationWithNomPartenaire()
        {
            PrestationDAO dao = new PrestationDAO();
            return dao.GetPrestationWithNomPartenaire();
        }
        //pour le grid caché de voir presta 
        public DataTable GetListHistoPrestationWithNomPartenaire()
        {
            PrestationDAO dao = new PrestationDAO();
            return dao.GetHistoriquePrestationWithNomPartenaire();
        }
        //pour le grid de majannulationpresta
        public DataTable GetListForGrid()
        {
            PrestationDAO dao = new PrestationDAO();
            return dao.GetAllForGridView();
        }
        //pour la fonction 'mettre à jour'
        public void UpdateInfoPrestation (PrestationEntity prestation)
        {
            PrestationDAO dao = new PrestationDAO();
            dao.UpdatePrestation(prestation);
        }
        // pour récup presta avec id pour edition
        public PrestationEntity GetPrestationById(int idPrestation)
        {
            PrestationDAO dao = new PrestationDAO();
            return dao.GetById(idPrestation);
        }
        public List<PrestationEntity> GetDatePrestationByLibellePrestation(int idPrestation)
        {
            PrestationDAO dao = new PrestationDAO();
            return dao.GetDatePrestationByLibelle(idPrestation);
        }
        public DataTable GetPrestationDetails()
        {
            PrestationDAO dao = new PrestationDAO();
            return dao.GetDetailsPrestation();
        }

        public DataTable GetPrestationDetailsDispoByIDAndDate(int idMasseur, DateTime date)
        {
            PrestationDAO dao = new PrestationDAO();
            DataTable dt = dao.GetDetailsPrestationDispoByIdAndDate(idMasseur, date);

           // int idMass = idMasseur;
                     
            return dt;
        }

        /*
        public int GetNbreOccurencePresta(int idPrestation)
        {
            PrestationDAO dao = new PrestationDAO();
            return dao.GetOccuNbr(idPrestation);
        }
        */

        public DataTable GetListePrestationByMasseur(int idMasseur)  // FAIRE une surcharge de la précédente?
                {
                    PrestationDAO dao = new PrestationDAO();
                    return dao.GetByMasseur(idMasseur);
                }

        public DataTable GetListePrestationAnterieureByMasseur(int idMasseur, DateTime now)
        {
            PrestationDAO dao = new PrestationDAO();
            return dao.GetByMasseurDateAnt(idMasseur, now);
        }
        public DataTable GetListePrestationAVenirByMasseur(int idMasseur, DateTime now)
        {
            PrestationDAO dao = new PrestationDAO();
            return dao.GetByMasseurDateFut(idMasseur, now);
        }

        public void CreerPrestation(PrestationEntity nouvellePrestation)
        {
            PrestationDAO dao = new PrestationDAO();
            dao.CreateEvent(nouvellePrestation);
        }
        #endregion

        public PrestationEntity GetPrestationByIdPresta (int idPrestation)
        {
            PrestationDAO dao = new PrestationDAO();

            return dao.GetPrestationById(idPrestation);
        }

        public List<MotifDesinscriptionEntity> GetListeMotifDesinscription()
        {
            MotifDesinscriptionDAO dao = new MotifDesinscriptionDAO();

            return dao.GetListeMotifDesinscriptionMasseur();
        }

        public List<TypeEvaluationMasseurEntity> GetListeTypeEvaluation()
        {
            TypeEvaluationMasseurDAO dao = new TypeEvaluationMasseurDAO();

            return dao.GetListeEvaluationMasseur();
        }

        #region Partenaires
        public DataTable GetListePartenaireForPrestation()
        {
            PartenaireDAO dao = new PartenaireDAO();
            return dao.GetPartenaireWithPrestation(); 
        }

        public List<TypeEvaluationPartenaireEntity> GetListeTypeEvaluationPartenaire()
        {
            TypeEvaluationPartenaireDAO dao = new TypeEvaluationPartenaireDAO();

            return dao.GetListeEvaluationPartenaire();
        }
        public List<MotifFinContratPartenaireEntity> GetListeLibelleMotifFinContratPartenaire()
        {
            return new MotifFinContratPartenaireDAO().GetListeLibelleMotifFinContratPartenaire();
        }
        public void SetEndContratPartenaire(ContratPartenaireEntity contratPartenaire)
        {
            new ContratPartenaireDAO().SetEndContratPartenaire(contratPartenaire);
        }
        public int GetLastIdContratPartenaire()
        {
            return new ContratPartenaireDAO().GetLastIdContratPartenaire();
        }
        public void AddContratPartenaire(ContratPartenaireEntity contratPartenaire)
        {
            new ContratPartenaireDAO().AddContratPartenaire(contratPartenaire);
        }
        public ContratPartenaireEntity GetContratPartenaireById(int idContratPartenaire)
        {
            return new ContratPartenaireDAO().GetContratPartenaireById(idContratPartenaire);
        }
        public void MAJPartenaire(PartenaireEntity partenaire)
        {
            new PartenaireDAO().UpdatePartenaire(partenaire);
        }
        public List<PartenaireEntity> GetListePartenairesForPresta()
        {
            PartenaireDAO dao = new PartenaireDAO();
            return dao.GetPartenaireWithPrestationOk(); 
        }
        public List<PartenaireEntity> GetListePartenairesForHistoPresta()
        {
            PartenaireDAO dao = new PartenaireDAO();
            return dao.GetPartenaireWithPrestationHistoOk();
        }
        public DataTable GetListePartenaireForHistoPrestation()
        {
            PartenaireDAO dao = new PartenaireDAO();
            return dao.GetPartenaireWithPrestationForHisto();
        }
        public List<PartenaireEntity> GetListePartenaires()
        {
            return GetListePartenairesByIdCategorie(-1);
        }

        public DataTable GetListePrestationByPartenaire(int idPartenaire, DateTime now)  // FAIRE une surcharge de la précédente?
        {
            PrestationDAO dao = new PrestationDAO();
            return dao.GetByLibellePartenaireDate(idPartenaire, now);
        }

        //public DataTable GetListePrestationByMasseur(int idMasseur, DateTime now)
        //{
        //    PrestationDAO dao = new PrestationDAO();
        //    return dao.GetByLibellePartenaireDate(idMasseur, now);
        //}


        public List<PartenaireEntity> GetListePartenairesByIdCategorie(int IdCategorie)
        {
            return new PartenaireDAO().GetListePartenairesByIdCategorie(IdCategorie);
        }

       

        public PartenaireEntity GetPartenairesById(int IdPartenaire)
        {
            return new PartenaireDAO().GetPartenairesById(IdPartenaire);
        }

        public List<TypePartenaireEntity> GetAllTypePartenaire()
        {
            return new TypePartenaireDAO().GetAllTypePartenaire();
        }
        public DataTable GetTypePartenaireByLibelle()
            {
                PrestationDAO dao = new PrestationDAO();
                return dao.GetByLibellePartenaire();
            }

        public void AjouterCommentairePartenaire (RetourExperiencePartenaireEntity retourPartenaire, int idTypeEvalPartenaire)
                {
                    RetourPartenaireDAO dao = new RetourPartenaireDAO();
                    dao.Insert(retourPartenaire, idTypeEvalPartenaire);
                }
        #endregion

        #region Recurrences
        public void AjoutRecurrence(RecurrenceEntity nouvelleRecurrence)
        {
            RecurrenceDAO dao = new RecurrenceDAO();
            dao.AddReccurence(nouvelleRecurrence);
        }
        public int GetLastIdRecurrence()
        {
            RecurrenceDAO dao = new RecurrenceDAO();
            return dao.GetLastIdRecurrence();
        }
        public List<RecurrenceEntity> GetListeRecurrence()
        {
            RecurrenceDAO dao = new RecurrenceDAO();
            return dao.GetListeRecurrence();
        }
        public int GetIdDureeLibelleRecurrenceByLibelle(string libelle)
        {
            LibelleDureeRecurrenceDAO dao = new LibelleDureeRecurrenceDAO();
            return dao.GetIdDureeLibelleRecurrenceByLibelle(libelle);
        }
        public List<LibelleDureeRecurrenceEntity> GetListeLibelleDureeRecurrence()
        {
            LibelleDureeRecurrenceDAO dao = new LibelleDureeRecurrenceDAO();
            return dao.GetListeLibelleDureeRecurrence();
        }
        public int GetIdLibelleRecurrenceByLibelle(string libelle)
        {
            LibelleRecurrenceDAO dao = new LibelleRecurrenceDAO();
            return dao.GetIdLibelleRecurrenceByLibelle(libelle);
        }
        public List<LibelleRecurrenceEntity> GetListeLibelleRecurrence()
        {
            LibelleRecurrenceDAO dao = new LibelleRecurrenceDAO();
            return dao.GetListeLibelleRecurrence();
        }
        #endregion

        #region Ville
        public VilleEntity GetVilleById(int idVille)
        {
            VilleDAO dao = new VilleDAO();
            return dao.GetVilleById(idVille);

        }
        public VilleEntity GetVilleByLibelle(string libelleVille)
        {
            VilleDAO dao = new VilleDAO();
            return dao.GetVilleByLibelle(libelleVille);

        }
        public List<VilleEntity> GetListeVille()
        {
            return new VilleDAO().GetListeVille();
        }
            #endregion

        #region Paiement
            public void AjoutNouveauTypePaiement(TypePaiementEntity typePaiement)
        {
            TypePaiementDAO dao = new TypePaiementDAO();
            dao.AddNewTypePaiement(typePaiement);
        }
        public int GetLastIdTypePaiement()
        {
            TypePaiementDAO dao = new TypePaiementDAO();
            return dao.GetLastIdTypePaiement();
        }
        public LibelleTypePaiementEntity GetLibelleTypePaiementById(int idLibelleTypePaiement)
        {
            return new LibelleTypePaiementDAO().GetLibelleTypePaiementById(idLibelleTypePaiement);
        }
        public TypePaiementEntity GetTypePaiementById(int idTypePaiement)
        {
            return new TypePaiementDAO().GetTypePaiementById(idTypePaiement);
        }
        public List<LibelleTypePaiementEntity> GetListeLibelleTypePaiement()
        {
            return new LibelleTypePaiementDAO().GetListeLibelleTypePaiement();
        }
        #endregion
    }
}